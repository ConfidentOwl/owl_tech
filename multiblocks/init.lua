local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

dofile(minetest.get_modpath("owl_tech") .. "/multiblocks/multiblock_api.lua")    --api and help func
dofile(minetest.get_modpath("owl_tech") .. "/multiblocks/decor.lua")    --decor node
dofile(minetest.get_modpath("owl_tech") .. "/multiblocks/bronze_blast_furnance.lua") --pimateve blast furnance help_node.lua
dofile(minetest.get_modpath("owl_tech") .. "/multiblocks/wood_pyrolysis.lua")    
dofile(minetest.get_modpath("owl_tech") .. "/multiblocks/help_node.lua")   --energy ,items , fluid hatches