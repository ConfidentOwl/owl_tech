local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

local function set_formspect_bronze_blast_furnance(meta)
	local redines = owl_tech.get_item_redines(meta)
	local burn= meta:get_int("fuel_burn")
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Bronze blast furnance"))).."]"..
	"list[context;input_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;input_in_add;4,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(4,2.5,1,1)..
	"list[context;dst;5.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"label[0,3;"..minetest.formspec_escape(minetest.colorize("#313131", (burn))).."]"..
	"listring[context;dst]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[context;input_in_add]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end

local function bronze_blast_furnanse_build_correct(pos)
	local retu = true
	local centr_pos= {
		x=0,y=0,z=0
	}
    if minetest.get_node_or_nil({x=pos.x+1,y=pos.y,z=pos.z}).name=='mcl_core:lava_source' then --x+1
		centr_pos={
			x=pos.x+1,y=pos.y,z=pos.z
		}	
	elseif minetest.get_node_or_nil({x=pos.x-1,y=pos.y,z=pos.z}).name=='mcl_core:lava_source' then --x-1
		centr_pos={
			x=pos.x-1,y=pos.y,z=pos.z
		}
	elseif minetest.get_node_or_nil({x=pos.x,y=pos.y,z=pos.z+1}).name=='mcl_core:lava_source' then  --z+1
		centr_pos={
			x=pos.x,y=pos.y,z=pos.z+1
		}
	elseif minetest.get_node_or_nil({x=pos.x,y=pos.y,z=pos.z-1}).name=='mcl_core:lava_source' then  --z-1
		centr_pos={
			x=pos.x,y=pos.y,z=pos.z-1
		}
	end
	if centr_pos.y==pos.y then
		for i = -1, 1, 1 do --check botom
			for j = -1, 1, 1 do
				if minetest.get_node_or_nil({x=centr_pos.x+i,y=centr_pos.y-1,z=centr_pos.z+j}).name~="owl_tech:bronze_briks"  then
					retu=false
				end
			end
		end
		if minetest.get_node_or_nil({x=centr_pos.x+1,y=centr_pos.y,z=centr_pos.z+1}).name~="owl_tech:bronze_briks" or
			minetest.get_node_or_nil({x=centr_pos.x-1,y=centr_pos.y,z=centr_pos.z+1}).name~="owl_tech:bronze_briks" or 
			minetest.get_node_or_nil({x=centr_pos.x+1,y=centr_pos.y,z=centr_pos.z-1}).name~="owl_tech:bronze_briks" or
			minetest.get_node_or_nil({x=centr_pos.x-1,y=centr_pos.y,z=centr_pos.z-1}).name~="owl_tech:bronze_briks"  then
				retu=false
		end
		for i = -1, 1, 1 do --check +1 level
			for j = -1, 1, 1 do
				if minetest.get_node_or_nil({x=centr_pos.x+i,y=centr_pos.y+1,z=centr_pos.z+j}).name~="owl_tech:bronze_briks" and not(i==0 and j==0) then
					retu=false
				end
			end
		end
		for i = -1, 1, 1 do --check +2 level
			for j = -1, 1, 1 do
				if minetest.get_node_or_nil({x=centr_pos.x+i,y=centr_pos.y+2,z=centr_pos.z+j}).name~="owl_tech:bronze_briks" and not(i==0 and j==0) then
					retu=false
				end
			end
		end
		return retu
	else
		return false
	end
end

--bronze_blast furnance 
minetest.register_node("owl_tech:bronze_blast_furnace_controller", {
	description = S("Bronze blast furnace"),
	_doc_items_longdesc = S("Primitive way to get steel"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_smelter_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
        owl_tech.set_mashine_tire(meta,1) --all steam mishine has 1 tire
		owl_tech.reset_item_redines(meta)
		set_formspect_bronze_blast_furnance(meta)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
        meta:set_int("fuel_burn",0)
		inv:set_size("input_in", 1) 
		inv:set_size("input_in_add", 1) 
		inv:set_size("dst", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local input_its_add = inv:get_stack('input_in_add', 1)
		local dst_its = inv:get_stack('dst', 1)
		if  bronze_blast_furnanse_build_correct(pos) then
			if meta:get_int("fuel_burn")~=0 and input_its:get_name() ~= "owl_tech:iron_ingot"  then
				meta:set_int("fuel_burn",0)
			end
			if meta:get_int("fuel_burn")==0 and input_its_add:get_name() =='owl_tech:coal' and input_its:get_name() == "owl_tech:iron_ingot"  then
				meta:set_int("fuel_burn",50)
				input_its_add:set_count(input_its_add:get_count()-1)
				inv:set_stack('input_in_add', 1,input_its_add)
			end
			if input_its:get_name()== "owl_tech:iron_ingot"  and meta:get_int("fuel_burn")>0 then -- chek for item in slots  
				local name1 =input_its:get_name() -- for metal
				if name1=="owl_tech:iron_ingot" and owl_tech.get_item_redines(meta)>-1 and owl_tech.get_item_redines(meta)<100  then --recipt real
					owl_tech.add_item_redines(meta,1)
					meta:set_int("fuel_burn",meta:get_int("fuel_burn")-1)
				elseif  name1=="owl_tech:iron_ingot" and owl_tech.get_item_redines(meta)>=100 then
					input_its:set_count(input_its:get_count()-1) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)
					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+1) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item ={name="owl_tech:steel_ingot", count=1, wear=0, metadata=""}
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
				end
			else 
			end
		else
			owl_tech.reset_item_redines(meta)
			meta:set_int("fuel_burn",0)
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_bronze_blast_furnance(meta)
		timer:start(0.2)
	end
})
--Crafte controller 
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:bronze_blast_furnace_controller",
    recipe = {
        {"mcl_furnaces:furnace","owl_tech:bronze_plate","mcl_furnaces:furnace"},
        {"owl_tech:bronze_plate","mcl_furnaces:furnace","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"}
    }
})