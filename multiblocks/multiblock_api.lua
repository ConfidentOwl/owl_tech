local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

-------------------------------------------------------
--get and set active hatch 
function owl_tech:set_active_hatch(meta,amount)
    meta:set_int("activ_hatch", amount)
end
function owl_tech:get_active_hatch(meta)
    return meta:get_int("activ_hatch")
end
-------------------------------------------------------
--get and set enrgy  hatch pos ofset (energy_hatch,item_hatch,fluid_hatch)
function owl_tech:set_hatch_ofset(meta,number_hatch,hatch_name,x,y,z)
    meta:set_int("X"..number_hatch..hatch_name,x)
    meta:set_int("Y"..number_hatch..hatch_name,y)
    meta:set_int("Z"..number_hatch..hatch_name,z)
end
function owl_tech:get_hatch_ofset_X(meta,number_hatch,hatch_name)
    return meta:get_int("X"..number_hatch..hatch_name)
end
function owl_tech:get_hatch_ofset_Y(meta,number_hatch,hatch_name)
    return meta:get_int("Y"..number_hatch..hatch_name)
end
function owl_tech:get_hatch_ofset_Z(meta,number_hatch,hatch_name)
    return meta:get_int("Z"..number_hatch..hatch_name)
end
-------------------------------------------------------
--set amount hatchs 
function owl_tech:set_amount_energy_hatch(meta,amount)
    meta:set_int("amount_energy_hatch",amount)
end
function owl_tech:get_amount_energy_hatch(meta)
    return meta:get_int("amount_energy_hatch")
end
function owl_tech:set_amount_fluid_hatch(meta,amount)
    meta:set_int("amount_fluid_hatch",amount)
end
function owl_tech:get_amount_fluid_hatch(meta)
    return meta:get_int("amount_fluid_hatch")
end
function owl_tech:set_amount_item_hatch(meta,amount)
    meta:set_int("amount_item_hatch",amount)
end
function owl_tech:get_amount_item_hatch(meta)
    return meta:get_int("amount_item_hatch")
end
------------------------------------------------------
--get and set item in hatch 
function owl_tech:get_item_in_input_hatch_pos(x,y,z)
    local meta = minetest.get_meta({x=x,y=y,z=z})
    local inv = meta:get_inventory()
    local input_stack = inv:get_stack("input_in", 1)
    return input_stack
end
function owl_tech:set_item_in_input_hatch_pos(x,y,z,item_stack)
    local meta = minetest.get_meta({x=x,y=y,z=z})
    local inv = meta:get_inventory()
    inv:set_stack("input_in", 1 ,item_stack) 
end
function owl_tech:get_item_in_input_output_pos(x,y,z)
    local meta = minetest.get_meta({x=x,y=y,z=z})
    local inv = meta:get_inventory()
    local input_stack = inv:get_stack("dst", 1)
    return input_stack
end
function owl_tech:set_item_in_input_output_pos(x,y,z,item_stack)
    local meta = minetest.get_meta({x=x,y=y,z=z})
    local inv = meta:get_inventory()
    inv:set_stack("dst", 1 ,item_stack) 
end
------------------------------------------------------
--get and set energy in hatch
function owl_tech:get_charge_in_hatch_pos(x,y,z)
    return owl_tech:get_charge(minetest.get_meta({x=x,y=y,z=z}))
end
function owl_tech:set_charge_in_hatch_pos(x,y,z,energy)
    owl_tech:set_charge(minetest.get_meta({x=x,y=y,z=z}),energy)
end
------------------------------------------------------
--get and set max charge in hatch
function owl_tech:get_max_charge_in_hatch_pos(x,y,z)
    return owl_tech:get_charge_max(minetest.get_meta({x=x,y=y,z=z}))
end
function owl_tech:set_max_charge_in_hatch_pos(x,y,z,energy)
    owl_tech:set_charge_max(minetest.get_meta({x=x,y=y,z=z}),energy)
end
------------------------------------------------------
--get and set voltage hatch 
function owl_tech:get_voltage_in_hatch_pos(x,y,z)
    return owl_tech:get_voltage(minetest.get_meta({x=x,y=y,z=z}))
end
function owl_tech:set_voltage_in_hatch_pos(x,y,z,energy)
    owl_tech:set_voltage(minetest.get_meta({x=x,y=y,z=z}),energy)
end
------------------------------------------------------
--get and set fluid amount in hatch
function owl_tech:get_volume_amount_in_hatch_pos(x,y,z)
    return owl_tech.get_pull_volume(minetest.get_meta({x=x,y=y,z=z}),1)
end
function owl_tech:set_volume_amount_in_hatch_pos(x,y,z,volume)
    owl_tech.set_pull_volume(minetest.get_meta({x=x,y=y,z=z}),1,volume)
end
------------------------------------------------------
--get and set max volume amount in hatch
function owl_tech:get_max_volume_in_hatch_pos(x,y,z)
    return owl_tech.get_pull_max_volume(minetest.get_meta({x=x,y=y,z=z}),1)
end
function owl_tech:set_max_volume_in_hatch_pos(x,y,z,volume)
    owl_tech.set_pull_max_volume(minetest.get_meta({x=x,y=y,z=z}),1,volume)
end
------------------------------------------------------
--get and set name fluid in hatch
function owl_tech:get_name_fluid_in_hatch_pos(x,y,z)
    return owl_tech.get_pull_fluid_name(minetest.get_meta({x=x,y=y,z=z}),1)
end
function owl_tech:set_name_fluid_in_hatch_pos(x,y,z,name)
    owl_tech.set_pull_fluid_name(minetest.get_meta({x=x,y=y,z=z}),1,name)
end
-------------------------------------------------------
