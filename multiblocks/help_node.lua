local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

local energy_hatch_table ={
    {"base","Base",32,16000,"#575757","owl_tech:copper_electro_wire","steel","owl_tech:bronze_fluid_pipe"}
}
for i, value in ipairs(energy_hatch_table) do
    --input electro hatch 
    minetest.register_node("owl_tech:"..energy_hatch_table[i][1].."_energy_hatch_input", {
        description = S(energy_hatch_table[i][2].."energy hatch input"),
        _doc_items_longdesc = S("Contein energy for multinode"),
        tiles = {
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_steam_input.png",
        },
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2, owl_tech_machine=1,energy_hatch_1_input=1,owl_tech_electro_mashine=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        paramtype2 = "facedir",
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
        on_construct = function(pos)
            local meta = minetest.get_meta(pos)
            owl_tech:add_electro(pos,energy_hatch_table[i][3],energy_hatch_table[i][4])
            owl_tech.set_mashine_tire(meta,i+1)
            owl_tech:set_active_hatch(meta,0)
            local timer =minetest.get_node_timer(pos)
            meta:set_string("infotext",owl_tech:get_charge(meta).."/"..owl_tech:get_charge_max(meta))
            timer:start(0.2)
        end,
        on_timer = function(pos, elapsed)
            local timer = minetest.get_node_timer(pos)
            local meta = minetest.get_meta(pos)
            local rotation = minetest.get_node(pos).param2 % 32
                if rotation==0 then --z-1
                    if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+1})).name,"owl_tech_electro_wire")>0
                    and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
                        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1})
                        if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
                            owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
                        end
                    end
                end
                if rotation==2 then --z+1
                    if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z-1})).name,"owl_tech_electro_wire")>0
                    and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
                        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1})
                        if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
                            owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
                        end
                    end
                end
                if rotation==3 then --x-1
                    if minetest.get_item_group((minetest.get_node({x=pos.x+1,y=pos.y,z=pos.z})).name,"owl_tech_electro_wire")>0
                    and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
                        local meta_up = minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z})
                        if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
                            owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
                        end
                    end
                end
                if rotation==1 then --x+1
                    if minetest.get_item_group((minetest.get_node({x=pos.x-1,y=pos.y,z=pos.z})).name,"owl_tech_electro_wire")>0
                    and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
                        local meta_up = minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z})
                        if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
                            owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
                        end
                    end
                end
            
            meta:set_string("infotext",owl_tech:get_charge(meta).."/"..owl_tech:get_charge_max(meta))
            timer:start(0.2)
        end
    })
    --Crafte 
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..energy_hatch_table[i][1].."_energy_hatch_input",
        recipe = {
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate",energy_hatch_table[i][6],"owl_tech:"..energy_hatch_table[i][7].."_plate"}
        }
    })
    --output electro hatch 
    minetest.register_node("owl_tech:"..energy_hatch_table[i][1].."_energy_hatch_output", {
        description = S(energy_hatch_table[i][2].."energy hatch output"),
        _doc_items_longdesc = S("Contein energy for multinode"),
        tiles = {
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_steam_output.png",
        },
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2, owl_tech_machine=1,energy_hatch_1_output=1,owl_tech_electro_mashine=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        paramtype2 = "facedir",
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
        on_construct = function(pos)
            local meta = minetest.get_meta(pos)
            owl_tech:add_electro(pos,energy_hatch_table[i][3],energy_hatch_table[i][4])
            owl_tech.set_mashine_tire(meta,i+1)
            owl_tech:set_active_hatch(meta,0)
            local timer =minetest.get_node_timer(pos)
            meta:set_string("infotext",owl_tech:get_charge(meta).."/"..owl_tech:get_charge_max(meta))
            timer:start(0.2)
        end,
        on_timer = function(pos, elapsed)
            local timer = minetest.get_node_timer(pos)
            local meta = minetest.get_meta(pos)
            local rotation = minetest.get_node(pos).param2 % 32
                if rotation==0 then --z-1
                    if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+1})).name,"owl_tech_electro_wire") then --send electro in wire
                        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1})
                        if  owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) and 
                        owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) and 
                        owl_tech:get_charge_max(meta_up)-owl_tech:get_charge(meta_up)>=owl_tech:get_voltage(meta) then
                            owl_tech:set_charge(meta,owl_tech:get_charge(meta)-owl_tech:get_voltage(meta))
                            owl_tech:set_charge(meta_up,owl_tech:get_charge(meta_up)+owl_tech:get_voltage(meta_up))
                        end
                    end
                end
                if rotation==2 then --z+1
                    if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z-1})).name,"owl_tech_electro_wire") then --send electro in wire
                        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1})
                        if  owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) and 
                        owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) and 
                        owl_tech:get_charge_max(meta_up)-owl_tech:get_charge(meta_up)>=owl_tech:get_voltage(meta) then
                            owl_tech:set_charge(meta,owl_tech:get_charge(meta)-owl_tech:get_voltage(meta))
                            owl_tech:set_charge(meta_up,owl_tech:get_charge(meta_up)+owl_tech:get_voltage(meta_up))
                        end
                    end
                end
                if rotation==3 then --x-1
                    if minetest.get_item_group((minetest.get_node({x=pos.x+1,y=pos.y,z=pos.z})).name,"owl_tech_electro_wire") then --send electro in wire
                        local meta_up = minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z})
                        if  owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) and 
                        owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) and 
                        owl_tech:get_charge_max(meta_up)-owl_tech:get_charge(meta_up)>=owl_tech:get_voltage(meta) then
                            owl_tech:set_charge(meta,owl_tech:get_charge(meta)-owl_tech:get_voltage(meta))
                            owl_tech:set_charge(meta_up,owl_tech:get_charge(meta_up)+owl_tech:get_voltage(meta_up))
                        end
                    end
                end
                if rotation==1 then --x+1
                    if minetest.get_item_group((minetest.get_node({x=pos.x-1,y=pos.y,z=pos.z})).name,"owl_tech_electro_wire") then --send electro in wire
                        local meta_up = minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z})
                        if  owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) and 
                        owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) and 
                        owl_tech:get_charge_max(meta_up)-owl_tech:get_charge(meta_up)>=owl_tech:get_voltage(meta) then
                            owl_tech:set_charge(meta,owl_tech:get_charge(meta)-owl_tech:get_voltage(meta))
                            owl_tech:set_charge(meta_up,owl_tech:get_charge(meta_up)+owl_tech:get_voltage(meta_up))
                        end
                    end
                end
            
            meta:set_string("infotext",owl_tech:get_charge(meta).."/"..owl_tech:get_charge_max(meta))
            timer:start(0.2)
        end
    })
    --Crafte 
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..energy_hatch_table[i][1].."_energy_hatch_output",
        recipe = {
            {"owl_tech:"..energy_hatch_table[i][7].."_plate",energy_hatch_table[i][6],"owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"}
        }
    })
    --input item
    minetest.register_node("owl_tech:"..energy_hatch_table[i][1].."_item_hatch_input", {
        description = S(energy_hatch_table[i][2].."item hatch input"),
        _doc_items_longdesc = S("Contein item for multinode"),
        tiles = {
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_item_hatch.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_item_hatch.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_item_hatch.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_steam_input.png",
        },
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2, owl_tech_machine=1,item_hatch_1_input=1,input_in=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        paramtype2 = "facedir",
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
        on_construct = function(pos)
            local meta = minetest.get_meta(pos)
            owl_tech.set_mashine_tire(meta,i+1)
            owl_tech:set_active_hatch(meta,0)
            local inv = meta:get_inventory()
            inv:set_size("input_in", 1) 
            local timer =minetest.get_node_timer(pos)
            local text_info ="empty"
            local input_stack = inv:get_stack("input_in", 1)
            if not  input_stack:is_empty() then
                text_info = input_stack:get_count().."--"..input_stack:get_name()
            end
            meta:set_string("infotext",text_info)
            timer:start(0.2)
        end,
        on_timer = function(pos, elapsed)
            local timer = minetest.get_node_timer(pos)
            local meta = minetest.get_meta(pos)
            local inv = meta:get_inventory()
            local input_stack = inv:get_stack("input_in", 1)
            local rotation = minetest.get_node(pos).param2 % 32
            local text_info ="empty"
            if not  input_stack:is_empty() then
                text_info = input_stack:get_count().."--"..input_stack:get_name()
            end
            meta:set_string("infotext",text_info)
            timer:start(0.2)
        end
    })
    --Crafte 
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..energy_hatch_table[i][1].."_item_hatch_input",
        recipe = {
            {"owl_tech:"..energy_hatch_table[i][7].."_plate",energy_hatch_table[i][6],"owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","mcl_chests:chest","owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","mcl_chests:chest","owl_tech:"..energy_hatch_table[i][7].."_plate"}
        }
    })
    --output item
    minetest.register_node("owl_tech:"..energy_hatch_table[i][1].."_item_hatch_output", {
        description = S(energy_hatch_table[i][2].."item hatch output"),
        _doc_items_longdesc = S("Contein item for multinode"),
        tiles = {
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_item_hatch.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_item_hatch.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_item_hatch.png",
            "(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_steam_output.png",
        },
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2, owl_tech_machine=1,item_hatch_1_output=1,dst=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        paramtype2 = "facedir",
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
        on_construct = function(pos)
            local meta = minetest.get_meta(pos)
            owl_tech.set_mashine_tire(meta,i+1)
            owl_tech:set_active_hatch(meta,0)
            local inv = meta:get_inventory()
            inv:set_size("dst", 1) 
            local timer =minetest.get_node_timer(pos)
            local text_info ="empty"
            local input_stack = inv:get_stack("dst", 1)
            if not  input_stack:is_empty() then
                text_info = input_stack:get_count().."--"..input_stack:get_name()
            end
            meta:set_string("infotext",text_info)
            timer:start(0.2)
        end,
        on_timer = function(pos, elapsed)
            local timer = minetest.get_node_timer(pos)
            local meta = minetest.get_meta(pos)
            local inv = meta:get_inventory()
            local input_stack = inv:get_stack("dst", 1)
            local text_info ="empty"
            if not  input_stack:is_empty() then
                text_info = input_stack:get_count().."--"..input_stack:get_name()
            end
            meta:set_string("infotext",text_info)
            timer:start(0.2)
        end
    })
    --Crafte 
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..energy_hatch_table[i][1].."_item_hatch_output",
        recipe = {
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","mcl_chests:chest","owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","mcl_chests:chest","owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate",energy_hatch_table[i][6],"owl_tech:"..energy_hatch_table[i][7].."_plate"}
        }
    })
    --input fluid hatch 
    minetest.register_node("owl_tech:"..energy_hatch_table[i][1].."_fluid_hatch_input", {
        description = S(energy_hatch_table[i][2].."fluid hatch input"),
        _doc_items_longdesc = S("Contein fluid for multinode"),
        tiles = {
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_steam_input.png",
        },
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2, owl_tech_machine=1,fluid_hatch_1_input=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        paramtype2 = "facedir",
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
        on_construct = function(pos)
            local meta = minetest.get_meta(pos)
            owl_tech.add_new_pull(meta,(energy_hatch_table[i][4]*2),1,1)
            owl_tech.set_mashine_tire(meta,i+1)
            owl_tech:set_active_hatch(meta,0)
            owl_tech.update_fluid_pipe_around(pos)
            owl_tech.set_fluid_sand_in_tick(meta,(energy_hatch_table[i][3]*2))
            local timer =minetest.get_node_timer(pos)
            meta:set_string("infotext",owl_tech.get_pull_volume(meta,1).."--"..owl_tech.get_pull_fluid_name(meta,1))
            timer:start(0.2)
        end,
        on_timer = function(pos, elapsed)
            local timer = minetest.get_node_timer(pos)
            local meta = minetest.get_meta(pos)
            local rotation = minetest.get_node(pos).param2 % 32
                if rotation==0 then --z-1
                    if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+1})).name,"fluid_pipe") then --get from pipe 
                        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1})
                        local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,1)
                        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
                        local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        if can_do and can_do2 then
                            owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
                            owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
                        end
                    end
                end
                if rotation==2 then --z+1
                    if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z-1})).name,"fluid_pipe") then --get from pipe 
                        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1})
                        local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,1)
                        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
                        local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        if can_do and can_do2 then
                            owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
                            owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
                        end
                    end
                end
                if rotation==3 then --x-1
                    if minetest.get_item_group((minetest.get_node({x=pos.x+1,y=pos.y,z=pos.z})).name,"fluid_pipe") then --get from pipe 
                        local meta_up = minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z})
                        local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,1)
                        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
                        local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        if can_do and can_do2 then
                            owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
                            owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
                        end
                    end
                end
                if rotation==1 then --x+1
                    if minetest.get_item_group((minetest.get_node({x=pos.x-1,y=pos.y,z=pos.z})).name,"fluid_pipe") then --get from pipe 
                        local meta_up = minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z})
                        local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,1)
                        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
                        local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        if can_do and can_do2 then
                            owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
                            owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
                        end
                    end
                end
            
            owl_tech.delit_name_fluid_if_0(meta)
            meta:set_string("infotext",owl_tech.get_pull_volume(meta,1).."--"..owl_tech.get_pull_fluid_name(meta,1))
            timer:start(0.2)
        end
    })
    --Crafte 
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..energy_hatch_table[i][1].."_fluid_hatch_input",
        recipe = {
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate",energy_hatch_table[i][8],"owl_tech:"..energy_hatch_table[i][7].."_plate"}
        }
    })
    --output fluid hatch
    minetest.register_node("owl_tech:"..energy_hatch_table[i][1].."_fluid_hatch_output", {
        description = S(energy_hatch_table[i][2].."fluid hatch output"),
        _doc_items_longdesc = S("Contein fluid for multinode"),
        tiles = {
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)",
            "(owl_tech_base_tank_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_steam_output.png",
        },
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2, owl_tech_machine=1,fluid_hatch_1_output=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        paramtype2 = "facedir",
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
        on_construct = function(pos)
            local meta = minetest.get_meta(pos)
            owl_tech.add_new_pull(meta,(energy_hatch_table[i][4]*2),1,1)
            owl_tech.set_mashine_tire(meta,i+1)
            owl_tech:set_active_hatch(meta,0)
            owl_tech.update_fluid_pipe_around(pos)
            owl_tech.set_fluid_sand_in_tick(meta,(energy_hatch_table[i][3]*2))
            local timer =minetest.get_node_timer(pos)
            meta:set_string("infotext",owl_tech.get_pull_volume(meta,1).."--"..owl_tech.get_pull_fluid_name(meta,1))
            timer:start(0.2)
        end,
        on_timer = function(pos, elapsed)
            local timer = minetest.get_node_timer(pos)
            local meta = minetest.get_meta(pos)
            local rotation = minetest.get_node(pos).param2 % 32
                if rotation==0 then --z-1
                    if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+1})).name,"fluid_pipe") then --sand in pipe
                        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1})
                        local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,1)
                        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        if can_do and can_do2 then
                            owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
                            local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
                            local difer = pull_curent_volume-remove_amount
                            owl_tech.set_pull_volume(meta,1,difer)
                        end
                    end
                end
                if rotation==2 then --z+1
                    if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z-1})).name,"fluid_pipe") then --sand in pipe
                        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1})
                        local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,1)
                        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        if can_do and can_do2 then
                            owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
                            local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
                            local difer = pull_curent_volume-remove_amount
                            owl_tech.set_pull_volume(meta,1,difer)
                        end
                    end
                end
                if rotation==3 then --x-1
                    if minetest.get_item_group((minetest.get_node({x=pos.x+1,y=pos.y,z=pos.z})).name,"fluid_pipe") then --sand in pipe
                        local meta_up = minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z})
                        local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,1)
                        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        if can_do and can_do2 then
                            owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
                            local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
                            local difer = pull_curent_volume-remove_amount
                            owl_tech.set_pull_volume(meta,1,difer)
                        end
                    end
                end
                if rotation==1 then --x+1
                    if minetest.get_item_group((minetest.get_node({x=pos.x-1,y=pos.y,z=pos.z})).name,"fluid_pipe") then --sand in pipe
                        local meta_up = minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z})
                        local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,1)
                        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
                        if can_do and can_do2 then
                            owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
                            local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
                            local difer = pull_curent_volume-remove_amount
                            owl_tech.set_pull_volume(meta,1,difer)
                        end
                    end
                end
            
            owl_tech.delit_name_fluid_if_0(meta)
            meta:set_string("infotext",owl_tech.get_pull_volume(meta,1).."--"..owl_tech.get_pull_fluid_name(meta,1))
            timer:start(0.2)
        end
    })
    --Crafte 
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..energy_hatch_table[i][1].."_fluid_hatch_output",
        recipe = {
            {"owl_tech:"..energy_hatch_table[i][7].."_plate",energy_hatch_table[i][8],"owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"},
            {"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"}
        }
    })
end