local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

local function set_formspect_wood_pyrolis(meta)
	local redines = owl_tech.get_item_redines(meta)
	local b_corect = "eror"
	if meta:get_int("b_corect")==1 then
		b_corect="ok"
	end
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Wood pyrolis"))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"label[0,3;"..minetest.formspec_escape(minetest.colorize("#313131", (b_corect.."--correctly built"))).."]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end

local function bronze_blast_furnanse_build_correct(pos)
	local retu = true
	local meta = minetest.get_meta(pos)
	local rotation = minetest.get_node(pos).param2 % 32

	local id_frame = minetest.get_content_id("owl_tech:steel_big_tiles")
	local id_entrails = minetest.get_content_id("owl_tech:copper_tiles")
	local id_other= minetest.get_content_id("owl_tech:tin_tiles")

	local id_item_input_hatch = minetest.get_content_id("owl_tech:base_item_hatch_input")
	local id_item_output_hatch = minetest.get_content_id("owl_tech:base_item_hatch_output")
	local id_energy_input_hatch = minetest.get_content_id("owl_tech:base_energy_hatch_input") 
	local id_energy_output_hatch = minetest.get_content_id("owl_tech:base_energy_hatch_output") 
	local id_fluid_input_hatch = minetest.get_content_id("owl_tech:base_fluid_hatch_input") 
	local id_fluid_output_hatch = minetest.get_content_id("owl_tech:base_fluid_hatch_output")

	local item_hatch_input_count = 0
	local item_hatch_output_count = 0
	local energy_hatch_input_count = 0
	local fluid_hatch_output_count = 0 

	--check frame 
	if rotation==0 then --z-1
		local corect =true 
		local vm = minetest.get_voxel_manip()
		local pos1,pos2 = {x=pos.x-1,y=pos.y-1,z=pos.z},{x=pos.x+1,y=pos.y+1,z=pos.z+4}
		local emin, emax = vm:read_from_map(pos1,pos2)
		local a = VoxelArea:new{
			MinEdge = emin,
			MaxEdge = emax
		}
		local data = vm:get_data()
		-- begin frames
		for z = pos1.z, pos2.z do
			for y = pos1.y,pos2.y,pos2.y-pos1.y do
				for x = pos1.x,pos2.x,pos2.x-pos1.x do
					local vi = a:index(x, y, z)
					if data[vi] ~= id_frame then
						minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
						corect = false
					end
				end
			end
		end
		for y = pos1.y,pos2.y,pos2.y-pos1.y do
			for z = pos1.z,pos2.z,pos2.z-pos1.z do
				local vi = a:index(pos.x, y, z)
				if data[vi] ~= id_frame then
					minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
					corect = false
				end
			end
		end
		for x = pos1.x,pos2.x,pos2.x-pos1.x do
			for z = pos1.z,pos2.z,pos2.z-pos1.z do
				local vi = a:index(x, pos1.y+1, z)
				if data[vi] ~= id_frame then
					minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
					corect = false
				end
			end
		end
		-- end frames -begin entrails
		for z = pos1.z+1,pos2.z-1 , 1 do
			local vi = a:index(pos.x, pos.y, z)
			if data[vi] ~= id_entrails then
				minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
				corect = false
			end
		end
		-- end entrails look for hatch
		for x = pos1.x, pos2.x,pos2.x-pos1.x  do
			for z = pos1.z+1,pos2.z-1 , 1 do
				local vi = a:index(x, pos.y, z)
				if data[vi] == id_item_input_hatch then
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",x,pos.y,z)
					item_hatch_input_count = item_hatch_input_count + 1
				end
				if data[vi] == id_item_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",x,pos.y,z)
					item_hatch_output_count = item_hatch_output_count + 1
				end
				if data[vi] == id_energy_input_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",x,pos.y,z)
					energy_hatch_input_count = energy_hatch_input_count + 1
				end
				if data[vi] == id_fluid_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",x,pos.y,z)
					fluid_hatch_output_count = fluid_hatch_output_count + 1
				end
			end		
		end
		for y = pos1.y, pos2.y,pos2.y-pos1.y  do
			for z = pos1.z+1,pos2.z-1 , 1 do
				local vi = a:index(pos.x, y, z)
				if data[vi] == id_item_input_hatch then
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",pos.x, y, z)
					item_hatch_input_count = item_hatch_input_count + 1
				end
				if data[vi] == id_item_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",pos.x, y, z)
					item_hatch_output_count = item_hatch_output_count + 1
				end
				if data[vi] == id_energy_input_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",pos.x, y, z)
					energy_hatch_input_count = energy_hatch_input_count + 1
				end
				if data[vi] == id_fluid_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, y, z)
					fluid_hatch_output_count = fluid_hatch_output_count + 1
				end
			end		
		end
		if data[a:index(pos.x, pos.y, pos2.z)] == id_item_input_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, pos.y, pos2.z)
			fluid_hatch_output_count = fluid_hatch_output_count + 1
		end
		if data[a:index(pos.x, pos.y, pos2.z)] == id_item_output_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",pos.x, pos.y, pos2.z)
			item_hatch_output_count = item_hatch_output_count + 1
		end
		if data[a:index(pos.x, pos.y, pos2.z)] == id_energy_input_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",pos.x, pos.y, pos2.z)
			energy_hatch_input_count = energy_hatch_input_count + 1
		end
		if data[a:index(pos.x, pos.y, pos2.z)] == id_fluid_output_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, pos.y, pos2.z)
			fluid_hatch_output_count = fluid_hatch_output_count + 1
		end
		--end look for hacth begin look for other nodes
		for x = pos1.x, pos2.x,pos2.x-pos1.x  do
			for z = pos1.z+1,pos2.z-1 , 1 do
				local vi = a:index(x, pos.y, z)
				if not (data[vi] == id_other or data[vi] == id_item_input_hatch or data[vi] == id_item_output_hatch or data[vi] == id_energy_input_hatch or data[vi] == id_fluid_output_hatch) then
					minetest.chat_send_all(1)
					corect = false
				end
			end		
		end
		for y = pos1.y, pos2.y,pos2.y-pos1.y  do
			for z = pos1.z+1,pos2.z-1 , 1 do
				local vi = a:index(pos.x, y, z)
				if not (data[vi] == id_other or data[vi] == id_item_input_hatch or data[vi] == id_item_output_hatch or data[vi] == id_energy_input_hatch or data[vi] == id_fluid_output_hatch) then
					minetest.chat_send_all(1)
					corect = false
				end
			end		
		end
		if not (data[a:index(pos.x, pos.y, pos2.z)] == id_other or data[a:index(pos.x, pos.y, pos2.z)] == id_item_input_hatch or data[a:index(pos.x, pos.y, pos2.z)] == id_item_output_hatch 
		or data[a:index(pos.x, pos.y, pos2.z)] == id_energy_input_hatch or data[a:index(pos.x, pos.y, pos2.z)] == id_fluid_output_hatch) then 
			corect = false
		end
		if  item_hatch_input_count~=1 or item_hatch_output_count~=1 or energy_hatch_input_count~=1 or fluid_hatch_output_count~=1  then--get need count of hatchs
			local meta = minetest.get_meta(pos)
			corect = false
			minetest.chat_send_all(rotation.."eror!")
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",0,0,0)
		end
		return corect
	end
	if rotation==2 then --z+1
		local corect =true 
		local vm = minetest.get_voxel_manip()
		local pos1,pos2 = {x=pos.x-1,y=pos.y-1,z=pos.z-4},{x=pos.x+1,y=pos.y+1,z=pos.z}
		local emin, emax = vm:read_from_map(pos1,pos2)
		local a = VoxelArea:new{
			MinEdge = emin,
			MaxEdge = emax
		}
		local data = vm:get_data()
		-- begin frames
		for z = pos1.z, pos2.z do
			for y = pos1.y,pos2.y,pos2.y-pos1.y do
				for x = pos1.x,pos2.x,pos2.x-pos1.x do
					local vi = a:index(x, y, z)
					if data[vi] ~= id_frame then
						minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
						corect = false
					end
				end
			end
		end
		for y = pos1.y,pos2.y,pos2.y-pos1.y do
			for z = pos1.z,pos2.z,pos2.z-pos1.z do
				local vi = a:index(pos.x, y, z)
				if data[vi] ~= id_frame then
					minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
					corect = false
				end
			end
		end
		for x = pos1.x,pos2.x,pos2.x-pos1.x do
			for z = pos1.z,pos2.z,pos2.z-pos1.z do
				local vi = a:index(x, pos1.y+1, z)
				if data[vi] ~= id_frame then
					minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
					corect = false
				end
			end
		end
		-- end frames -begin entrails
		for z = pos1.z+1,pos2.z-1 , 1 do
			local vi = a:index(pos.x, pos.y, z)
			if data[vi] ~= id_entrails then
				minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
				corect = false
			end
		end
		-- end entrails look for hatch
		for x = pos1.x, pos2.x,pos2.x-pos1.x  do
			for z = pos1.z+1,pos2.z-1 , 1 do
				local vi = a:index(x, pos.y, z)
				if data[vi] == id_item_input_hatch then
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",x,pos.y,z)
					item_hatch_input_count = item_hatch_input_count + 1
				end
				if data[vi] == id_item_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",x,pos.y,z)
					item_hatch_output_count = item_hatch_output_count + 1
				end
				if data[vi] == id_energy_input_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",x,pos.y,z)
					energy_hatch_input_count = energy_hatch_input_count + 1
				end
				if data[vi] == id_fluid_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",x,pos.y,z)
					fluid_hatch_output_count = fluid_hatch_output_count + 1
				end
			end		
		end
		for y = pos1.y, pos2.y,pos2.y-pos1.y  do
			for z = pos1.z+1,pos2.z-1 , 1 do
				local vi = a:index(pos.x, y, z)
				if data[vi] == id_item_input_hatch then
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",pos.x, y, z)
					item_hatch_input_count = item_hatch_input_count + 1
				end
				if data[vi] == id_item_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",pos.x, y, z)
					item_hatch_output_count = item_hatch_output_count + 1
				end
				if data[vi] == id_energy_input_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",pos.x, y, z)
					energy_hatch_input_count = energy_hatch_input_count + 1
				end
				if data[vi] == id_fluid_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, y, z)
					fluid_hatch_output_count = fluid_hatch_output_count + 1
				end
			end		
		end
		if data[a:index(pos.x, pos.y, pos2.z)] == id_item_input_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, pos.y, pos2.z)
			fluid_hatch_output_count = fluid_hatch_output_count + 1
		end
		if data[a:index(pos.x, pos.y, pos2.z)] == id_item_output_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",pos.x, pos.y, pos2.z)
			item_hatch_output_count = item_hatch_output_count + 1
		end
		if data[a:index(pos.x, pos.y, pos2.z)] == id_energy_input_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",pos.x, pos.y, pos2.z)
			energy_hatch_input_count = energy_hatch_input_count + 1
		end
		if data[a:index(pos.x, pos.y, pos2.z)] == id_fluid_output_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, pos.y, pos2.z)
			fluid_hatch_output_count = fluid_hatch_output_count + 1
		end
		--end look for hacth begin look for other nodes
		for x = pos1.x, pos2.x,pos2.x-pos1.x  do
			for z = pos1.z+1,pos2.z-1 , 1 do
				local vi = a:index(x, pos.y, z)
				if not (data[vi] == id_other or data[vi] == id_item_input_hatch or data[vi] == id_item_output_hatch or data[vi] == id_energy_input_hatch or data[vi] == id_fluid_output_hatch) then
					corect = false
				end
			end		
		end
		for y = pos1.y, pos2.y,pos2.y-pos1.y  do
			for z = pos1.z+1,pos2.z-1 , 1 do
				local vi = a:index(pos.x, y, z)
				if not (data[vi] == id_other or data[vi] == id_item_input_hatch or data[vi] == id_item_output_hatch or data[vi] == id_energy_input_hatch or data[vi] == id_fluid_output_hatch) then
					corect = false
				end
			end		
		end
		if not (data[a:index(pos.x, pos.y, pos1.z)]== id_other 
		or data[a:index(pos.x, pos.y, pos1.z)] == id_item_input_hatch 
		or data[a:index(pos.x, pos.y, pos1.z)] == id_item_output_hatch 
		or data[a:index(pos.x, pos.y, pos1.z)] == id_energy_input_hatch 
		or data[a:index(pos.x, pos.y, pos1.z)] == id_fluid_output_hatch) then
			corect = false
		end
		if  item_hatch_input_count~=1 or item_hatch_output_count~=1 or energy_hatch_input_count~=1 or fluid_hatch_output_count~=1  then--get need count of hatchs
			local meta = minetest.get_meta(pos)
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",0,0,0)
		end
		return corect
	end
	if rotation==1 then --x-1
		local corect =true 
		local vm = minetest.get_voxel_manip()
		local pos1,pos2 = {x=pos.x,y=pos.y-1,z=pos.z-1},{x=pos.x+4,y=pos.y+1,z=pos.z+1}
		local emin, emax = vm:read_from_map(pos1,pos2)
		local a = VoxelArea:new{
			MinEdge = emin,
			MaxEdge = emax
		}
		local data = vm:get_data()
		-- begin frames
		for x = pos1.x, pos2.x do
			for y = pos1.y,pos2.y,pos2.y-pos1.y do
				for z = pos1.z,pos2.z,pos2.x-pos1.z do
					local vi = a:index(x, y, z)
					if data[vi] ~= id_frame then
						minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
						corect = false
					end
				end
			end
		end
		for y = pos1.y,pos2.y,pos2.y-pos1.y do
			for x = pos1.x,pos2.x,pos2.x-pos1.x do
				local vi = a:index(x, y, pos.z)
				if data[vi] ~= id_frame then
					minetest.chat_send_all("error"..x.."--"..pos1.y.."--"..pos1.z)
					corect = false
				end
			end
		end
		for z = pos1.z,pos2.z,pos2.z-pos1.z do
			for x = pos1.x,pos2.x,pos2.x-pos1.x do
				local vi = a:index(x, pos1.y+1, z)
				if data[vi] ~= id_frame then
					minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
					corect = false
				end
			end
		end
		-- end frames -begin entrails
		for x = pos1.x+1,pos2.x-1 , 1 do
			local vi = a:index(x, pos.y, pos.z)
			if data[vi] ~= id_entrails then
				minetest.chat_send_all("error"..x.."--"..pos1.y.."--"..pos1.z)
				corect = false
			end
		end
		-- end entrails look for hatch
		for z = pos1.z, pos2.z,pos2.z-pos1.z  do
			for x = pos1.x+1,pos2.x-1 , 1 do
				local vi = a:index(x, pos.y, z)
				if data[vi] == id_item_input_hatch then
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",x,pos.y,z)
					item_hatch_input_count = item_hatch_input_count + 1
				end
				if data[vi] == id_item_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",x,pos.y,z)
					item_hatch_output_count = item_hatch_output_count + 1
				end
				if data[vi] == id_energy_input_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",x,pos.y,z)
					energy_hatch_input_count = energy_hatch_input_count + 1
				end
				if data[vi] == id_fluid_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",x,pos.y,z)
					fluid_hatch_output_count = fluid_hatch_output_count + 1
				end
			end		
		end
		for y = pos1.y, pos2.y,pos2.y-pos1.y  do
			for x = pos1.x+1,pos2.x-1 , 1 do
				local vi = a:index(x, y, pos.z)
				if data[vi] == id_item_input_hatch then
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",x, y, pos.z)
					item_hatch_input_count = item_hatch_input_count + 1
				end
				if data[vi] == id_item_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",x, y, pos.z)
					item_hatch_output_count = item_hatch_output_count + 1
				end
				if data[vi] == id_energy_input_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",x, y, pos.z)
					energy_hatch_input_count = energy_hatch_input_count + 1
				end
				if data[vi] == id_fluid_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",x, y, pos.z)
					fluid_hatch_output_count = fluid_hatch_output_count + 1
				end
			end		
		end
		if data[a:index(pos2.x, pos.y, pos.z)] == id_item_input_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, pos.y, pos2.z)
			fluid_hatch_output_count = fluid_hatch_output_count + 1
		end
		if data[a:index(pos2.x, pos.y, pos.z)] == id_item_output_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",pos.x, pos.y, pos2.z)
			item_hatch_output_count = item_hatch_output_count + 1
		end
		if data[a:index(pos2.x, pos.y, pos.z)] == id_energy_input_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",pos.x, pos.y, pos2.z)
			energy_hatch_input_count = energy_hatch_input_count + 1
		end
		if data[a:index(pos2.x, pos.y, pos.z)] == id_fluid_output_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, pos.y, pos2.z)
			fluid_hatch_output_count = fluid_hatch_output_count + 1
		end
		--end look for hacth begin look for other nodes
		for z = pos1.x, pos2.z,pos2.z-pos1.z  do
			for x = pos1.x+1,pos2.x-1 , 1 do
				local vi = a:index(x, pos.y, z)
				if not (data[vi] == id_other or data[vi] == id_item_input_hatch or data[vi] == id_item_output_hatch or data[vi] == id_energy_input_hatch or data[vi] == id_fluid_output_hatch) then
					corect = false
				end
			end		
		end
		for y = pos1.y, pos2.y,pos2.y-pos1.y  do
			for x = pos1.x+1,pos2.x-1 , 1 do
				local vi = a:index(x, y, pos.z)
				if not (data[vi] == id_other or data[vi] == id_item_input_hatch or data[vi] == id_item_output_hatch or data[vi] == id_energy_input_hatch or data[vi] == id_fluid_output_hatch) then
					corect = false
				end
			end		
		end
		if not (data[a:index(pos2.x, pos.y, pos.z)] == id_other or data[a:index(pos2.x, pos.y, pos.z)] == id_item_input_hatch or data[a:index(pos2.x, pos.y, pos.z)] == id_item_output_hatch 
		or data[a:index(pos2.x, pos.y, pos.z)] == id_energy_input_hatch or data[a:index(pos2.x, pos.y, pos.z)] == id_fluid_output_hatch) then 
			corect = false
		end
		if  item_hatch_input_count~=1 or item_hatch_output_count~=1 or energy_hatch_input_count~=1 or fluid_hatch_output_count~=1  then--get need count of hatchs
			local meta = minetest.get_meta(pos)
			minetest.chat_send_all(rotation.."eror!")
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",0,0,0)
		end
		return corect
	end
	if rotation==3 then --x+1 
		local corect =true 
		local vm = minetest.get_voxel_manip()
		local pos1,pos2 = {x=pos.x-4,y=pos.y-1,z=pos.z-1},{x=pos.x,y=pos.y+1,z=pos.z+1}
		local emin, emax = vm:read_from_map(pos1,pos2)
		local a = VoxelArea:new{
			MinEdge = emin,
			MaxEdge = emax
		}
		local data = vm:get_data()
		-- begin frames
		for x = pos1.x, pos2.x do
			for y = pos1.y,pos2.y,pos2.y-pos1.y do
				for z = pos1.z,pos2.z,pos2.x-pos1.z do
					local vi = a:index(x, y, z)
					if data[vi] ~= id_frame then
						minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
						corect = false
					end
				end
			end
		end
		for y = pos1.y,pos2.y,pos2.y-pos1.y do
			for x = pos1.x,pos2.x,pos2.x-pos1.x do
				local vi = a:index(x, y, pos.z)
				if data[vi] ~= id_frame then
					minetest.chat_send_all("error"..x.."--"..pos1.y.."--"..pos1.z)
					corect = false
				end
			end
		end
		for z = pos1.z,pos2.z,pos2.z-pos1.z do
			for x = pos1.x,pos2.x,pos2.x-pos1.x do
				local vi = a:index(x, pos1.y+1, z)
				if data[vi] ~= id_frame then
					minetest.chat_send_all("error"..pos1.x.."--"..pos1.y.."--"..z)
					corect = false
				end
			end
		end
		-- end frames -begin entrails
		for x = pos1.x+1,pos2.x-1 , 1 do
			local vi = a:index(x, pos.y, pos.z)
			if data[vi] ~= id_entrails then
				minetest.chat_send_all("error"..x.."--"..pos1.y.."--"..pos1.z)
				corect = false
			end
		end
		-- end entrails look for hatch
		for z = pos1.z, pos2.z,pos2.z-pos1.z  do
			for x = pos1.x+1,pos2.x-1 , 1 do
				local vi = a:index(x, pos.y, z)
				if data[vi] == id_item_input_hatch then
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",x,pos.y,z)
					item_hatch_input_count = item_hatch_input_count + 1
				end
				if data[vi] == id_item_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",x,pos.y,z)
					item_hatch_output_count = item_hatch_output_count + 1
				end
				if data[vi] == id_energy_input_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",x,pos.y,z)
					energy_hatch_input_count = energy_hatch_input_count + 1
				end
				if data[vi] == id_fluid_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",x,pos.y,z)
					fluid_hatch_output_count = fluid_hatch_output_count + 1
				end
			end		
		end
		for y = pos1.y, pos2.y,pos2.y-pos1.y  do
			for x = pos1.x+1,pos2.x-1 , 1 do
				local vi = a:index(x, y, pos.z)
				if data[vi] == id_item_input_hatch then
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",x, y, pos.z)
					item_hatch_input_count = item_hatch_input_count + 1
				end
				if data[vi] == id_item_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",x, y, pos.z)
					item_hatch_output_count = item_hatch_output_count + 1
				end
				if data[vi] == id_energy_input_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",x, y, pos.z)
					energy_hatch_input_count = energy_hatch_input_count + 1
				end
				if data[vi] == id_fluid_output_hatch then 
					owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",x, y, pos.z)
					fluid_hatch_output_count = fluid_hatch_output_count + 1
				end
			end		
		end
		if data[a:index(pos2.x, pos.y, pos.z)] == id_item_input_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, pos.y, pos2.z)
			fluid_hatch_output_count = fluid_hatch_output_count + 1
		end
		if data[a:index(pos2.x, pos.y, pos.z)] == id_item_output_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",pos.x, pos.y, pos2.z)
			item_hatch_output_count = item_hatch_output_count + 1
		end
		if data[a:index(pos2.x, pos.y, pos.z)] == id_energy_input_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",pos.x, pos.y, pos2.z)
			energy_hatch_input_count = energy_hatch_input_count + 1
		end
		if data[a:index(pos2.x, pos.y, pos.z)] == id_fluid_output_hatch then 
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",pos.x, pos.y, pos2.z)
			fluid_hatch_output_count = fluid_hatch_output_count + 1
		end
		--end look for hacth begin look for other nodes
		for z = pos1.x, pos2.z,pos2.z-pos1.z  do
			for x = pos1.x+1,pos2.x-1 , 1 do
				local vi = a:index(x, pos.y, z)
				if not (data[vi] == id_other or data[vi] == id_item_input_hatch or data[vi] == id_item_output_hatch or data[vi] == id_energy_input_hatch or data[vi] == id_fluid_output_hatch) then
					corect = false
				end
			end		
		end
		for y = pos1.y, pos2.y,pos2.y-pos1.y  do
			for x = pos1.x+1,pos2.x-1 , 1 do
				local vi = a:index(x, y, pos.z)
				if not (data[vi] == id_other or data[vi] == id_item_input_hatch or data[vi] == id_item_output_hatch or data[vi] == id_energy_input_hatch or data[vi] == id_fluid_output_hatch) then
					corect = false
				end
			end		
		end
		if not (data[a:index(pos1.x, pos.y, pos.z)] == id_other or data[a:index(pos1.x, pos.y, pos.z)] == id_item_input_hatch or data[a:index(pos1.x, pos.y, pos.z)] == id_item_output_hatch 
		or data[a:index(pos1.x, pos.y, pos.z)] == id_energy_input_hatch or data[a:index(pos1.x, pos.y, pos.z)] == id_fluid_output_hatch) then 
			corect = false
		end 
		if  item_hatch_input_count~=1 or item_hatch_output_count~=1 or energy_hatch_input_count~=1 or fluid_hatch_output_count~=1  then--get need count of hatchs
			local meta = minetest.get_meta(pos)
			minetest.chat_send_all(rotation.."eror!")
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_input",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"item_hatch_output",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"energy_hatch_input",0,0,0)
			owl_tech:set_hatch_ofset(meta,1,"fluid_hatch_output",0,0,0)
		end
		return corect
	end
end
--wood pyrolyze controller
minetest.register_node("owl_tech:wood_pyrolis", {
	description = S("Wood pyrolis controller"),
	_doc_items_longdesc = S("A more efficient way to turn wood into coal"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_macerator_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,controller_node=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_int("recipt_type",0) --0 nil  1 wood in charcola 2 biomas in solid_biofuel
        owl_tech.set_mashine_tire(meta,2) --all steam mishine has 1 tire
		owl_tech.reset_item_redines(meta)
		local timer =minetest.get_node_timer(pos)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		meta:set_int("b_corect",0)
		if bronze_blast_furnanse_build_correct(pos) then
			meta:set_int("b_corect",1)
		end
		if meta:get_int("b_corect")==1 then
			local input_stack = owl_tech:get_item_in_input_hatch_pos(
				owl_tech:get_hatch_ofset_X(meta,1,"item_hatch_input"),
				owl_tech:get_hatch_ofset_Y(meta,1,"item_hatch_input"),
				owl_tech:get_hatch_ofset_Z(meta,1,"item_hatch_input")
			)
			local output_stack = owl_tech:get_item_in_input_output_pos(
				owl_tech:get_hatch_ofset_X(meta,1,"item_hatch_output"),
				owl_tech:get_hatch_ofset_Y(meta,1,"item_hatch_output"),
				owl_tech:get_hatch_ofset_Z(meta,1,"item_hatch_output")
			)
			local input_energy = owl_tech:get_charge_in_hatch_pos(
				owl_tech:get_hatch_ofset_X(meta,1,"energy_hatch_input"),
				owl_tech:get_hatch_ofset_Y(meta,1,"energy_hatch_input"),
				owl_tech:get_hatch_ofset_Z(meta,1,"energy_hatch_input")
			)
			local fluid_output = owl_tech:get_volume_amount_in_hatch_pos(
				owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
				owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
				owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output")
			)
			--woood pyrolis
			if owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100 then
				if input_stack:get_count()>=4 and (output_stack:get_free_space()>=6 or output_stack:is_empty())
				and minetest.get_item_group(input_stack:get_name(),"tree")>0 and (output_stack:get_name()=="mcl_core:charcoal_lump" or output_stack:is_empty())  
				and input_energy>=100
				and owl_tech:get_max_volume_in_hatch_pos(
				owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
				owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
				owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"))-fluid_output>=250
				and meta:get_int("recipt_type")==1 then --has bonus coal 
					owl_tech.add_item_redines(meta,5)
					owl_tech:set_charge_in_hatch_pos(owl_tech:get_hatch_ofset_X(meta,1,"energy_hatch_input"),owl_tech:get_hatch_ofset_Y(meta,1,"energy_hatch_input"),owl_tech:get_hatch_ofset_Z(meta,1,"energy_hatch_input")
					,input_energy-5)
				end
			end
			if owl_tech.get_item_redines(meta)==0 and input_stack:get_count()>=4 and (output_stack:get_free_space()>=6 or output_stack:is_empty())
			and minetest.get_item_group(input_stack:get_name(),"tree")>0 and (output_stack:get_name()=="mcl_core:charcoal_lump" or output_stack:is_empty())  
			and input_energy>=100
			and owl_tech:get_max_volume_in_hatch_pos(
			owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
			owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
			owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"))-fluid_output>=250
			and meta:get_int("recipt_type")==0 then --has bonus coal 
				meta:set_int("recipt_type",1)
				owl_tech.add_item_redines(meta,5)
				owl_tech:set_charge_in_hatch_pos(owl_tech:get_hatch_ofset_X(meta,1,"energy_hatch_input"),owl_tech:get_hatch_ofset_Y(meta,1,"energy_hatch_input"),owl_tech:get_hatch_ofset_Z(meta,1,"energy_hatch_input")
				,input_energy-5)
			end
			if owl_tech.get_item_redines(meta)>=100 and meta:get_int("recipt_type")==1 then
				if not output_stack:is_empty() then-- Add  main output 
					output_stack:set_count(output_stack:get_count()+6) 
					owl_tech:set_item_in_input_output_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"item_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"item_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"item_hatch_output"),output_stack
					)
				else
					local item ={name="mcl_core:charcoal_lump", count=6, wear=0, metadata=""}
					output_stack:add_item(item)
					owl_tech:set_item_in_input_output_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"item_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"item_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"item_hatch_output"),output_stack
					)
				end
				input_stack:set_count(input_stack:get_count()-4)
				owl_tech:set_item_in_input_hatch_pos(
					owl_tech:get_hatch_ofset_X(meta,1,"item_hatch_input"),
					owl_tech:get_hatch_ofset_Y(meta,1,"item_hatch_input"),
					owl_tech:get_hatch_ofset_Z(meta,1,"item_hatch_input"),input_stack
				)
				if fluid_output==0 then ---Dont forget add fluid TODO
					owl_tech:set_volume_amount_in_hatch_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"),250)
					owl_tech:set_name_fluid_in_hatch_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"),"owl_tech:tar_source")
				else
					owl_tech:set_volume_amount_in_hatch_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"),fluid_output+250)
				end
				meta:set_int("recipt_type",0) 
				owl_tech.reset_item_redines(meta)
			end
			--biomas make 
			if owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100 then
				if input_stack:get_count()>=4 and (output_stack:get_free_space()>=6 or output_stack:is_empty())
				and input_stack:get_name()=="owl_tech:wood_and_biomass" and (output_stack:get_name()=="owl_tech:solid_biofuel" or output_stack:is_empty())  
				and input_energy>=100
				and owl_tech:get_max_volume_in_hatch_pos(
				owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
				owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
				owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"))-fluid_output>=250
				and meta:get_int("recipt_type")==2 then --has bonus coal 
					owl_tech.add_item_redines(meta,5)
					owl_tech:set_charge_in_hatch_pos(owl_tech:get_hatch_ofset_X(meta,1,"energy_hatch_input"),owl_tech:get_hatch_ofset_Y(meta,1,"energy_hatch_input"),owl_tech:get_hatch_ofset_Z(meta,1,"energy_hatch_input")
					,input_energy-5)
				end
			end
			if owl_tech.get_item_redines(meta)==0 and input_stack:get_count()>=4 and (output_stack:get_free_space()>=6 or output_stack:is_empty())
			and input_stack:get_name()=="owl_tech:wood_and_biomass" and (output_stack:get_name()=="owl_tech:solid_biofuel" or output_stack:is_empty())   
			and input_energy>=100
			and owl_tech:get_max_volume_in_hatch_pos(
			owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
			owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
			owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"))-fluid_output>=250
			and meta:get_int("recipt_type")==0 then --has bonus coal 
				meta:set_int("recipt_type",2)
				owl_tech.add_item_redines(meta,5)
				owl_tech:set_charge_in_hatch_pos(owl_tech:get_hatch_ofset_X(meta,1,"energy_hatch_input"),owl_tech:get_hatch_ofset_Y(meta,1,"energy_hatch_input"),owl_tech:get_hatch_ofset_Z(meta,1,"energy_hatch_input")
				,input_energy-5)
			end
			if owl_tech.get_item_redines(meta)>=100 and meta:get_int("recipt_type")==2 then
				if not output_stack:is_empty() then-- Add  main output 
					output_stack:set_count(output_stack:get_count()+6) 
					owl_tech:set_item_in_input_output_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"item_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"item_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"item_hatch_output"),output_stack
					)
				else
					local item ={name="owl_tech:solid_biofuel", count=6, wear=0, metadata=""}
					output_stack:add_item(item)
					owl_tech:set_item_in_input_output_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"item_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"item_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"item_hatch_output"),output_stack
					)
				end
				input_stack:set_count(input_stack:get_count()-4)
				owl_tech:set_item_in_input_hatch_pos(
					owl_tech:get_hatch_ofset_X(meta,1,"item_hatch_input"),
					owl_tech:get_hatch_ofset_Y(meta,1,"item_hatch_input"),
					owl_tech:get_hatch_ofset_Z(meta,1,"item_hatch_input"),input_stack
				)
				if fluid_output==0 then 
					owl_tech:set_volume_amount_in_hatch_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"),250)
					owl_tech:set_name_fluid_in_hatch_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"),"owl_tech:tar_source")
				else
					owl_tech:set_volume_amount_in_hatch_pos(
						owl_tech:get_hatch_ofset_X(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Y(meta,1,"fluid_hatch_output"),
						owl_tech:get_hatch_ofset_Z(meta,1,"fluid_hatch_output"),fluid_output+250)
				end
				meta:set_int("recipt_type",0) 
				owl_tech.reset_item_redines(meta)
			end
		end
		set_formspect_wood_pyrolis(meta)
		timer:start(0.2)
	end
})
--Crafte controller 
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:wood_pyrolis",
    recipe = {
        {"owl_tech:circuit_tire_1","owl_tech:steel_plate","owl_tech:circuit_tire_1"},
        {"owl_tech:steel_plate","owl_tech:circuit_tire_1","owl_tech:steel_plate"},
        {"owl_tech:steel_plate","owl_tech:steel_plate","owl_tech:steel_plate"}
    }
})