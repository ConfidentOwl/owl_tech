--List of all tools like ( hammers , file ets) 

local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

dofile(minetest.get_modpath("owl_tech") .. "/tools/electro_tools.lua") --electro tools load

--Work hammer
minetest.register_craftitem("owl_tech:work_hammer", {
	description = S("Work hammer"),
	_doc_items_longdesc = S("First way to get plater"),
	inventory_image = "owl_tech_work_hammer_head.png^owl_tech_hoe_stick.png",
	stack_max = 1,
	groups = { craftitem = 1  },
})
--Work file for crafte
minetest.register_craftitem("owl_tech:work_file", {
	description = S("Work file"),
	_doc_items_longdesc = S("First way to get plater"),
	inventory_image = "owl_tech_work_file.png",
	stack_max = 1,
	groups = { craftitem = 1  },
})

--Work mortar
minetest.register_craftitem("owl_tech:work_mortar", {
	description = S("Work mortar"),
	_doc_items_longdesc = S("First way to get dusts"),
	inventory_image = "owl_tech_mortar.png",
	stack_max = 1,
	groups = { craftitem = 1  },
}) 
local mortal_metalls_list={
    "owl_tech:iron_ingot","owl_tech:copper_ingot","owl_tech:tin_ingot","owl_tech:bronze_ingot","mcl_core:iron_ingot"
}
for i, value in ipairs(mortal_metalls_list) do
    --Crafte worlk file
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:work_mortar",
        recipe = {
            {"",mortal_metalls_list[i],""},
            {"mcl_core:stone",mortal_metalls_list[i],"mcl_core:stone"},
            {"mcl_core:stone","mcl_core:stick","mcl_core:stone"}
        }  
    })
    --Crafte wrench
    minetest.register_craft({
    type = "shaped",
    output = "owl_tech:wrench",
    recipe = {
        {mortal_metalls_list[i],"",mortal_metalls_list[i]},
        {mortal_metalls_list[i],mortal_metalls_list[i],mortal_metalls_list[i]},
        {"",mortal_metalls_list[i],""}
    }  
    })
    --Crafte worlk file
    minetest.register_craft({
    type = "shaped",
    output = "owl_tech:work_file",
    recipe = {
        {"",mortal_metalls_list[i],""},
        {"",mortal_metalls_list[i],""},
        {"","mcl_core:stick",""}
    }  
    })
    --Crafte work hammer
    minetest.register_craft({
    type = "shaped",
    output = "owl_tech:work_hammer",
    recipe = {
        {mortal_metalls_list[i],mortal_metalls_list[i],""},
        {mortal_metalls_list[i],mortal_metalls_list[i],""},
        {"","mcl_core:stick",""}
    }  
    })
end
--Work mortar
minetest.register_craftitem("owl_tech:wrench", {
	description = S("Wrench"),
	_doc_items_longdesc = S("Use for crafte and some confige machins"),
	inventory_image = "owl_tech_wrench.png",
	stack_max = 1,
	groups = { craftitem = 1,hand_wrench=1},
}) 
