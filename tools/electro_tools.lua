local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

minetest.register_tool("owl_tech:base_drill", {
    description = S("Base drill"),
    _doc_items_longdesc = "Pick from owl tech",
    inventory_image = "owl_tech_base_drill.png",
    wield_scale =  mcl_vars.tool_wield_scale,
    groups = { tool=1, pickaxe=1, dig_speed_class=4, enchantability=14 ,electro_tool=1},
    tool_capabilities = {
        -- 1/1.2
        full_punch_interval = 0.83333333,
        max_drop_level=4,
        damage_groups = {fleshy=4},
        punch_attack_uses = (16),
    },
    sound = { breaks = "default_tool_breaks" },
    _mcl_toollike_wield = true,
    _mcl_diggroups = {
        pickaxey = { speed = 6, level = 4, uses = 16*2}
    },         
})
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:base_drill",
    recipe = {
        {"owl_tech:steel_plate","owl_tech:tin_plate","owl_tech:steel_plate"},
        {"owl_tech:bronze_plate","owl_tech:circuit_tire_1","owl_tech:tin_plate"},
        {"owl_tech:circuit_tire_1","owl_tech:bronze_plate","owl_tech:steel_plate"}
    }
})