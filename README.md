# Owl tech

Large tech mod specially developed for Mineclone 2

***
Mod Features:
* Reworked generation of ores - Now the ores appear in the world in large veins .Each ore vein has its own set of ores, spawn frequency and amount of ore
  
* Ore processing system. The higher the level of mechanisms will be used, the more resources will be obtained from the ore

* Fluid and Energy Transfer System

* Geologist exploration systems from millet hammers that will tell you about the ores around the player - to open pits
  
***
## Ore

Do not forget to turn off the generation of ordinary ores in the settingtypes.txt file
on the line mcl_generate_ores (Generate Ores) set instead of true -> falsemcl_generate_ores (Generate Ores) bool false

P.S.
Disabling ores in Mineclone 2 does not work yet(this is a bug and needs to be fixed)
P.S.S
Incompatible with  Mineclone2 More Ore Variants

***
## Metals

The mod adds both a large number of metals - copper, tin, silver, and so on, as well as an alloy, for example, electrum, bronze, steel, and so on.

***
## Instruments

Removed original tools except for wood and stone (unless you find them in chests in the world as loot). Now, in order to get metal tools, you need to create them according to new recipes with plates and various additional tools - such as a hammer or a file.

***

## Pipe

There is a mechanic of transferring liquids (such as water, lava, steam, etc.) through pipes from one mechanism to another. Pipes work on the principle of where there is less liquid there and send the liquid.

There is a mechanics of item pipes. Item transfer only works with regular chests and mechanisms from the mod itself(WIP)

## Mashins

Add base steam mashine and electrick mashins ( like generators , furnance , alliy smelters and etc)The higher the level of the machine, the more new recipes are available and the machine works faster - but the consumption of energy and attracted resources also rises, which is necessary for the creation.

all mechanisms have red and blue marks for - these are inputs and outputs for energy / liquid / items
***

## Automation

All electrical machines can be automated with the help of pipes, cables.

***
## Multiblocks
Multiblocks - not just large machines, you can strictly build on inturctions - large and large machines in which you can change the input and output nodes
   1)Pyrolysis oven -converts wood into charcoal and wood tars
    Node List -
        0)1-owl_tech:wood_pyrolis -C
        1)1-owl_tech:base_item_hatch_input
        2)1-owl_tech:base_item_hatch_output
        3)1-owl_tech:base_energy_hatch_input
        4)1-owl_tech:base_fluid_hatch_output
        5)2-owl_tech:copper_grid -Z
        6)6-owl_tech:tin_tiles -Y
        7)28-owl_tech:steel_big_tiles- X
    Assembly instructions
    1)
        XXXXX
        XYYYX
        XXXXX
    2)
        XYYYX
        CZZZY
        XYYYX
    3)
        XXXXX
        XYYYX
        XXXXX
    Instead of Y, you can put hatch in any order
***
Warning - mod in deep development (install it in your game worlds only at your own peril and risk)
