local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

local xray={
    "mcl_core:gravel","mcl_core:dirt","mcl_core:diorite","mcl_core:andesite","mcl_core:granite","mcl_core:sand","mcl_core:dirt_with_grass","mcl_core:sandstone","mcl_core:stone",
}
for i, value in ipairs(xray) do
    minetest.override_item(xray[i],{
        drawtype="glasslike",
        paramtype = "light",
        walkable=false,
        tiles = {"owl_tech_xray.png"},
        light_source = 10,
        })

end