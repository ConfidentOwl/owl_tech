---Integration for i3 FORGE_HAMMER_RECIPS=


i3.register_craft_type("alloy_smelter", {
	description = "Alloy smetler",
	icon = "owl_tech_smelter_face.png",
})

i3.register_craft_type("macerator", { --SIEV_RESIPS
	description = "Macerator",
	icon = "owl_tech_macerator_face.png",
})

i3.register_craft_type("siev", {
	description = "Siev",
	icon = "owl_tech_sieve_face.png",
})

i3.register_craft_type("forge_hammer", { --SIEV_RESIPS
	description = "Forge hammer",
	icon = "owl_tech_forge_face.png",
})

for i, value in pairs(SMELTER_RECIPS) do
    i3.register_craft {
        type   = "alloy_smelter",
        result = value[3]..' '..value[4],
        items  = {value[7]..' '..value[1],value[8]..' '..value[2]},
    }
end

for i, value in pairs(MACERATO_RECIPS) do
    i3.register_craft {
        type   = "macerator",
        result = value[3]..' '..value[4],
        items  = {value[9]..' '..value[1]},
    }
end

for i, value in pairs(SIEV_RESIPS) do
    i3.register_craft {
        type   = "siev",
        result = value[3]..' '..value[4],
        items  = {value[9]..' '..value[1]},
    }
end

for i, value in pairs(FORGE_HAMMER_RECIPS) do
    i3.register_craft {
        type   = "forge_hammer",
        result = value[3]..' '..value[4],
        items  = {value[9]..' '..value[1]},
    }
end
