local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)
local use_select_box = minetest.settings:get_bool("mcl_buckets_use_select_box", false)

local steelHammerDigs = {}

minetest.register_on_dignode(  -- take from Hammermod by  cultom  https://content.minetest.net/packages/cultom/hammermod/
  function(pos, oldnode, digger)
    if digger == nil or not ( minetest.get_item_group(digger:get_wielded_item():get_name(),"miner_hammer")>0) then
      return
    end
    
    local playerName = digger:get_player_name()
    if(playerName == ""  or steelHammerDigs[playerName]) then
      return
    end
    steelHammerDigs[playerName] = true

    local posDiff = pos.y - digger:get_pos().y
    if(posDiff < 2 and posDiff > 1) then
      if
        digger:get_look_horizontal() > math.pi / 4 and digger:get_look_horizontal() < 3 * math.pi / 4 or
        digger:get_look_horizontal() > 5 * math.pi / 4 and digger:get_look_horizontal() < 7 * math.pi / 4
      then
        pos.y = pos.y -1
        pos.z = pos.z -1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.y = pos.y +1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.y = pos.y +1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.z = pos.z +1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.z = pos.z +1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.y = pos.y -1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.y = pos.y -1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.z = pos.z -1
        minetest.node_dig(pos, minetest.get_node(pos), digger)
      else
        pos.y = pos.y -1
        pos.x = pos.x -1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.y = pos.y +1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.y = pos.y +1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.x = pos.x +1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.x = pos.x +1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.y = pos.y -1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.y = pos.y -1
        minetest.node_dig(pos, minetest.get_node(pos), digger)

        pos.x = pos.x -1
        minetest.node_dig(pos, minetest.get_node(pos), digger)
      end
    else
      pos.x = pos.x -1
      pos.z = pos.z -1
      minetest.node_dig(pos, minetest.get_node(pos), digger)

      pos.x = pos.x +1
      minetest.node_dig(pos, minetest.get_node(pos), digger)

      pos.x = pos.x +1
      minetest.node_dig(pos, minetest.get_node(pos), digger)

      pos.z = pos.z +1
      minetest.node_dig(pos, minetest.get_node(pos), digger)

      pos.z = pos.z +1
      minetest.node_dig(pos, minetest.get_node(pos), digger)

      pos.x = pos.x -1
      minetest.node_dig(pos, minetest.get_node(pos), digger)

      pos.x = pos.x -1
      minetest.node_dig(pos, minetest.get_node(pos), digger)

      pos.z = pos.z -1
      minetest.node_dig(pos, minetest.get_node(pos), digger)

    end
    steelHammerDigs[playerName] = nil

  end
)
---- This crutch was suggested by the developers of Mineclone 2 - they made the functions of their shovels and hoes localS
local function create_soil(pos, inv)
	if pos == nil then
		return false
	end
	local node = minetest.get_node(pos)
	local name = node.name
	local above = minetest.get_node({x=pos.x, y=pos.y+1, z=pos.z})
	if minetest.get_item_group(name, "cultivatable") == 2 then
		if above.name == "air" then
			node.name = "mcl_farming:soil"
			minetest.set_node(pos, node)
			minetest.sound_play("default_dig_crumbly", { pos = pos, gain = 0.5 }, true)
			return true
		end
	elseif minetest.get_item_group(name, "cultivatable") == 1 then
		if above.name == "air" then
			node.name = "mcl_core:dirt"
			minetest.set_node(pos, node)
			minetest.sound_play("default_dig_crumbly", { pos = pos, gain = 0.6 }, true)
			return true
		end
	end
	return false
end

local make_grass_path = function(itemstack, placer, pointed_thing)
	-- Use pointed node's on_rightclick function first, if present
	local node = minetest.get_node(pointed_thing.under)
	if placer and not placer:get_player_control().sneak then
		if minetest.registered_nodes[node.name] and minetest.registered_nodes[node.name].on_rightclick then
			return minetest.registered_nodes[node.name].on_rightclick(pointed_thing.under, node, placer, itemstack) or itemstack
		end
	end

	-- Only make grass path if tool used on side or top of target node
	if pointed_thing.above.y < pointed_thing.under.y then
		return itemstack
	end

	if (minetest.get_item_group(node.name, "path_creation_possible") == 1) then
		local above = table.copy(pointed_thing.under)
		above.y = above.y + 1
		if minetest.get_node(above).name == "air" then
			if minetest.is_protected(pointed_thing.under, placer:get_player_name()) then
				minetest.record_protection_violation(pointed_thing.under, placer:get_player_name())
				return itemstack
			end

			if not minetest.is_creative_enabled(placer:get_player_name()) then
				-- Add wear (as if digging a shovely node)
				local toolname = itemstack:get_name()
				local wear = mcl_autogroup.get_wear(toolname, "shovely")
				itemstack:add_wear(wear)
			end
			minetest.sound_play({name="default_grass_footstep", gain=1}, {pos = above}, true)
			minetest.swap_node(pointed_thing.under, {name="mcl_core:grass_path"})
		end
	end
	return itemstack
end

-- Axes
local function make_stripped_trunk(itemstack, placer, pointed_thing)
    if pointed_thing.type ~= "node" then return end

    local node = minetest.get_node(pointed_thing.under)
    local noddef = minetest.registered_nodes[minetest.get_node(pointed_thing.under).name]

    if not placer:get_player_control().sneak and noddef.on_rightclick then
        return minetest.item_place(itemstack, placer, pointed_thing)
    end
    if minetest.is_protected(pointed_thing.under, placer:get_player_name()) then
        minetest.record_protection_violation(pointed_thing.under, placer:get_player_name())
        return itemstack
    end

    if noddef._mcl_stripped_variant == nil then
		return itemstack
	else
		minetest.swap_node(pointed_thing.under, {name=noddef._mcl_stripped_variant, param2=node.param2})
		if not minetest.is_creative_enabled(placer:get_player_name()) then
			-- Add wear (as if digging a axey node)
			local toolname = itemstack:get_name()
			local wear = mcl_autogroup.get_wear(toolname, "axey")
			itemstack:add_wear(wear)
		end
	end
    return itemstack
end

local hoe_on_place_function = function(wear_divisor)  --Copy-past from mcl_farming ( in mc it local func adn can 't access to mod ')
	return function(itemstack, user, pointed_thing)
		-- Call on_rightclick if the pointed node defines it
		local node = minetest.get_node(pointed_thing.under)
		if user and not user:get_player_control().sneak then
			if minetest.registered_nodes[node.name] and minetest.registered_nodes[node.name].on_rightclick then
				return minetest.registered_nodes[node.name].on_rightclick(pointed_thing.under, node, user, itemstack) or itemstack
			end
		end

		if minetest.is_protected(pointed_thing.under, user:get_player_name()) then
			minetest.record_protection_violation(pointed_thing.under, user:get_player_name())
			return itemstack
		end

		if create_soil(pointed_thing.under, user:get_inventory()) then
			if not minetest.is_creative_enabled(user:get_player_name()) then
				itemstack:add_wear(65535/wear_divisor)
			end
			return itemstack
		end
	end
end

-- 1)tech_name 2)useal name 3)ineed ore ? 4)pickasxe_level  5)color  6)intrument uses 7)can simple burn ore in furnance ( and dusts)
local  metals_ore_array={
    {"iron","Iron",true,3,"#f7f7f7",125,true},
    {"copper","Copper",true,2,"#ff5e00",75,true},
    {"tin","Tin",true,2,"#c9c9c9",88,true},
    {"gold","Golg",true,2,"#ffe600",50,true},
    {"silver","Silver",true,3,"#d1d1d1",62,true},
    {"lead","Lead",true,3,"#9092ab",62,true},
    {"steel","Steel",false,3,"#575757",300,false},
    {"bronze","Bronze",false,3,"#a35900",200,true},
    {"electrum","Electrum",false,2,"#fffd73",55,true},
}
--make metals ALL
for i, value in ipairs(metals_ore_array) do
    --ore   
    if metals_ore_array[i][3] then
        minetest.register_node("owl_tech:"..metals_ore_array[i][1].."_ore", {
            description = S(metals_ore_array[i][2].." ore"),
            _doc_items_longdesc = S(metals_ore_array[i][2]..' ore'),
            _doc_items_hidden = false,
            tiles = {"default_stone.png^(owl_tech_ore_base.png^[colorize:"..metals_ore_array[i][5]..":128)"},
            is_ground_content = true,
            stack_max = 64,
            groups = {pickaxey=metals_ore_array[i][4], building_block=1, material_stone=1, blast_furnace_smeltable=1,ore=1},
            drop = "owl_tech:"..metals_ore_array[i][1].."_ore",
            sounds = mcl_sounds.node_sound_stone_defaults(),
            _mcl_blast_resistance = 3,
            _mcl_hardness = 3,
            _mcl_silk_touch_drop = true,
        })  
    end
    --Nugget
    minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_nugget", {
        description = S(metals_ore_array[i][2].. " Nugget"),
        _doc_items_longdesc = S(metals_ore_array[i][2].. " Nugget"),
        inventory_image = "mcl_core_iron_nugget.png^[colorize:"..metals_ore_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Ingot
    minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_ingot", {
        description = S(metals_ore_array[i][2].. " Ingot"),
        _doc_items_longdesc = S(metals_ore_array[i][2].. " Ingot"),
        inventory_image = "default_steel_ingot.png^[colorize:"..metals_ore_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --dust    owl_tech_lamp_base.png       owl_tech_lamp_part.png 
    minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_dust", {
        description = S(metals_ore_array[i][2].. " dust"),
        _doc_items_longdesc = S(metals_ore_array[i][2].. " dust"),
        inventory_image = "owl_tech_dust.png^[colorize:"..metals_ore_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Cafte dust 
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:"..metals_ore_array[i][1].."_dust",
        recipe = {"owl_tech:work_mortar","owl_tech:"..metals_ore_array[i][1].."_ingot"},
        replacements = {
            {"owl_tech:work_mortar","owl_tech:work_mortar"},
        }
    })
    --Sharp blade
    minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_sharp_blade", {
        description = S(metals_ore_array[i][2].. " sharp blade"),
        _doc_items_longdesc = S("simple and reliable sharp blade element"),
        inventory_image = "owl_tech_sharp_blade.png^[colorize:"..metals_ore_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Cafte Sharp blade
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..metals_ore_array[i][1].."_sharp_blade 3",
        recipe = {
            {"owl_tech:work_hammer","owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot"},
            {"owl_tech:work_file","owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot"},
            {"","",""}
        },
        replacements = {
            {"owl_tech:work_hammer", "owl_tech:work_hammer"},
            {"owl_tech:work_file", "owl_tech:work_file"},
        }
    })
    --lamp    owl_tech_sharp_blade.png 
    minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_lamp", {
        description = S(metals_ore_array[i][2].. " lamp"),
        _doc_items_longdesc = S("simple and reliable electrical circuit element"),
        inventory_image = "owl_tech_lamp_base.png^(owl_tech_lamp_part.png^[colorize:"..metals_ore_array[i][5]..":128)",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Cafte lamp 
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..metals_ore_array[i][1].."_lamp 4",
        recipe = {
            {"mcl_core:glass","owl_tech:"..metals_ore_array[i][1].."_ingot","mcl_core:glass"},
            {"mcl_core:glass","owl_tech:"..metals_ore_array[i][1].."_ingot","mcl_core:glass",},
            {"","",""}
        },
    })
    --heat element  
    minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_heat_element", {
        description = S(metals_ore_array[i][2].. " heat element"),
        _doc_items_longdesc = S("simple and reliable heat element"),
        inventory_image = "owl_tech_base_heat_element.png^[colorize:"..metals_ore_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Cafte heat element  
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..metals_ore_array[i][1].."_heat_element 2" ,
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_nugget","owl_tech:"..metals_ore_array[i][1].."_ingot"},
            {"owl_tech:work_hammer","","owl_tech:work_file",},
            {"owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_nugget","owl_tech:"..metals_ore_array[i][1].."_ingot"}
        },
        replacements = {
            {"owl_tech:work_hammer", "owl_tech:work_hammer"},
            {"owl_tech:work_file", "owl_tech:work_file"},
        }
    })   
    --mesh owl_tech_base_mesh.png
    minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_mesh", {
        description = S(metals_ore_array[i][2].. " mesh"),
        _doc_items_longdesc = S("Metallic mesh"),
        inventory_image = "owl_tech_base_mesh.png^[colorize:"..metals_ore_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Cafte mesh  
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..metals_ore_array[i][1].."_mesh 2" ,
        recipe = {
            {"mcl_mobitems:string","owl_tech:"..metals_ore_array[i][1].."_ingot","mcl_mobitems:string"},
            {"mcl_mobitems:string","owl_tech:work_file","mcl_mobitems:string"},
            {"mcl_mobitems:string","owl_tech:"..metals_ore_array[i][1].."_ingot","mcl_mobitems:string"}
        },
        replacements = {
            {"owl_tech:work_file", "owl_tech:work_file"},
        }
    })
    --dirt dust
    minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_dirt_dust", {
        description = S(metals_ore_array[i][2].. " dirt dust"),
        _doc_items_longdesc = S(metals_ore_array[i][2].. " dirt dust"),
        inventory_image = "owl_tech_dirt_dust.png^[colorize:"..metals_ore_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    if metals_ore_array[i][7] then
        --Simple burn ore in ingot
        minetest.register_craft({
		    type = "cooking",
		    output = "owl_tech:"..metals_ore_array[i][1].."_ingot",
		    recipe = "owl_tech:"..metals_ore_array[i][1].."_ore",
		    cooktime = 10,
	    })
        --Simple burn dust in ingot 
        minetest.register_craft({
		    type = "cooking",
		    output = "owl_tech:"..metals_ore_array[i][1].."_ingot",
		    recipe = "owl_tech:"..metals_ore_array[i][1].."_dust",
		    cooktime = 10,
	    })
        --burn dirt dust
        minetest.register_craft({
		    type = "cooking",
		    output = "owl_tech:"..metals_ore_array[i][1].."_ingot",
		    recipe = "owl_tech:"..metals_ore_array[i][1].."_dirt_dust",
		    cooktime = 10,
	    })
    end
    mcl_armor.register_set({
        name = metals_ore_array[i][1],
        description = metals_ore_array[i][2],
        durability = metals_ore_array[i][6]*2,
        enchantability = 9,
        points = {
            head = 2,
            torso = 6,
            legs = 5,
            feet = 2,
        },
        craft_material = "owl_tech:"..metals_ore_array[i][1].."_plate",
        cook_material = "owl_tech:"..metals_ore_array[i][1].."_nugget",
        sound_equip = "mcl_armor_equip_iron",
        sound_unequip = "mcl_armor_unequip_iron",
    })
    --plate
        minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_plate", {
        description = S(metals_ore_array[i][2].. " plate"),
        _doc_items_longdesc = S(metals_ore_array[i][2].. " plate"),
        inventory_image = "owl_tech_plate.png^[colorize:"..metals_ore_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --fence 
    local p = {-2/16, -0.5, -2/16, 2/16, 0.5, 2/16}
    local x1 = {-0.5, 4/16, -1/16, -2/16, 7/16, 1/16}   --oben(quer) -x
    local x12 = {-0.5, -2/16, -1/16, -2/16, 1/16, 1/16} --unten(quer) -x
    local x2 = {2/16, 4/16, -1/16, 0.5, 7/16, 1/16}   --oben(quer) x
    local x22 = {2/16, -2/16, -1/16, 0.5, 1/16, 1/16} --unten(quer) x
    local z1 = {-1/16, 4/16, -0.5, 1/16, 7/16, -2/16}   --oben(quer) -z
    local z12 = {-1/16, -2/16, -0.5, 1/16, 1/16, -2/16} --unten(quer) -z
    local z2 = {-1/16, 4/16, 2/16, 1/16, 7/16, 0.5}   --oben(quer) z
    local z22 = {-1/16, -2/16, 2/16, 1/16, 1/16, 0.5} --unten(quer) z

    -- Collision box
    local cp = {-2/16, -0.5, -2/16, 2/16, 1.01, 2/16}
    local cx1 = {-0.5, -0.5, -2/16, -2/16, 1.01, 2/16} --unten(quer) -x
    local cx2 = {2/16, -0.5, -2/16, 0.5, 1.01, 2/16} --unten(quer) x
    local cz1 = {-2/16, -0.5, -0.5, 2/16, 1.01, -2/16} --unten(quer) -z
    local cz2 = {-2/16, -0.5, 2/16, 2/16, 1.01, 0.5} --unten(quer) z

	minetest.register_node("owl_tech:"..metals_ore_array[i][1].."_fence", { --copypast from mc2 
		description = S(metals_ore_array[i][2].." fence"),
		_doc_items_longdesc = S("Fences are structures which block the way. Fences will connect to each other and solid blocks. They cannot be jumped over with a simple jump."),
		tiles = {"default_steel_block.png^[colorize:"..metals_ore_array[i][5]..":128"},
		inventory_image = "mcl_fences_fence_mask.png^" .. "default_steel_block.png^[colorize:"..metals_ore_array[i][5]..":128" .. "^mcl_fences_fence_mask.png^[makealpha:255,126,126",
		wield_image = "mcl_fences_fence_mask.png^" .. "default_steel_block.png^[colorize:"..metals_ore_array[i][5]..":128" .. "^mcl_fences_fence_mask.png^[makealpha:255,126,126",
		paramtype = "light",
		is_ground_content = false,
		groups = {  pickaxey= 1,  fence_wood = 1, },
		stack_max = 64,
		sunlight_propagates = true,
		drawtype = "nodebox",
		connect_sides = { "front", "back", "left", "right" },
		connects_to = {"group:fence_wood"},
		node_box = {
			type = "connected",
			fixed = {p},
			connect_front = {z1,z12},
			connect_back = {z2,z22,},
			connect_left = {x1,x12},
			connect_right = {x2,x22},
		},
		collision_box = {
			type = "connected",
			fixed = {cp},
			connect_front = {cz1},
			connect_back = {cz2,},
			connect_left = {cx1},
			connect_right = {cx2},
		},
		sounds = mcl_sounds.node_sound_metal_defaults(),
		_mcl_blast_resistance = 6,
		_mcl_hardness = 5,
	})


    --Cafte plate
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:"..metals_ore_array[i][1].."_plate",
        recipe = {"owl_tech:work_hammer","owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot"},
        replacements = {
            {"owl_tech:work_hammer", "owl_tech:work_hammer"},
        }
    })
    --pick
    minetest.register_tool("owl_tech:pick_".. metals_ore_array[i][1], {
        description = S( metals_ore_array[i][2].." Pickaxe"),
        _doc_items_longdesc = "Pick from owl tech",
        inventory_image = "(owl_tech_pick_head.png^[colorize:"..metals_ore_array[i][5]..":128)^owl_tech_pick_stick.png",         --owl_tech_pick_head.png  owl_tech_pick_stick.png
        wield_scale =  mcl_vars.tool_wield_scale,
        groups = { tool=1, pickaxe=1, dig_speed_class=4, enchantability=14},
        tool_capabilities = {
            -- 1/1.2
            full_punch_interval = 0.83333333,
            max_drop_level=4,
            damage_groups = {fleshy=4},
            punch_attack_uses = (metals_ore_array[i][6]),
        },
        sound = { breaks = "default_tool_breaks" },
        _repair_material = "owl_tech:"..metals_ore_array[i][1].."_ingot",
        _mcl_toollike_wield = true,
        _mcl_diggroups = {
            pickaxey = { speed = 6, level = 4, uses = metals_ore_array[i][6]*2}
        },         
    })
   --Pick head 
   minetest.register_craftitem("owl_tech:pick_head_"..metals_ore_array[i][1], {
        description = S("Pick head "..metals_ore_array[i][1]),
        _doc_items_longdesc = S("Pick head use for crafte pick in any place"),
        inventory_image =  "(owl_tech_pick_head.png^[colorize:"..metals_ore_array[i][5]..":128)",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Crafte pick head
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:pick_head_"..metals_ore_array[i][1],
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot"},
            {"owl_tech:work_file","","owl_tech:work_hammer",},
            {"","",""}
        },
        replacements = {
            {"owl_tech:work_hammer", "owl_tech:work_hammer"},
            {"owl_tech:work_file", "owl_tech:work_file"},
        }
    })
    --Crafte pick from pick head
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:pick_"..metals_ore_array[i][1],
        recipe = {"owl_tech:pick_head_"..metals_ore_array[i][1],"mcl_core:stick"}
    })

    --hammer
    minetest.register_tool("owl_tech:hammer_".. metals_ore_array[i][1], {
        description = S( metals_ore_array[i][2].." Hammer"),
        _doc_items_longdesc = "Hammer from owl tech",
        inventory_image = "(owl_tech_hammer_head.png^[colorize:"..metals_ore_array[i][5]..":128)^owl_tech_pick_stick.png",         --owl_tech_pick_head.png  owl_tech_pick_stick.png
        wield_scale =  mcl_vars.tool_wield_scale,
        groups = { tool=1, pickaxe=1, dig_speed_class=4, enchantability=14,miner_hammer=1 },
        tool_capabilities = {
            -- 1/1.2
            full_punch_interval = 0.83333333,
            max_drop_level=4,
            damage_groups = {fleshy=4},
            punch_attack_uses = (metals_ore_array[i][6]*5),
        },
        sound = { breaks = "default_tool_breaks" },
        _repair_material = "owl_tech:"..metals_ore_array[i][1].."_ingot",
        _mcl_toollike_wield = true,
        _mcl_diggroups = {
            pickaxey = { speed = 6, level = 4, uses = metals_ore_array[i][6]*10}
        },         
    })
   --Hammer head 
   minetest.register_craftitem("owl_tech:hammer_head_"..metals_ore_array[i][1], {
        description = S("Hammer head "..metals_ore_array[i][1]),
        _doc_items_longdesc = S("Hammer head use for crafte hammer in any place"),
        inventory_image =  "(owl_tech_hammer_head.png^[colorize:"..metals_ore_array[i][5]..":128)",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Crafte Hammer head
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:hammer_head_"..metals_ore_array[i][1],
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_plate"},
            {"owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot"},
            {"owl_tech:work_file","","owl_tech:work_hammer",}
        },
        replacements = {
            {"owl_tech:work_hammer", "owl_tech:work_hammer"},
            {"owl_tech:work_file", "owl_tech:work_file"},
        }
    })
    --Crafte hasmmer from hammer head
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:hammer_"..metals_ore_array[i][1],
        recipe = {"owl_tech:hammer_head_"..metals_ore_array[i][1],"mcl_core:stick"}
    })

    
   --axe 
    minetest.register_tool("owl_tech:axe_".. metals_ore_array[i][1], {
        description = S(metals_ore_array[i][2].." axe"),
        _doc_items_longdesc = "Axe owl tech",
        inventory_image = "(owl_tech_axe_head.png^[colorize:"..metals_ore_array[i][5]..":128)^owl_tech_pick_stick.png", 
        wield_scale = mcl_vars.tool_wield_scale,
        groups = { tool=1, axe=1, dig_speed_class=4, enchantability=14 },
        tool_capabilities = {
            -- 1/0.9
            full_punch_interval = 1.11111111,
            max_drop_level=4,
            damage_groups = {fleshy=9},
            punch_attack_uses = (metals_ore_array[i][6]),
        },
        on_place = make_stripped_trunk,
        sound = { breaks = "default_tool_breaks" },
        _repair_material = "owl_tech:"..metals_ore_array[i][1].."_ingot",
        _mcl_toollike_wield = true,
        _mcl_diggroups = {
            axey = { speed = 6, level = 4, uses = metals_ore_array[i][6]*2 }
        },
    })
    --axe head item
    minetest.register_craftitem("owl_tech:axe_head_"..metals_ore_array[i][1], {
        description = S("Axe head "..metals_ore_array[i][1]),
        _doc_items_longdesc = S("Axe head use for crafte axe in any place"),
        inventory_image =  "(owl_tech_axe_head.png^[colorize:"..metals_ore_array[i][5]..":128)",
        stack_max = 64,
        groups = { craftitem=1 },
    })
     --Crafte axe from axe head
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:axe_"..metals_ore_array[i][1],
        recipe = {"owl_tech:axe_head_"..metals_ore_array[i][1],"mcl_core:stick"}
    }) 
    --Crafte  head
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:axe_head_"..metals_ore_array[i][1],
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_ingot",""},
            {"owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:work_hammer",""},
            {"owl_tech:work_file","",""}
        },
        replacements = {
            {"owl_tech:work_hammer", "owl_tech:work_hammer"},
            {"owl_tech:work_file", "owl_tech:work_file"},
        }
    })
    --Sword 
    minetest.register_tool("owl_tech:sword_"..metals_ore_array[i][1], {
        description = S(metals_ore_array[i][2].." sword"),
        _doc_items_longdesc = "Owl tech sword",
        inventory_image = "(owl_tech_sword_blade.png^[colorize:"..metals_ore_array[i][5]..":128)^owl_tech_sword_stick.png", 
        wield_scale = mcl_vars.tool_wield_scale,
        groups = { weapon=1, sword=1, dig_speed_class=4, enchantability=14 },
        tool_capabilities = {
            full_punch_interval = 0.625,
            max_drop_level=4,
            damage_groups = {fleshy=6},
            punch_attack_uses = metals_ore_array[i][6],
        },
        sound = { breaks = "default_tool_breaks" },
        _repair_material = "owl_tech:"..metals_ore_array[i][1].."_ingot",
        _mcl_toollike_wield = true,
        _mcl_diggroups = {
            swordy = { speed = 6, level = 4, uses = metals_ore_array[i][6]*2 },
            swordy_cobweb = { speed = 6, level = 4, uses = metals_ore_array[i][6]*2 }
        },
    })
    --sword head
    minetest.register_craftitem("owl_tech:sword_head_"..metals_ore_array[i][1], {
        description = S("Sword head "..metals_ore_array[i][1]),
        _doc_items_longdesc = S("Sword head use for crafte sword in any place"),
        inventory_image =  "(owl_tech_sword_blade.png^[colorize:"..metals_ore_array[i][5]..":128)",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Crafte sword from sword head
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:sword_"..metals_ore_array[i][1],
        recipe = {"owl_tech:sword_head_"..metals_ore_array[i][1],"mcl_core:stick"}
    })
    --Crafte sword head
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:sword_head_"..metals_ore_array[i][1],
        recipe = {
            {"","owl_tech:"..metals_ore_array[i][1].."_plate",""},
            {"owl_tech:work_file","owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:work_hammer"},
            {"","",""}
        },
        replacements = {
            {"owl_tech:work_hammer", "owl_tech:work_hammer"},
            {"owl_tech:work_file", "owl_tech:work_file"},
        }
    })
    --shovel 
    minetest.register_tool("owl_tech:shovel_"..metals_ore_array[i][1], {
        description = S(metals_ore_array[i][2].." Shovel"),
        _doc_items_longdesc = "Owl tech shovel",
        _doc_items_usagehelp = shovel_use,
        inventory_image = "(owl_tech_shovel_head.png^[colorize:"..metals_ore_array[i][5]..":128)^owl_tech_shovel_stick.png",
        wield_scale = wield_scale,
        groups = { tool=1, shovel=1, dig_speed_class=4, enchantability=14 },
        tool_capabilities = {
            full_punch_interval = 1,
            max_drop_level=4,
            damage_groups = {fleshy=4},
            punch_attack_uses = metals_ore_array[i][6],
        },
        on_place = make_grass_path,
        sound = { breaks = "default_tool_breaks" },
        _repair_material = "owl_tech:"..metals_ore_array[i][1].."_ingot",
        _mcl_toollike_wield = true,
        _mcl_diggroups = {
            shovely = { speed = 6, level = 4, uses = metals_ore_array[i][6]*2  }
        },
    })
    --Shovel head
    minetest.register_craftitem("owl_tech:shovel_head_"..metals_ore_array[i][1], {
        description = S("Shovel head "..metals_ore_array[i][1]),
        _doc_items_longdesc = S("Shovel head use for crafte pick in any place"),
        inventory_image =  "(owl_tech_shovel_head.png^[colorize:"..metals_ore_array[i][5]..":128)",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Crafte sword from sword head
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:shovel_"..metals_ore_array[i][1],
        recipe = {"owl_tech:shovel_head_"..metals_ore_array[i][1],"mcl_core:stick"}
    })
    --Crafte sword head
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:shovel_head_"..metals_ore_array[i][1],
        recipe = {
            {"owl_tech:work_file","owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:work_hammer"},
            {"","owl_tech:"..metals_ore_array[i][1].."_ingot",""},
            {"","",""}
        },
        replacements = {
            {"owl_tech:work_hammer", "owl_tech:work_hammer"},
            {"owl_tech:work_file", "owl_tech:work_file"},
        }
    })
    --Hoe
    minetest.register_tool("owl_tech:hoe_"..metals_ore_array[i][1], {
        description = S(metals_ore_array[i][1].." Hoe"),
        --_tt_help = hoe_tt.."\n"..S("Uses: @1", uses.iron),
        _doc_items_longdesc = "Owl tech hoe",
        inventory_image = "(owl_tech_hoe_head.png^[colorize:"..metals_ore_array[i][5]..":128)^owl_tech_hoe_stick.png",
        wield_scale = mcl_vars.tool_wield_scale,
        on_place = hoe_on_place_function(metals_ore_array[i][6]),
        groups = { tool=1, hoe=1, enchantability=14 },
        tool_capabilities = {
            -- 1/3
            full_punch_interval = 0.33333333,
            damage_groups = { fleshy = 1, },
            punch_attack_uses = metals_ore_array[i][6],
        },
        _repair_material = "owl_tech:"..metals_ore_array[i][1].."_ingot",
        _mcl_toollike_wield = true,
        _mcl_diggroups = {
            hoey = { speed = 6, level = 4, uses = 251 }
        },
    })
    --Hoe head
    minetest.register_craftitem("owl_tech:hoe_head_"..metals_ore_array[i][1], {
        description = S("Hoe head "..metals_ore_array[i][1]),
        _doc_items_longdesc = S("Hoe head use for crafte hoe in any place"),
        inventory_image =  "(owl_tech_hoe_head.png^[colorize:"..metals_ore_array[i][5]..":128)",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --Crafte hoe from hoe head
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:hoe_"..metals_ore_array[i][1],
        recipe = {"owl_tech:hoe_head_"..metals_ore_array[i][1],"mcl_core:stick"}
    })
    --Crafte hoe head
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:hoe_head_"..metals_ore_array[i][1],
        recipe = {
            {"owl_tech:work_file","owl_tech:"..metals_ore_array[i][1].."_plate",""},
            {"owl_tech:work_hammer","owl_tech:"..metals_ore_array[i][1].."_ingot",""},
            {"","",""}
        },
        replacements = {
            {"owl_tech:work_hammer", "owl_tech:work_hammer"},
            {"owl_tech:work_file", "owl_tech:work_file"},
        }
    })
    --stick 
    minetest.register_craftitem("owl_tech:"..metals_ore_array[i][1].."_stick", {
        description = S(metals_ore_array[i][2].. " stick"),
        _doc_items_longdesc = S(metals_ore_array[i][2].. " stick"),
        inventory_image = "owl_tech_stick.png^[colorize:"..metals_ore_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },  
    })
    --carfte stick
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:"..metals_ore_array[i][1].."_stick",
        recipe = {"owl_tech:work_file","owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot"},
        replacements = {
            {"owl_tech:work_file","owl_tech:work_file"},
        }
    })
    --Block
    minetest.register_node("owl_tech:"..metals_ore_array[i][1].."block", {
        description = S("Block of "..metals_ore_array[i][2]),
        _doc_items_longdesc = S("Block of "..metals_ore_array[i][2]),
        tiles = {"default_steel_block.png^[colorize:"..metals_ore_array[i][5]..":128"},
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=metals_ore_array[i][4], building_block=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
    })
    --Block dust
    minetest.register_node("owl_tech:"..metals_ore_array[i][1].."_dust_block", {
        description = S("Dust block of "..metals_ore_array[i][2]),
        _doc_items_longdesc = S("Block of "..metals_ore_array[i][2]),
        tiles = {"owl_tech_dust_block_1.png^[colorize:"..metals_ore_array[i][5]..":128"},
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=metals_ore_array[i][4], building_block=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
    })
    --metal briks
    minetest.register_node("owl_tech:"..value[1].."_briks", {
        description = S(value[2].." briks"),
        _doc_items_longdesc = S("Part of multiblocks -safe for decor"),
        tiles = {"owl_tech_base_briks.png^[colorize:"..value[5]..":128"},
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2 },
        sounds = mcl_sounds.node_sound_metal_defaults(),
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5
    })
    --crafte metal briks
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..value[1].."_briks 2",
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_plate"},
            {"owl_tech:"..metals_ore_array[i][1].."_plate","mcl_core:brick_block","owl_tech:"..metals_ore_array[i][1].."_plate"},
            {"owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_plate"}
        }
    })
    --frames
    minetest.register_node("owl_tech:"..value[1].."_frames", {
        description = S(value[2].." frames"),
        _doc_items_longdesc = S("Part of multiblocks -safe for decor"),
        tiles = {"owl_tech_base_frame.png^[colorize:"..value[5]..":128"},
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2 },
        sounds = mcl_sounds.node_sound_metal_defaults(),
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5
    })
    --crafte metal briks
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..value[1].."_frames",
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:"..metals_ore_array[i][1].."_stick"},
            {"owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:wrench","owl_tech:"..metals_ore_array[i][1].."_stick"},
            {"owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:"..metals_ore_array[i][1].."_stick"}
        },
        replacements = {
            {"owl_tech:wrench","owl_tech:wrench"},
        }
    })
    --grids
    minetest.register_node("owl_tech:"..value[1].."_grid", {
        description = S(value[2].." grid"),
        _doc_items_longdesc = S("Part of multiblocks -safe for decor"),   
        tiles = {"owl_tech_base_grid.png^[colorize:"..value[5]..":128"},
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2 },
        sounds = mcl_sounds.node_sound_metal_defaults(),
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5
    })
    --crafte metal grids
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..value[1].."_grid",
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:work_hammer","owl_tech:"..metals_ore_array[i][1].."_stick"},
            {"owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:work_file","owl_tech:"..metals_ore_array[i][1].."_stick"},
            {"owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:wrench","owl_tech:"..metals_ore_array[i][1].."_stick"}
        },
        replacements = {
            {"owl_tech:wrench","owl_tech:wrench"},
            {"owl_tech:work_file","owl_tech:work_file"},
            {"owl_tech:work_hammer","owl_tech:work_hammer"},
        }
    })
    --tiles
    minetest.register_node("owl_tech:"..value[1].."_tiles", {
        description = S(value[2].." tiles"),
        _doc_items_longdesc = S("Part of multiblocks -safe for decor"),
        tiles = {"owl_tech_base_tiles.png^[colorize:"..value[5]..":128"},
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2 },
        sounds = mcl_sounds.node_sound_metal_defaults(),
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5
    })
    --crafte tile
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..value[1].."_tiles",
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:"..metals_ore_array[i][1].."_plate"},
            {"owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:wrench","owl_tech:"..metals_ore_array[i][1].."_stick"},
            {"owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:"..metals_ore_array[i][1].."_plate"}
        },
        replacements = {
            {"owl_tech:wrench","owl_tech:wrench"},
        }
    })
    --big tiles
    minetest.register_node("owl_tech:"..value[1].."_big_tiles", {
        description = S(value[2].." big tiles"),
        _doc_items_longdesc = S("Part of multiblocks -safe for decor"),   --_big_tiles
        tiles = {"owl_tech_base_big_tiles.png^[colorize:"..value[5]..":128"},
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=2 },
        sounds = mcl_sounds.node_sound_metal_defaults(),
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5
    })
    --crafte tile
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..value[1].."_big_tiles",
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:"..metals_ore_array[i][1].."_plate"},
            {"owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:work_hammer","owl_tech:"..metals_ore_array[i][1].."_stick"},
            {"owl_tech:"..metals_ore_array[i][1].."_plate","owl_tech:"..metals_ore_array[i][1].."_stick","owl_tech:"..metals_ore_array[i][1].."_plate"}
        },
        replacements = {
            {"owl_tech:work_hammer","owl_tech:work_hammer"},
        }
    })
    --Crafte ingot from nugets
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..metals_ore_array[i][1].."_ingot",
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_nugget","owl_tech:"..metals_ore_array[i][1].."_nugget","owl_tech:"..metals_ore_array[i][1].."_nugget"},
            {"owl_tech:"..metals_ore_array[i][1].."_nugget","owl_tech:"..metals_ore_array[i][1].."_nugget","owl_tech:"..metals_ore_array[i][1].."_nugget"},
            {"owl_tech:"..metals_ore_array[i][1].."_nugget","owl_tech:"..metals_ore_array[i][1].."_nugget","owl_tech:"..metals_ore_array[i][1].."_nugget"}
        }
    })
    --Crafte block from ingots
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..metals_ore_array[i][1].."block",
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot"},
            {"owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot"},
            {"owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot","owl_tech:"..metals_ore_array[i][1].."_ingot"}
        }
    })
    --Crafte block dust from ingots
    minetest.register_craft({
        type = "shaped",
        output = "owl_tech:"..metals_ore_array[i][1].."_dust_block",
        recipe = {
            {"owl_tech:"..metals_ore_array[i][1].."_dust","owl_tech:"..metals_ore_array[i][1].."_dust","owl_tech:"..metals_ore_array[i][1].."_dust"},
            {"owl_tech:"..metals_ore_array[i][1].."_dust","owl_tech:"..metals_ore_array[i][1].."_dust","owl_tech:"..metals_ore_array[i][1].."_dust"},
            {"owl_tech:"..metals_ore_array[i][1].."_dust","owl_tech:"..metals_ore_array[i][1].."_dust","owl_tech:"..metals_ore_array[i][1].."_dust"}
        }
    })
    --Crafte ingots  from block
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:"..metals_ore_array[i][1].."_ingot 9",
        recipe = {"owl_tech:"..metals_ore_array[i][1].."block"}
    })
    --Crafte dust  from block
    minetest.register_craft({
        type = "shapeless",
        output = "owl_tech:"..metals_ore_array[i][1].."_dust 9",
        recipe = {"owl_tech:"..metals_ore_array[i][1].."_dust_block"}
    })
    
end
-- 1)tech_name 2)useal name 3)need ore ? 4)pickasxe_level  5)color 6)gem_base 7)gem_block_base 8)dust_block 9)burn fuel 10)tool hardenet

local gems_orew_array={
    {"coal","Coal ",true,1,"#1b1b1b","owl_tech_gem_1.png","owl_tech_gem_block_1.png","owl_tech_dust_block_1.png",80,0},
    {"solid_biofuel","Solid biofuel ",false,1,"#0e3000","owl_tech_gem_1.png","owl_tech_gem_block_1.png","owl_tech_dust_block_1.png",160,0},
    {"sulfur","Sulfur ",true,1,"#c2a800","owl_tech_gem_1.png","owl_tech_gem_block_1.png","owl_tech_dust_block_1.png",0,0},
    {"saltpeter","Saltpeter ",true,1,"#b3e6ee","owl_tech_gem_1.png","owl_tech_gem_block_1.png","owl_tech_dust_block_1.png",0,0},
    {"diamond","Diamond ",true,3,"#77cefb","owl_tech_gem_2.png","owl_tech_gem_block_2.png","owl_tech_dust_block_1.png",0,1000},
    {"ruby","Ruby ",true,3,"#d40000","owl_tech_gem_2.png","owl_tech_gem_block_2.png","owl_tech_dust_block_1.png",0,750},--
    {"raw_ruber","Raw Ruber ",false,1,"#dace00","owl_tech_gem_3.png","owl_tech_gem_block_3.png","owl_tech_dust_block_1.png",0,0},
    {"ruber","Ruber ",false,1,"#171717","default_steel_ingot.png","default_steel_block.png","owl_tech_dust_block_1.png",0,0},
}
for i, value in ipairs(gems_orew_array) do
    --ore
    if gems_orew_array[i][3]  then --and  
        minetest.register_node("owl_tech:"..gems_orew_array[i][1].."_ore", {
            description = S(gems_orew_array[i][2].." ore"),
            _doc_items_longdesc = S(gems_orew_array[i][2]..' ore'),
            _doc_items_hidden = false,
            tiles = {"default_stone.png^(owl_tech_ore_base.png^[colorize:"..gems_orew_array[i][5]..":128)"},
            is_ground_content = true,
            stack_max = 64,
            groups = {pickaxey=gems_orew_array[i][4], building_block=1, material_stone=1, blast_furnace_smeltable=1},
            sounds = mcl_sounds.node_sound_stone_defaults(),
            _mcl_blast_resistance = 3,
            _mcl_hardness = 3,
            _mcl_silk_touch_drop = true,
        })  
    end
    --dust
    minetest.register_craftitem("owl_tech:"..gems_orew_array[i][1].."_dust", {
        description = S(gems_orew_array[i][2].. " dust"),
        _doc_items_longdesc = S(gems_orew_array[i][2].. " dust"),
        inventory_image = "owl_tech_dust.png^[colorize:"..gems_orew_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --dirt dust
    minetest.register_craftitem("owl_tech:"..gems_orew_array[i][1].."_dirt_dust", {
        description = S(gems_orew_array[i][2].. " dirt dust"),
        _doc_items_longdesc = S(gems_orew_array[i][2].. " dirt dust"),
        inventory_image = "owl_tech_dirt_dust.png^[colorize:"..gems_orew_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    if gems_orew_array[i][9]==0 then
        --gem
        minetest.register_craftitem("owl_tech:"..gems_orew_array[i][1], {
            description = S(gems_orew_array[i][2]),
            _doc_items_longdesc = S(gems_orew_array[i][2]),
            inventory_image = gems_orew_array[i][6].."^[colorize:"..gems_orew_array[i][5]..":128",
            stack_max = 64,
            groups = { craftitem=1 },
        })
    else
        --gem
        minetest.register_craftitem("owl_tech:"..gems_orew_array[i][1], {
            description = S(gems_orew_array[i][2]),
            _doc_items_longdesc = S(gems_orew_array[i][2]),
            inventory_image = gems_orew_array[i][6].."^[colorize:"..gems_orew_array[i][5]..":128",
            stack_max = 64,
            groups = { craftitem=1,coal=1 },
        })
    end

    --burn ore in gen 
    minetest.register_craft({
        type = "cooking",
        output = "owl_tech:"..gems_orew_array[i][1],
        recipe = "owl_tech:"..gems_orew_array[i][1].."_ore",
        cooktime = 10,
    })
    --plate
    minetest.register_craftitem("owl_tech:"..gems_orew_array[i][1].."_plate", {
        description = S(gems_orew_array[i][2].. " plate"),
        _doc_items_longdesc = S(gems_orew_array[i][2].. " plate"),
        inventory_image = "owl_tech_plate.png^[colorize:"..gems_orew_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --stick 
    minetest.register_craftitem("owl_tech:"..gems_orew_array[i][1].."_stick", {
        description = S(gems_orew_array[i][2].. " stick"),
        _doc_items_longdesc = S(gems_orew_array[i][2].. " stick"),
        inventory_image = "owl_tech_stick.png^[colorize:"..gems_orew_array[i][5]..":128",
        stack_max = 64,
        groups = { craftitem=1 },
    })
    --burn time gem      
    if gems_orew_array[i][9]>0 then
        minetest.register_craft({
            type = "fuel",
            recipe = "owl_tech:"..gems_orew_array[i][1],
            burntime = gems_orew_array[i][9],
        })
        minetest.register_craft({
            type = "fuel",
            recipe = "owl_tech:"..gems_orew_array[i][1].."_dust",
            burntime = gems_orew_array[i][9],
        })
    end
    --Block
    minetest.register_node("owl_tech:"..gems_orew_array[i][1].."_block", {
        description = S("Block of "..gems_orew_array[i][2]),
        _doc_items_longdesc = S("Block of "..gems_orew_array[i][2]),
        tiles = {gems_orew_array[i][7].."^[colorize:"..gems_orew_array[i][5]..":128"},
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=gems_orew_array[i][4], building_block=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
    })
    --burn time block 
    if gems_orew_array[i][9]>0 then
        minetest.register_craft({
            type = "fuel",
            recipe = "owl_tech:"..gems_orew_array[i][1].."_block",
            burntime = gems_orew_array[i][9]*9,
        })
    end
    --Block dust
    minetest.register_node("owl_tech:"..gems_orew_array[i][1].."_dust_block", {
        description = S("Block of "..gems_orew_array[i][2]),
        _doc_items_longdesc = S("Block of "..gems_orew_array[i][2]),
        tiles = {gems_orew_array[i][8].."^[colorize:"..gems_orew_array[i][5]..":128"},
        is_ground_content = false,
        stack_max = 64,
        groups = {pickaxey=gems_orew_array[i][4], building_block=1},
        sounds = mcl_sounds.node_sound_metal_defaults(),
        _mcl_blast_resistance = 6,
        _mcl_hardness = 5,
    })
    if gems_orew_array[i][10]>0 then
        --pick
        minetest.register_tool("owl_tech:pick_".. gems_orew_array[i][1], {
            description = S( gems_orew_array[i][2].." Pickaxe"),
            _doc_items_longdesc = "Pick from owl tech",
            inventory_image = "(owl_tech_pick_head.png^[colorize:"..gems_orew_array[i][5]..":128)^owl_tech_pick_stick.png",         --owl_tech_pick_head.png  owl_tech_pick_stick.png
            wield_scale =  mcl_vars.tool_wield_scale,
            groups = { tool=1, pickaxe=1, dig_speed_class=4, enchantability=14 },
            tool_capabilities = {
                -- 1/1.2
                full_punch_interval = 0.83333333,
                max_drop_level=4,
                damage_groups = {fleshy=4},
                punch_attack_uses = (gems_orew_array[i][10]),
            },
            sound = { breaks = "default_tool_breaks" },
            _repair_material = "owl_tech:"..gems_orew_array[i][1],
            _mcl_toollike_wield = true,
            _mcl_diggroups = {
                pickaxey = { speed = 6, level = 4, uses = gems_orew_array[i][10]*2}
            },
        })
        --Pick head 
        minetest.register_craftitem("owl_tech:pick_head_"..gems_orew_array[i][1], {
            description = S("Pick head "..gems_orew_array[i][1]),
            _doc_items_longdesc = S("Pick head use for crafte pick in any place"),
            inventory_image =  "(owl_tech_pick_head.png^[colorize:"..gems_orew_array[i][5]..":128)",
            stack_max = 64,
            groups = { craftitem=1 },
        })
        --Crafte pick head
        minetest.register_craft({
            type = "shaped",
            output = "owl_tech:pick_head_"..gems_orew_array[i][1],
            recipe = {
                {"owl_tech:"..gems_orew_array[i][1].."_plate","owl_tech:"..gems_orew_array[i][1],"owl_tech:"..gems_orew_array[i][1]},
                {"owl_tech:work_file","","owl_tech:work_hammer",},
                {"","",""}
            },
            replacements = {
                {"owl_tech:work_hammer", "owl_tech:work_hammer"},
                {"owl_tech:work_file", "owl_tech:work_file"},
            }
        })
        --Crafte pick from pick head
        minetest.register_craft({
            type = "shapeless",
            output = "owl_tech:pick_"..gems_orew_array[i][1],
            recipe = {"owl_tech:pick_head_"..gems_orew_array[i][1],"mcl_core:stick"}
        })
        --axe 
        minetest.register_tool("owl_tech:axe_"..gems_orew_array[i][1], {
            description = S(gems_orew_array[i][2].." axe"),
            _doc_items_longdesc = "Axe owl tech",
            inventory_image = "(owl_tech_axe_head.png^[colorize:"..gems_orew_array[i][5]..":128)^owl_tech_pick_stick.png", 
            wield_scale = mcl_vars.tool_wield_scale,
            groups = { tool=1, axe=1, dig_speed_class=4, enchantability=14 },
            tool_capabilities = {
                -- 1/0.9
                full_punch_interval = 1.11111111,
                max_drop_level=4,
                damage_groups = {fleshy=9},
                punch_attack_uses = (gems_orew_array[i][10]),
            },
            on_place = make_stripped_trunk,
            sound = { breaks = "default_tool_breaks" },
            _repair_material = "owl_tech:"..gems_orew_array[i][1],
            _mcl_toollike_wield = true,
            _mcl_diggroups = {
                axey = { speed = 6, level = 4, uses = gems_orew_array[i][10]*2 }
            },
        })
        --axe head item
        minetest.register_craftitem("owl_tech:axe_head_"..gems_orew_array[i][1], {
            description = S("Axe head "..gems_orew_array[i][1]),
            _doc_items_longdesc = S("Axe head use for crafte axe in any place"),
            inventory_image =  "(owl_tech_axe_head.png^[colorize:"..gems_orew_array[i][5]..":128)",
            stack_max = 64,
            groups = { craftitem=1 },
        })
        --Crafte axe from axe head
        minetest.register_craft({
            type = "shapeless",
            output = "owl_tech:axe_"..gems_orew_array[i][1],
            recipe = {"owl_tech:axe_head_"..gems_orew_array[i][1],"mcl_core:stick"}
        }) 
        --Crafte  head
        minetest.register_craft({
            type = "shaped",
            output = "owl_tech:axe_head_"..gems_orew_array[i][1],
            recipe = {
                {"owl_tech:"..gems_orew_array[i][1].."_plate","owl_tech:"..gems_orew_array[i][1],""},
                {"owl_tech:"..gems_orew_array[i][1],"owl_tech:work_hammer",""},
                {"owl_tech:work_file","",""}
            },
            replacements = {
                {"owl_tech:work_hammer", "owl_tech:work_hammer"},
                {"owl_tech:work_file", "owl_tech:work_file"},
            }
        })
        --Sword 
        minetest.register_tool("owl_tech:sword_"..gems_orew_array[i][1], {
            description = S(gems_orew_array[i][2].." sword"),
            _doc_items_longdesc = "Owl tech sword",
            inventory_image = "(owl_tech_sword_blade.png^[colorize:"..gems_orew_array[i][5]..":128)^owl_tech_sword_stick.png", 
            wield_scale = mcl_vars.tool_wield_scale,
            groups = { weapon=1, sword=1, dig_speed_class=4, enchantability=14 },
            tool_capabilities = {
                full_punch_interval = 0.625,
                max_drop_level=4,
                damage_groups = {fleshy=6},
                punch_attack_uses = gems_orew_array[i][10],
            },
            sound = { breaks = "default_tool_breaks" },
            _repair_material = "owl_tech:"..gems_orew_array[i][1],
            _mcl_toollike_wield = true,
            _mcl_diggroups = {
                swordy = { speed = 6, level = 4, uses = gems_orew_array[i][10]*2 },
                swordy_cobweb = { speed = 6, level = 4, uses = gems_orew_array[i][10]*2 }
            },
        })
        --sword head
        minetest.register_craftitem("owl_tech:sword_head_"..gems_orew_array[i][1], {
            description = S("Sword head "..gems_orew_array[i][1]),
            _doc_items_longdesc = S("Sword head use for crafte sword in any place"),
            inventory_image =  "(owl_tech_sword_blade.png^[colorize:"..gems_orew_array[i][5]..":128)",
            stack_max = 64,
            groups = { craftitem=1 },
        })
        --Crafte sword from sword head
        minetest.register_craft({
            type = "shapeless",
            output = "owl_tech:sword_"..gems_orew_array[i][1],
            recipe = {"owl_tech:sword_head_"..gems_orew_array[i][1],"mcl_core:stick"}
        })
        --Crafte sword head
        minetest.register_craft({
            type = "shaped",
            output = "owl_tech:sword_head_"..gems_orew_array[i][1],
            recipe = {
                {"","owl_tech:"..gems_orew_array[i][1].."_plate",""},
                {"owl_tech:work_file","owl_tech:"..gems_orew_array[i][1].."_plate","owl_tech:work_hammer"},
                {"","",""}
            },
            replacements = {
                {"owl_tech:work_hammer", "owl_tech:work_hammer"},
                {"owl_tech:work_file", "owl_tech:work_file"},
            }
        })
        --shovel 
        minetest.register_tool("owl_tech:shovel_"..gems_orew_array[i][1], {
            description = S(gems_orew_array[i][2].." Shovel"),
            _doc_items_longdesc = "Owl tech shovel",
            _doc_items_usagehelp = shovel_use,
            inventory_image = "(owl_tech_shovel_head.png^[colorize:"..gems_orew_array[i][5]..":128)^owl_tech_shovel_stick.png",
            wield_scale = wield_scale,
            groups = { tool=1, shovel=1, dig_speed_class=4, enchantability=14 },
            tool_capabilities = {
                full_punch_interval = 1,
                max_drop_level=4,
                damage_groups = {fleshy=4},
                punch_attack_uses = gems_orew_array[i][10],
            },
            on_place = make_grass_path,
            sound = { breaks = "default_tool_breaks" },
            _repair_material = "owl_tech:"..gems_orew_array[i][1],
            _mcl_toollike_wield = true,
            _mcl_diggroups = {
                shovely = { speed = 6, level = 4, uses = gems_orew_array[i][10]*2  }
            },
        })
        --Shovel head
        minetest.register_craftitem("owl_tech:shovel_head_"..gems_orew_array[i][1], {
            description = S("Shovel head "..gems_orew_array[i][1]),
            _doc_items_longdesc = S("Shovel head use for crafte pick in any place"),
            inventory_image =  "(owl_tech_shovel_head.png^[colorize:"..gems_orew_array[i][5]..":128)",
            stack_max = 64,
            groups = { craftitem=1 },
        })
        --Crafte sword from sword head
        minetest.register_craft({
            type = "shapeless",
            output = "owl_tech:shovel_"..gems_orew_array[i][1],
            recipe = {"owl_tech:shovel_head_"..gems_orew_array[i][1],"mcl_core:stick"}
        })
        --Crafte sword head
        minetest.register_craft({
            type = "shaped",
            output = "owl_tech:shovel_head_"..gems_orew_array[i][1],
            recipe = {
                {"owl_tech:work_file","owl_tech:"..gems_orew_array[i][1].."_plate","owl_tech:work_hammer"},
                {"","owl_tech:"..gems_orew_array[i][1],""},
                {"","",""}
            },
            replacements = {
                {"owl_tech:work_hammer", "owl_tech:work_hammer"},
                {"owl_tech:work_file", "owl_tech:work_file"},
            }
        })
        --Hoe
        minetest.register_tool("owl_tech:hoe_"..gems_orew_array[i][1], {
            description = S(gems_orew_array[i][1].." Hoe"),
            --_tt_help = hoe_tt.."\n"..S("Uses: @1", uses.iron),
            _doc_items_longdesc = "Owl tech hoe",
            inventory_image = "(owl_tech_hoe_head.png^[colorize:"..gems_orew_array[i][5]..":128)^owl_tech_hoe_stick.png",
            wield_scale = mcl_vars.tool_wield_scale,
            on_place = hoe_on_place_function(gems_orew_array[i][10]),
            groups = { tool=1, hoe=1, enchantability=14 },
            tool_capabilities = {
                -- 1/3
                full_punch_interval = 0.33333333,
                damage_groups = { fleshy = 1, },
                punch_attack_uses = gems_orew_array[i][10],
            },
            _repair_material = "owl_tech:"..gems_orew_array[i][1],
            _mcl_toollike_wield = true,
            _mcl_diggroups = {
                hoey = { speed = 6, level = 4, uses = 251 }
            },
        })
        --Hoe head
        minetest.register_craftitem("owl_tech:hoe_head_"..gems_orew_array[i][1], {
            description = S("Hoe head "..gems_orew_array[i][1]),
            _doc_items_longdesc = S("Hoe head use for crafte hoe in any place"),
            inventory_image =  "(owl_tech_hoe_head.png^[colorize:"..gems_orew_array[i][5]..":128)",
            stack_max = 64,
            groups = { craftitem=1 },
        })
        --Crafte hoe from hoe head
        minetest.register_craft({
            type = "shapeless",
            output = "owl_tech:hoe_"..gems_orew_array[i][1],
            recipe = {"owl_tech:hoe_head_"..gems_orew_array[i][1],"mcl_core:stick"}
        })
        --Crafte hoe head
        minetest.register_craft({
            type = "shaped",
            output = "owl_tech:hoe_head_"..gems_orew_array[i][1],
            recipe = {
                {"owl_tech:work_file","owl_tech:"..gems_orew_array[i][1].."_plate",""},
                {"owl_tech:work_hammer","owl_tech:"..gems_orew_array[i][1],""},
                {"","",""}
            },
            replacements = {
                {"owl_tech:work_hammer", "owl_tech:work_hammer"},
                {"owl_tech:work_file", "owl_tech:work_file"},
            }
        })
    end
   


end
--1)name 2)Name 3)flowing animation 4)source animation 5)cell tezture 6)is fluid?
local fluid_array ={
    {"tar","Tar","owl_tech_tar_flowing_animated.png","owl_tech_tar_source_animated.png","owl_tech_tar_for_cell.png",1},--#8d4a00
    {"metan","Metan","owl_tech_tar_flowing_animated.png","owl_tech_tar_source_animated.png","owl_tech_metan_for_cell.png",0},--ccc8b0
    {"distilled_water","Distilled water","owl_tech_distilled_water_flowing_animated.png","owl_tech_distilled_water_source_animated.png","owl_tech_distilled_water_for_cell.png",1},
}
for i, value in ipairs(fluid_array) do
    if fluid_array[i][6]==1 then
        minetest.register_node("owl_tech:"..fluid_array[i][1].."_flowing", {
            description = S(fluid_array[i][2].." Water"),
            _doc_items_create_entry = false,
            wield_image = fluid_array[i][3].."^[verticalframe:64:0",
            drawtype = "flowingliquid",
            tiles = {fluid_array[i][3].."^[verticalframe:64:0"},
            special_tiles = {
                {
                    image="("..fluid_array[i][3]..")",
                    backface_culling=false,
                    animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=4.0}
                },
                {
                    image="("..fluid_array[i][3]..")",
                    backface_culling=false,
                    animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=4.0}
                },
            },
            sounds = mcl_sounds.node_sound_water_defaults(),
            is_ground_content = false,
            use_texture_alpha = USE_TEXTURE_ALPHA,
            paramtype = "light",
            paramtype2 = "flowingliquid",
            walkable = false,
            pointable = false,
            diggable = false,
            buildable_to = true,
            drop = "",
            drowning = 4,
            liquidtype = "flowing",
            liquid_alternative_flowing = "owl_tech:"..fluid_array[i][1].."_flowing",
            liquid_alternative_source = "owl_tech:"..fluid_array[i][1].."_source",
            liquid_viscosity = WATER_VISC,
            liquid_range = 7,
            waving = 3,
            post_effect_color = {a=20, r=0x03, g=0x3C, b=0x5C},
            groups = { water=3, liquid=3, puts_out_fire=1, melt_around=1, dig_by_piston=1, not_in_creative_inventory=1,},--not_in_creative_inventory=1,
            _mcl_blast_resistance = 100,
            -- Hardness intentionally set to infinite instead of 100 (Minecraft value) to avoid problems in creative mode
            _mcl_hardness = -1,
        })
        
        minetest.register_node("owl_tech:"..fluid_array[i][1].."_source", {
            description = S("Water Source"),
            _doc_items_entry_name = S("Water"),
            _doc_items_longdesc =
        S("Сompounds from cyclic, highly volatile and non-volatile compounds, which are presented in a spectrum: from methyl esters to combustion of alcohols, acids, phenols and light compounds.").."\n\n",
            _doc_items_hidden = false,
            drawtype = "liquid",
            waving = 3,
            tiles = {
                {name=fluid_array[i][4], animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=5.0}}
            },
            special_tiles = {
                -- New-style water source material (mostly unused)
                {
                    name=fluid_array[i][4],
                    animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=5.0},
                    backface_culling = false,
                }
            },
            sounds = mcl_sounds.node_sound_water_defaults(),
            is_ground_content = false,
            use_texture_alpha = USE_TEXTURE_ALPHA,
            paramtype = "light",
            walkable = false,
            pointable = false,
            diggable = false,
            buildable_to = true,
            drop = "",
            drowning = 4,
            liquidtype = "source",
            liquid_alternative_flowing = "owl_tech:"..fluid_array[i][1].."_flowing",
            liquid_alternative_source = "owl_tech:"..fluid_array[i][1].."_source",
            liquid_viscosity = WATER_VISC,
            liquid_range = 7,
            post_effect_color = {a=90, r=0x03, g=0x3C, b=0x5C},
            stack_max = 64,
            groups = { water=3, liquid=3, puts_out_fire=1,  not_in_creative_inventory=1, dig_by_piston=1},
            _mcl_blast_resistance = 100,
            -- Hardness intentionally set to infinite instead of 100 (Minecraft value) to avoid problems in creative mode
            _mcl_hardness = -1,
        })     
    end
    --capsuls 
    minetest.register_craftitem("owl_tech:"..fluid_array[i][1].."_cell", {
        description = S("Get 1000 of "..fluid_array[i][1]),
        _doc_items_longdesc = S("Contein "..fluid_array[i][1]),
        inventory_image = "owl_tech_empty_cell.png^"..fluid_array[i][5],
        stack_max = 64,
        groups = { craftitem=1,load_cell=1,can_place_fluid=fluid_array[i][6]  },
        on_place = function(itemstack, placer, pointed_thing)
            if minetest.get_item_group(itemstack:get_name(),"can_place_fluid")>0 and pointed_thing.type == "node"   then
                minetest.place_node(pointed_thing.above,{name="owl_tech:"..fluid_array[i][1].."_source"})
                itemstack:set_count(itemstack:get_count()-1)
                return itemstack
            end
        end
    })
end
--empty cell
minetest.register_craftitem("owl_tech:empty_cell", {
    description = S("cell tire 1"),
    _doc_items_longdesc = S("Simple cell for fluids"),
    inventory_image = "owl_tech_empty_cell.png",
    stack_max = 64,
    groups = { craftitem=1 },
})
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:empty_cell 4",
    recipe = {
        {"","owl_tech:iron_plate",""},
        {"owl_tech:tin_plate","","owl_tech:tin_plate"},
        {"","owl_tech:iron_plate",""},
    },
})
-- water and lava cell 
minetest.register_craftitem("owl_tech:water_cell", {
    description = S("Get 1000 of water"),
    _doc_items_longdesc = S("Contein water"),
    inventory_image = "owl_tech_empty_cell.png^owl_tech_water_for_cell.png",
    stack_max = 64,
    groups = { craftitem=1,load_cell=1  },
    on_place = function(itemstack, placer, pointed_thing)
        if pointed_thing.type == "node" then
            minetest.place_node(pointed_thing.above,{name="mcl_core:water_source"})
            itemstack:set_count(itemstack:get_count()-1)
            return itemstack
        end
    end
})
minetest.register_craftitem("owl_tech:lava_cell", {
    description = S("Get 1000 of lava"),
    _doc_items_longdesc = S("Contein lava"),
    inventory_image = "owl_tech_empty_cell.png^owl_tech_lava_for_cell.png",
    stack_max = 64,
    groups = { craftitem=1,load_cell=1  },
    on_place = function(itemstack, placer, pointed_thing)
        if pointed_thing.type == "node" then
            minetest.place_node(pointed_thing.above,{name="mcl_core:lava_source"})
            itemstack:set_count(itemstack:get_count()-1)
            return itemstack
        end
    end
})
-----------------------------------------------------------------------------------
--Some custom recips
--Crafte bronze dust from copper and tin
minetest.register_craft({
    type = "shapeless",
    output = "owl_tech:bronze_dust 3",
    recipe = {"owl_tech:copper_dust","owl_tech:copper_dust","owl_tech:copper_dust","owl_tech:tin_dust"}
})
--Crafte electrum from gold and silver
minetest.register_craft({
    type = "shapeless",
    output = "owl_tech:electrum_dust 3",
    recipe = {"owl_tech:gold_dust","owl_tech:gold_dust","owl_tech:silver_dust","owl_tech:silver_dust"}
})
--MC2 ingots in Owl_tech 
--Crutch - remake how the idea will be better!!!!
minetest.register_craft({
	type = "cooking",
    output = "owl_tech:gold_ingot",
    recipe = "mcl_core:gold_ingot",
	cooktime = 10,
})
minetest.register_craft({
    type = "cooking",
    output = "owl_tech:iron_ingot",
    recipe =  "mcl_core:iron_ingot", 
	cooktime = 10,
})
--Burn raw ruber in ruber
minetest.register_craft({
    type = "cooking",
    output = "owl_tech:ruber",
    recipe =  "mcl_core:raw_ruber", 
	cooktime = 10,
})
----------------------------------------------------------------------------------------
--Flint tools
--pick  "mcl_core:flint"  
minetest.register_tool("owl_tech:pick_flint", {
    description = ("Flint Pickaxe"),
    _doc_items_longdesc = "Pick from owl tech",
    inventory_image = "(owl_tech_pick_head.png^[colorize:#00264d:128)^owl_tech_pick_stick.png", 
	wield_scale = wield_scale,
	groups = { tool=1, pickaxe=1, dig_speed_class=3, enchantability=5 },
	tool_capabilities = {
		-- 1/1.2
		full_punch_interval = 0.83333333,
		max_drop_level=3,
		damage_groups = {fleshy=3},
		punch_attack_uses = 75,
	},
	sound = { breaks = "default_tool_breaks" },
	_repair_material = "mcl_core:flint",
	_mcl_toollike_wield = true,
	_mcl_diggroups = {
		pickaxey = { speed = 4, level = 3, uses = 150 }
	},
})
minetest.register_craft({
	output = "owl_tech:pick_flint",
	recipe = {
		{"mcl_core:flint","mcl_core:flint","mcl_core:flint"},
		{"","mcl_core:stick",""},
		{"","mcl_core:stick",""}
	}
})
--axe 
minetest.register_tool("owl_tech:axe_flint", {
    description = S("Flint axe"),
    _doc_items_longdesc = "Axe owl tech",
    inventory_image = "(owl_tech_axe_head.png^[colorize:#00264d:128)^owl_tech_pick_stick.png", 
	wield_scale = wield_scale,
	groups = { tool=1, axe=1, dig_speed_class=3, enchantability=5 },
	tool_capabilities = {
		full_punch_interval = 1.25,
		max_drop_level=3,
		damage_groups = {fleshy=9},
		punch_attack_uses = 75,
	},
	on_place = make_stripped_trunk,
	sound = { breaks = "default_tool_breaks" },
	_repair_material = "mcl_core:flint",
	_mcl_toollike_wield = true,
	_mcl_diggroups = {
		axey = { speed = 4, level = 3, uses = 155 }
	},
})
minetest.register_craft({
	output = "owl_tech:axe_flint",
	recipe = {
		{"mcl_core:flint","mcl_core:flint"},
		{"mcl_core:flint","mcl_core:stick"},
		{"","mcl_core:stick"}
	}
})

--Sword 
minetest.register_tool("owl_tech:sword_flint", {
    description = ("Flint sword"),
    _doc_items_longdesc = "Owl tech sword",
    inventory_image = "(owl_tech_sword_blade.png^[colorize:#00264d:128)^owl_tech_sword_stick.png", 
	wield_scale = wield_scale,
	groups = { weapon=1, sword=1, dig_speed_class=3, enchantability=5 },
	tool_capabilities = {
		full_punch_interval = 0.625,
		max_drop_level=3,
		damage_groups = {fleshy=5},
		punch_attack_uses = 150,
	},
	sound = { breaks = "default_tool_breaks" },
	_repair_material = "mcl_core:flint",
	_mcl_toollike_wield = true,
	_mcl_diggroups = {
		swordy = { speed = 4, level = 3, uses = 150 },
		swordy_cobweb = { speed = 4, level = 3, uses = 150 }
	},
})
minetest.register_craft({
	output = "owl_tech:sword_flint",
	recipe = {
		{"mcl_core:flint"},
		{"mcl_core:flint"},
		{"mcl_core:stick"}
	}
})
--shovel 
minetest.register_tool("owl_tech:shovel_flint", {
    description = S("Flint shovel"),
    _doc_items_longdesc = "Owl tech shovel",
    _doc_items_usagehelp = shovel_use,
    inventory_image = "(owl_tech_shovel_head.png^[colorize:#00264d:128)^owl_tech_shovel_stick.png",
	wield_scale = wield_scale,
	groups = { tool=1, shovel=1, dig_speed_class=3, enchantability=5 },
	tool_capabilities = {
		full_punch_interval = 1,
		max_drop_level=3,
		damage_groups = {fleshy=3},
		punch_attack_uses = 66,
	},
	on_place = make_grass_path,
	sound = { breaks = "default_tool_breaks" },
	_repair_material = "mcl_core:flint",
	_mcl_toollike_wield = true,
	_mcl_diggroups = {
		shovely = { speed = 4, level = 3, uses = 132 }
	},
})
minetest.register_craft({
	output = "owl_tech:shovel_flint",
	recipe = {
		{"mcl_core:flint"},
		{"mcl_core:stick"},
		{"mcl_core:stick"}
	}
})
--Hoe
minetest.register_tool("owl_tech:hoe_flint", {
    description = S("Flint hoe"),
    --_tt_help = hoe_tt.."\n"..S("Uses: @1", uses.iron),
    _doc_items_longdesc = "Owl tech hoe",
    inventory_image = "(owl_tech_hoe_head.png^[colorize:#00264d:128)^owl_tech_hoe_stick.png",
	wield_scale = mcl_vars.tool_wield_scale,
	on_place = hoe_on_place_function(75),
	groups = { tool=1, hoe=1, enchantability=5 },
	tool_capabilities = {
		full_punch_interval = 0.5,
		damage_groups = { fleshy = 1, },
		punch_attack_uses = 75,
	},
	_repair_material = "mcl_core:flint",
	_mcl_toollike_wield = true,
	_mcl_diggroups = {
		hoey = { speed = 4, level = 3, uses = 150 }
	},
})

minetest.register_craft({
	output = "owl_tech:hoe_flint",
	recipe = {
		{"mcl_core:flint", "mcl_core:flint"},
		{"", "mcl_core:stick"},
		{"", "mcl_core:stick"}
	}
})
minetest.register_craft({
	output = "owl_tech:hoe_flint",
	recipe = {
		{"mcl_core:flint", "mcl_core:flint"},
		{"mcl_core:stick", ""},
		{"mcl_core:stick", ""}
	}
})
--circuit
minetest.register_craftitem("owl_tech:circuit_tire_1", {
    description = S("Circuit tire 1"),
    _doc_items_longdesc = S("Simple lamp circuit"),
    inventory_image = "owl_tech_circuit_tire1.png",
    stack_max = 64,
    groups = { craftitem=1 },
})
--Cafte circuit
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:circuit_tire_1 2",
    recipe = {
        {"owl_tech:tin_lamp","owl_tech:copper_electro_wire","owl_tech:tin_lamp"},
        {"owl_tech:iron_lamp","owl_tech:copper_electro_wire","owl_tech:iron_lamp",},
        {"owl_tech:copper_lamp","owl_tech:copper_electro_wire","owl_tech:copper_lamp"}
    },
})
--battery 1 
minetest.register_craftitem("owl_tech:batteri_tire_1", {
    description = S("Battery tire 1"),
    _doc_items_longdesc = S("Use for crafte"),
    inventory_image = "owl_tech_base_battary_side.png",
    stack_max = 64,
    groups = { craftitem=1 },
})
--Cafte battery 1 
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:batteri_tire_1 2",
    recipe = {
        {"","owl_tech:copper_electro_wire",""},
        {"owl_tech:tin_plate","owl_tech:copper_electro_wire","owl_tech:tin_plate"},
        {"owl_tech:tin_plate","owl_tech:copper_lamp","owl_tech:tin_plate"}
    },
}) 
--biomassa
minetest.register_craftitem("owl_tech:biomassa", { 
    description = S("Biomassa"),
    _doc_items_longdesc = S("Get from leavs"),
    inventory_image = "owl_tech_boimassa.png",
    stack_max = 64,
    groups = { craftitem=1 },
})
--wood dust 
minetest.register_craftitem("owl_tech:wood_dust", { 
    description = S("Wood dust"),
    _doc_items_longdesc = S("Get from wood log "),
    inventory_image = "owl_tech_dust.png^[colorize:#643c0b:128",
    stack_max = 64,
    groups = { craftitem=1 },
})
--wood dust  and biomass
minetest.register_craftitem("owl_tech:wood_and_biomass", { 
    description = S("Wood dust and biomass"),
    _doc_items_longdesc = S("Get from wood log use for get biofule"),
    inventory_image = "owl_tech_dust.png^[colorize:#8d6b00:128",
    stack_max = 64,
    groups = { craftitem=1 },
})