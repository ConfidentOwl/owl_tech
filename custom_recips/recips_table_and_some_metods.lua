--setter and getter mashine tire
function owl_tech.get_mashine_tire(meta)
    return meta:get_int("_tire")
end 
function owl_tech.set_mashine_tire(meta,value)
    meta:set_int("_tire",value)
end
--------------------------------
-- maceratro recips func
function owl_tech.can_macaerat_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its) -- chek for tire , and slots for free space for res and add res
    local ret =false
    if  MACERATO_RECIPS[name][2]<=owl_tech.get_mashine_tire(meta) 
        and (dst_its:is_empty() or(dst_its:get_name()==MACERATO_RECIPS[name][3] and dst_its:get_count()+MACERATO_RECIPS[name][4]<=dst_its:get_stack_max()))
        and (dst_add_its:is_empty() or(dst_add_its:get_name()==MACERATO_RECIPS[name][7] and dst_add_its:get_count()+MACERATO_RECIPS[name][8]<=dst_add_its:get_stack_max())) then
        ret =true
    end
    return ret
end
--------------------------------
-- Fluid separator recips func
function owl_tech.can_fluid_separator_recips_go_in_this_mashien(meta,name,fluid1_amount,fluid2_name,fluid2_amount,fluid3_name,fluid3_amount,pull_max2,pull_max3) -- chek for tire , and slots for free space for res and add res
    local ret =false
    if  FLUID_SEPARATOR[name][2]<=owl_tech.get_mashine_tire(meta) 
        and (FLUID_SEPARATOR[name][1]<=fluid1_amount)
        and (fluid2_name=="none" or(fluid2_name==FLUID_SEPARATOR[name][3] and fluid2_amount+FLUID_SEPARATOR[name][4]<=pull_max2))
        and (fluid3_name=="none" or(fluid3_name==FLUID_SEPARATOR[name][7] and fluid3_amount+FLUID_SEPARATOR[name][8]<=pull_max3)) then
        ret =true
    end
    return ret
end
--------------------------------
-- sieve recips func
function owl_tech.can_siev_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its) -- chek for tire , and slots for free space for res and add res
    local ret =false
    if  SIEV_RESIPS[name][2]<=owl_tech.get_mashine_tire(meta) 
        and (dst_its:is_empty() or(dst_its:get_name()==SIEV_RESIPS[name][3] and dst_its:get_count()+SIEV_RESIPS[name][4]<=dst_its:get_stack_max()))
        and (dst_add_its:is_empty() or(dst_add_its:get_name()==SIEV_RESIPS[name][7] and dst_add_its:get_count()+SIEV_RESIPS[name][8]<=dst_add_its:get_stack_max())) then
        ret =true
    end
    return ret
end
--------------------------------
-- Forge hammer recips func
function owl_tech.can_forge_hammer_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its) -- chek for tire , and slots for free space for res and add res
    local ret =false
    if  FORGE_HAMMER_RECIPS[name][2]<=owl_tech.get_mashine_tire(meta) 
        and (dst_its:is_empty() or(dst_its:get_name()==FORGE_HAMMER_RECIPS[name][3] and dst_its:get_count()+FORGE_HAMMER_RECIPS[name][4]<=dst_its:get_stack_max()))
        and (dst_add_its:is_empty() or(dst_add_its:get_name()==FORGE_HAMMER_RECIPS[name][7] and dst_add_its:get_count()+FORGE_HAMMER_RECIPS[name][8]<=dst_add_its:get_stack_max())) then
        ret =true
    end
    return ret
end
--------------------------------
-- smelter recips func
function owl_tech.can_smelter_recips_recips_go_in_this_mashien(meta,name,dst_its) -- chek for tire , and slots for free space for res and add res
    local ret =false
    if  SMELTER_RECIPS[name][5]<=owl_tech.get_mashine_tire(meta) 
        and (dst_its:is_empty() or(dst_its:get_name()==SMELTER_RECIPS[name][3] and dst_its:get_count()+SMELTER_RECIPS[name][4]<=dst_its:get_stack_max())) then
        ret =true
    end
    return ret
end
--------------------------------
-- smelter recips func
function owl_tech.can_mixer_recips_recips_go_in_this_mashien(meta,name,dst_its) -- chek for tire , and slots for free space for res and add res
    local ret =false
    if  MIXER_RESIPS[name][5]<=owl_tech.get_mashine_tire(meta) 
        and (dst_its:is_empty() or(dst_its:get_name()==MIXER_RESIPS[name][3] and dst_its:get_count()+MIXER_RESIPS[name][4]<=dst_its:get_stack_max())) then
        ret =true
    end
    return ret
end
--------------------------------
--"mcl_fire:flint_and_steel"
minetest.clear_craft({output = "mcl_fire:flint_and_steel",})
minetest.register_craft({
	type = "shapeless",
	output = "mcl_fire:flint_and_steel",
	recipe = { "owl_tech:steel_ingot", "mcl_core:flint"},
})