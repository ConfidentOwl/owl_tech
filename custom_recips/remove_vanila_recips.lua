local list_to_remove_recips_intruments ={
    "mcl_tools:pick_iron","mcl_tools:pick_gold", "mcl_tools:pick_diamond","mcl_tools:pick_netherite","mcl_tools:shovel_iron","mcl_tools:shovel_gold","mcl_tools:shovel_diamond","mcl_tools:shovel_netherite",
    "mcl_tools:axe_iron","mcl_tools:axe_gold","mcl_tools:axe_diamond","mcl_tools:axe_netherite","mcl_tools:sword_iron","mcl_tools:sword_gold","mcl_tools:sword_diamond","mcl_tools:sword_netherite",
    "mcl_farming:hoe_iron","mcl_farming:hoe_gold","mcl_farming:hoe_diamond","mcl_farming:hoe_netherite"
}
for i, value in ipairs(list_to_remove_recips_intruments) do
    minetest.clear_craft({
        type = "shaped",
        output = value, 
        recipe = {
            {"","",""},
            {"","",""},
            {"","",""}
        }
    }) 
end

