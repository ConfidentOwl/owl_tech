--Get and set charge
function owl_tech:get_charge(meta)
    return meta:get_int("charge")
end
function owl_tech:set_charge(meta,num)
   meta:set_int("charge",num) 
end
---------------------------------------
--Get and set  max charge
function owl_tech:get_charge_max(meta)
    return meta:get_int("max_charge")
end
function owl_tech:set_charge_max(meta,num)
   meta:set_int("max_charge",num) 
end
---------------------------------------
--Get and set voltage
function owl_tech:get_voltage(meta)
    return meta:get_int("voltage")
end
function owl_tech:set_voltage(meta,num)
   meta:set_int("voltage",num) 
end
---------------------------------------
--Add base electro in node
function owl_tech:add_electro(pos,voltage,max_charge)
    local meta=minetest.get_meta(pos)
    meta:set_int("voltage",voltage)
    meta:set_int("max_charge",max_charge)
    meta:set_int("charge",0)

end
---------------------------------------
--Prepear vire 
function owl_tech:prepear_vire(pos,voltage)
    owl_tech:add_electro(pos,voltage,voltage*6)
    owl_tech.reset_connection_wire_meta(pos)
end
---------------------------------------
--Reset connection meta
function owl_tech.reset_connection_wire_meta(pos)
    local meta=minetest.get_meta(pos)

    meta:set_int("connect_amount", 0)
    meta:set_int("+Y", 0)
    meta:set_int("-Y", 0)
    meta:set_int("+Z", 0)
    meta:set_int("-Z", 0)
    meta:set_int("+X", 0)
    meta:set_int("-X", 0)

end
---------------------------------------
--seeters and geters connection amount
function owl_tech:get_connection_amount_wire(pos)
    return (minetest.get_meta(pos)):get_int("connect_amount")
end
function owl_tech:set_connection_amount_wire(pos,value) 
    (minetest.get_meta(pos)):set_int("connect_amount", value)
end
function owl_tech.add_1_conection_amount_wire(pos)
    (minetest.get_meta(pos)):set_int("connect_amount",minetest.get_meta(pos):get_int("connect_amount")+1 )
end
---------------------------------------
--Can send electro in pos
function owl_tech:can_send_electro(pos,pos_send)
    local meta = minetest.get_meta(pos)
    local meta_send = minetest.get_meta(pos_send)
    if owl_tech:get_voltage(meta)== owl_tech:get_voltage(meta_send)then-- if == voltage
        if owl_tech:get_charge(meta)>0
        and owl_tech:get_charge_max(meta_send)-owl_tech:get_charge(meta_send)>=owl_tech:get_voltage(meta_send) then
            return true
        else
            return false
        end
    else
        return false
    end
end

local conetc_to={
    "owl_tech_electro_wire"
}
--node in pos ?in list 
function owl_tech.node_in_pos_connect_to_wire(pos)
    local ret=0
    for index, value in ipairs(conetc_to) do
        if minetest.get_item_group((minetest.get_node(pos)).name,"owl_tech_electro_wire")>0 then
            ret=1
        end
    end
    return ret
end
-------------------------------------------
--update pipe around 
function owl_tech.update_wire_around(pos)
    local mass_pos ={
        {x=pos.x,y=pos.y+1,z=pos.z},
        {x=pos.x,y=pos.y-1,z=pos.z},
        {x=pos.x+1,y=pos.y,z=pos.z},
        {x=pos.x-1,y=pos.y,z=pos.z},
        {x=pos.x,y=pos.y,z=pos.z+1},
        {x=pos.x,y=pos.y,z=pos.z-1}
    }
    for i, value in ipairs(mass_pos) do
        if minetest.get_item_group((minetest.get_node(mass_pos[i])).name,"owl_tech_electro_wire")>0  then
            local meta =  minetest.get_meta(mass_pos[i])
            owl_tech.check_all_side_for_fluid_work(mass_pos[i],meta) 
        end
    end
end
------------------------------
--Chek all side for work and set it
function owl_tech.check_all_side_for_wire_work(pos,meta)
    local meta = minetest.get_meta(pos)
    owl_tech:set_connection_amount_wire(pos,0) --reset conection amount

    owl_tech.set_side_info_y_p(meta,owl_tech.node_in_pos_connect_to_wire({x=pos.x,y=pos.y+1,z=pos.z}))--y+
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_wire({x=pos.x,y=pos.y+1,z=pos.z}))

    owl_tech.set_side_info_y_m(meta,owl_tech.node_in_pos_connect_to_wire({x=pos.x,y=pos.y-1,z=pos.z}))--y-
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_wire({x=pos.x,y=pos.y-1,z=pos.z}))

    owl_tech.set_side_info_x_p(meta,owl_tech.node_in_pos_connect_to_wire({x=pos.x+1,y=pos.y,z=pos.z}))--x+
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_wire({x=pos.x+1,y=pos.y,z=pos.z}))

    owl_tech.set_side_info_x_m(meta,owl_tech.node_in_pos_connect_to_wire({x=pos.x-1,y=pos.y,z=pos.z}))--x-
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_wire({x=pos.x-1,y=pos.y,z=pos.z}))

    owl_tech.set_side_info_z_p(meta,owl_tech.node_in_pos_connect_to_wire({x=pos.x,y=pos.y,z=pos.z+1}))--z+
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_wire({x=pos.x,y=pos.y,z=pos.z+1}))

    owl_tech.set_side_info_z_m(meta,owl_tech.node_in_pos_connect_to_wire({x=pos.x,y=pos.y,z=pos.z-1}))--z-
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_wire({x=pos.x,y=pos.y,z=pos.z-1}))
end
-------------------------------------------
--chek can send fluid in pipe\mashine
function owl_tech.can_send_electro_in_node(meta,pos_send)
    local meta_send = minetest.get_meta(pos_send)
    if owl_tech:get_voltage(meta)== owl_tech:get_voltage(meta_send)then-- if == voltage
        if owl_tech:get_charge(meta)>0
        and owl_tech:get_charge_max(meta_send)-owl_tech:get_charge(meta_send)>=owl_tech:get_voltage(meta_send) then
            return true
        else
            return false
        end
    end
end
-------------------------------------------
--send fluid in pos by pipe
function owl_tech.send_electro_from_wire_in_pos(meta,pos_send)
    local meta_send = minetest.get_meta(pos_send)
    owl_tech:set_charge(meta,owl_tech:get_charge(meta)-owl_tech:get_voltage(meta))--sender
    owl_tech:set_charge(meta_send,owl_tech:get_charge(meta_send)+owl_tech:get_voltage(meta_send))--resiver
end
-------------------------------------------
--send for all sides in 1 tick
function owl_tech.send_for_all_sides_wire_electro(meta,pos)
    local meta = minetest.get_meta(pos)
    local side_andres = owl_tech.get_need_wire_to_sand_electro(pos,meta)
        --+Y
        if side_andres==1  then
            local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x,y=pos.y+1,z=pos.z})
            if can_do  then
                owl_tech.send_electro_from_wire_in_pos(meta,{x=pos.x,y=pos.y+1,z=pos.z})
            end
        end    
        ---Y
        if side_andres==2  then
            local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x,y=pos.y-1,z=pos.z})
            if can_do  then
                owl_tech.send_electro_from_wire_in_pos(meta,{x=pos.x,y=pos.y-1,z=pos.z})
            end
        end
            --+X
        if side_andres==3  then
            local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x+1,y=pos.y,z=pos.z})
            if can_do  then
                owl_tech.send_electro_from_wire_in_pos(meta,{x=pos.x+1,y=pos.y,z=pos.z})
            end
        end
            ---X
        if side_andres==4  then
            local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x-1,y=pos.y,z=pos.z})
            if can_do  then
                owl_tech.send_electro_from_wire_in_pos(meta,{x=pos.x-1,y=pos.y,z=pos.z})
            end
        end
            --+Z
        if side_andres==5 then
            local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x,y=pos.y,z=pos.z+1})
            if can_do  then
                owl_tech.send_electro_from_wire_in_pos(meta,{x=pos.x,y=pos.y,z=pos.z+1})
            end
        end
            ---Z
        if side_andres==6  then
            local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x,y=pos.y,z=pos.z-1})
            if can_do  then
                owl_tech.send_electro_from_wire_in_pos(meta,{x=pos.x,y=pos.y,z=pos.z-1})
            end
        end
end
-------------------------------------------
--get need pipe to send 
function owl_tech.get_need_wire_to_sand_electro(pos,meta)
    local all_sede_table ={}
    all_sede_table[1]=math.huge
    all_sede_table[2]=math.huge
    all_sede_table[3]=math.huge
    all_sede_table[4]=math.huge
    all_sede_table[5]=math.huge
    all_sede_table[6]=math.huge
    --+Y
    if  minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0 
    and owl_tech:get_charge(minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z}))<owl_tech:get_charge(meta) then 
        local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x,y=pos.y+1,z=pos.z})
        if can_do then -- remove_amount
            all_sede_table[1]= owl_tech:get_charge(minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z}))
        end
    end    
    ---Y
    if  minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"owl_tech_electro_wire")>0 
    and owl_tech:get_charge(minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z}))<owl_tech:get_charge(meta) then 
        local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x,y=pos.y-1,z=pos.z})
        if can_do then -- remove_amount
            all_sede_table[2]= owl_tech:get_charge(minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z}))
        end
    end  
    --+X
    if  minetest.get_item_group((minetest.get_node({x=pos.x+1,y=pos.y,z=pos.z})).name,"owl_tech_electro_wire")>0 
    and owl_tech:get_charge(minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z}))<owl_tech:get_charge(meta) then 
        local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x+1,y=pos.y,z=pos.z})
        if can_do then -- remove_amount
            all_sede_table[3]= owl_tech:get_charge(minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z}))
        end
    end  
    ---X
    if  minetest.get_item_group((minetest.get_node({x=pos.x-1,y=pos.y,z=pos.z})).name,"owl_tech_electro_wire")>0 
    and owl_tech:get_charge(minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z}))<owl_tech:get_charge(meta) then 
        local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x-1,y=pos.y,z=pos.z})
        if can_do then -- remove_amount
            all_sede_table[4]= owl_tech:get_charge(minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z}))
        end
    end 
    --+Z
    if  minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+1})).name,"owl_tech_electro_wire")>0 
    and owl_tech:get_charge(minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1}))<owl_tech:get_charge(meta) then 
        local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x,y=pos.y,z=pos.z+1})
        if can_do then -- remove_amount
            all_sede_table[5]= owl_tech:get_charge(minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1}))
        end
    end 
    ---Z
    if  minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z-1})).name,"owl_tech_electro_wire")>0 
    and owl_tech:get_charge(minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1}))<owl_tech:get_charge(meta) then 
        local can_do  =owl_tech.can_send_electro_in_node(meta,{x=pos.x,y=pos.y,z=pos.z-1})
        if can_do then -- remove_amount
            all_sede_table[6]= owl_tech:get_charge(minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1}))
        end
    end 
    local side_andres =-1
    local min = math.huge
    for i = 1, #all_sede_table  do
        if min > all_sede_table[i]  then
            min = all_sede_table[i] 
            side_andres =i 
        end
    end
    return side_andres 
end