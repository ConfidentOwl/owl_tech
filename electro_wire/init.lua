local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

dofile(path.."/electro_wire/electro_api.lua")
dofile(path.."/electro_wire/electro_wire.lua")