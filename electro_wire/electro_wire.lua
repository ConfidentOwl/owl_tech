local S = minetest.get_translator(minetest.get_current_modname())

--Copper wire  
minetest.register_node("owl_tech:copper_electro_wire",{
    description = "Copper electro wire tire 1",
    _tt_help = S("Transport electro enegry"),
    _doc_items_longdesc = S("Fence gates can be opened or closed and can't be jumped over. Fences will connect nicely to fence gates."),
    _doc_items_usagehelp = S("Right-click the fence gate to open or close it."),
    tiles = {"(owl_tech_base_fluid_pipe.png^[colorize:#ff5e00:128)^owl_tech_base_wire.png"},
    paramtype = "light",
    is_ground_content = false,
    stack_max = 64,
    sunlight_propagates = true,
    walkable = true,
    groups = {handy = 1 , owl_tech_electro_wire = 1},
    drawtype = "nodebox",
    node_box = {
        type = "connected",
        fixed = {-0.125,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_top = {-0.125,-0.125,-0.125,0.125,0.5,0.125} ,
        connect_bottom = {-0.125,-0.5,-0.125,0.125,0.125,0.125} ,
        connect_front = {-0.125,-0.125,-0.5,0.125,0.125,0.125} ,
        connect_left = {-0.5,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_back = {-0.125,-0.125,-0.125,0.125,0.125,0.5} ,
        connect_right = {-0.125,-0.125,-0.125,0.5,0.125,0.125} ,
    },
    connects_to = {"group:owl_tech_electro_wire","group:owl_tech_electro_mashine","group:owl_tech_electro_gen","group:owl_tech_electro_battery"},
    sounds = mcl_sounds.node_sound_wool_defaults(),
    _mcl_hardness = 0.1,
    _mcl_blast_resistance = 0.1,
    on_construct = function(pos)
        local meta =  minetest.get_meta(pos)
        owl_tech:prepear_vire(pos,32)
        owl_tech.check_all_side_for_wire_work(pos,meta)
        owl_tech.update_wire_around(pos)
        local timer =minetest.get_node_timer(pos)
        meta:set_string("infotext", owl_tech:get_charge(meta))
        timer:start(0.2)
    end,
    on_timer = function (pos, elapsed)
        local meta =  minetest.get_meta(pos)
        if owl_tech:get_charge(meta)>0 then
            owl_tech.send_for_all_sides_wire_electro(meta,pos)
        end
        meta:set_string("infotext", owl_tech:get_charge(meta))
        local timer =minetest.get_node_timer(pos)
        timer:start(0.2)
    end,
    --[[after_dig_node = function(pos, oldnode, oldmetadata, digger)

    end]]
})
--Crafte iron fluid pipe
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:copper_electro_wire 4",
    recipe = {
        {"owl_tech:copper_plate","owl_tech:ruber_dust","owl_tech:copper_plate"},
        {"","",""},
        {"","",""}
    }
})