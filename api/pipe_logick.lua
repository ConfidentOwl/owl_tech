--[[
    0-nothing
    1-get\set_somthing
]]
local conetc_to={
    "owl_tech_generator","owl_tech_fluid_pipe","owl_tech_machine"
}
--node in pos ?in list 
function owl_tech.node_in_pos_connect_to_fluid_pipe(pos)
    local ret=0
    for index, value in ipairs(conetc_to) do
        if minetest.get_item_group((minetest.get_node(pos)).name,"owl_tech_fluid_pipe")>0 then
            ret=1
        end
    end
    return ret
end
-------------------------------------------
--Getter and setter conection amount 
function owl_tech.get_conection_amount_fluid(meta)
    return meta:get_int("conection_amount")
end
function owl_tech.set_conection_amount_fluid(meta,number)
    meta:set_int("conection_amount",number)
end
function  owl_tech.add_1_conection_amount_fluid(meta)
    meta:set_int("conection_amount",meta:get_int("conection_amount")+1)
end
-------------------------------------------
--setter and getter  fluid sander in 1 tick
function owl_tech.get_fluid_sand_in_tick(meta)
    return meta:get_int("fluid_sand_in_tick")
end
function owl_tech.set_fluid_sand_in_tick(meta,number)
    meta:set_int("fluid_sand_in_tick",number)
end
-------------------------------------------
--setter and geters for all 6 sides info
function owl_tech.set_side_info_y_p(meta,number)--y+
    meta:set_int("+y",number)
end
function owl_tech.set_side_info_y_m(meta,number)--y-
    meta:set_int("-y",number)
end
function owl_tech.set_side_info_x_p(meta,number)--x+
    meta:set_int("+x",number)
end
function owl_tech.set_side_info_x_m(meta,number)--x-
    meta:set_int("-x",number)
end
function owl_tech.set_side_info_z_p(meta,number)--z+
    meta:set_int("+z",number)
end
function owl_tech.set_side_info_z_m(meta,number)--z-
    meta:set_int("-z",number)
end
function owl_tech.get_side_info_y_p(meta)--y+
    return meta:get_int("+y")
end
function owl_tech.get_side_info_y_m(meta)--y-
    return meta:get_int("-y")
end
function owl_tech.get_side_info_x_p(meta)--x+
    return meta:get_int("+x")
end
function owl_tech.get_side_info_x_m(meta)--x-
    return meta:get_int("-x")
end
function owl_tech.get_side_info_z_p(meta)--z+
    return meta:get_int("+z")
end
function owl_tech.get_side_info_z_m(meta)--z-
    return meta:get_int("-z")
end
-------------------------------------------
--update pipe around 
function owl_tech.update_fluid_pipe_around(pos)
    local mass_pos ={
        {x=pos.x,y=pos.y+1,z=pos.z},
        {x=pos.x,y=pos.y-1,z=pos.z},
        {x=pos.x+1,y=pos.y,z=pos.z},
        {x=pos.x-1,y=pos.y,z=pos.z},
        {x=pos.x,y=pos.y,z=pos.z+1},
        {x=pos.x,y=pos.y,z=pos.z-1}
    }
    for i, value in ipairs(mass_pos) do
        if minetest.get_item_group((minetest.get_node(mass_pos[i])).name,"fluid_pipe")>0  then
            local meta =  minetest.get_meta(mass_pos[i])
            owl_tech.check_all_side_for_fluid_work(mass_pos[i],meta) 
        end
    end
end
------------------------------
--Chek all side for work and set it
function owl_tech.check_all_side_for_fluid_work(pos,meta)
    owl_tech.set_conection_amount_fluid(meta,0)--reset conection amount

    owl_tech.set_side_info_y_p(meta,owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x,y=pos.y+1,z=pos.z}))--y+
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x,y=pos.y+1,z=pos.z}))

    owl_tech.set_side_info_y_m(meta,owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x,y=pos.y-1,z=pos.z}))--y-
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x,y=pos.y-1,z=pos.z}))

    owl_tech.set_side_info_x_p(meta,owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x+1,y=pos.y,z=pos.z}))--x+
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x+1,y=pos.y,z=pos.z}))

    owl_tech.set_side_info_x_m(meta,owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x-1,y=pos.y,z=pos.z}))--x-
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x-1,y=pos.y,z=pos.z}))

    owl_tech.set_side_info_z_p(meta,owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x,y=pos.y,z=pos.z+1}))--z+
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x,y=pos.y,z=pos.z+1}))

    owl_tech.set_side_info_z_m(meta,owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x,y=pos.y,z=pos.z-1}))--z-
    owl_tech.set_conection_amount_fluid(meta,owl_tech.get_conection_amount_fluid(meta)+owl_tech.node_in_pos_connect_to_fluid_pipe({x=pos.x,y=pos.y,z=pos.z-1}))
end
-------------------------------------------
--chek can send fluid in pipe\mashine
function owl_tech.can_send_fluid_in_node(meta,pos_send,send_amount)
    local flyid_name= owl_tech.get_pull_fluid_name(meta,0)
    local meta_send = minetest.get_meta(pos_send)
    local can_sand ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_send,flyid_name,send_amount)
    return can_sand ,inde_pull 
end
-------------------------------------------
--send fluid in pos by pipe
function owl_tech.send_fluid_from_pipe_in_pos(meta,pos_send,send_amount)
    local meta_send = minetest.get_meta(pos_send)
    local flyid_name= owl_tech.get_pull_fluid_name(meta,0)
    local can_sand , inde_pull = owl_tech.can_send_fluid_in_node(meta_send,pos_send,send_amount)
    if can_sand then
        owl_tech.add_fluid_in_node_pull(meta_send,flyid_name,send_amount,inde_pull)
    end
end
-------------------------------------------
--send for all sides in 1 tick
function owl_tech.send_for_all_sides_fluid_pipe(meta,pos)
    local send_amount = owl_tech.get_fluid_sand_in_tick(meta)
    local flyid_name= owl_tech.get_pull_fluid_name(meta,1)
    local side_andres = owl_tech.get_need_pipe_to_send_fluid(pos,meta)
        --+Y
        if side_andres==1  then
            local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
            local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
            local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
            if can_do and can_do2 then
                owl_tech.add_fluid_in_node_pull(meta_up,flyid_name,remove_amount,inde_pull)
                owl_tech.remove_fluid_in_node_pull(meta,remove_amount,inde_pull)
            end
        end    
        ---Y
        if side_andres==2  then
            local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
            local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
            local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
            if can_do and can_do2 then
                owl_tech.add_fluid_in_node_pull(meta_up,flyid_name,remove_amount,inde_pull)
                owl_tech.remove_fluid_in_node_pull(meta,remove_amount,inde_pull)
            end
        end
            --+X
        if side_andres==3  then
            local meta_up = minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z})
            local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
            local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
            if can_do and can_do2 then
                owl_tech.add_fluid_in_node_pull(meta_up,flyid_name,remove_amount,inde_pull)
                owl_tech.remove_fluid_in_node_pull(meta,remove_amount,inde_pull)
            end
        end
            ---X
        if side_andres==4  then
            local meta_up = minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z})
            local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
            local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
            if can_do and can_do2 then
                owl_tech.add_fluid_in_node_pull(meta_up,flyid_name,remove_amount,inde_pull)
                owl_tech.remove_fluid_in_node_pull(meta,remove_amount,inde_pull)
            end
        end
            --+Z
        if side_andres==5 then
            local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1})
            local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
            local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
            if can_do and can_do2 then
                owl_tech.add_fluid_in_node_pull(meta_up,flyid_name,remove_amount,inde_pull)
                owl_tech.remove_fluid_in_node_pull(meta,remove_amount,inde_pull)
            end
        end
            ---Z
        if side_andres==6  then
            local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1})
            local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
            local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
            if can_do and can_do2 then
                owl_tech.add_fluid_in_node_pull(meta_up,flyid_name,remove_amount,inde_pull)
                owl_tech.remove_fluid_in_node_pull(meta,remove_amount,inde_pull)
            end
        end
end
-------------------------------------------
--get need pipe to send 
function owl_tech.get_need_pipe_to_send_fluid(pos,meta)
    local send_amount = owl_tech.get_fluid_sand_in_tick(meta)
    local flyid_name= owl_tech.get_pull_fluid_name(meta,1)
    local all_sede_table ={}
    all_sede_table[1]=math.huge
    all_sede_table[2]=math.huge
    all_sede_table[3]=math.huge
    all_sede_table[4]=math.huge
    all_sede_table[5]=math.huge
    all_sede_table[6]=math.huge
        --+Y
    if  minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_fluid_pipe")>0 
    and owl_tech.get_pull_volume(minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z}),1)<owl_tech.get_pull_volume(meta,1) then
        local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
        local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
        if can_do and can_do2 then -- remove_amount
            all_sede_table[1]=  owl_tech.get_pull_volume(minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z}),1)
        end
    end    
    ---Y
    if  minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"owl_tech_fluid_pipe")>0 
    and owl_tech.get_pull_volume(minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z}),1)<owl_tech.get_pull_volume(meta,1) then
        local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
        local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
        if can_do and can_do2 then
            all_sede_table[2]= owl_tech.get_pull_volume(minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z}),1)
        end
    end
    --+X
    if  minetest.get_item_group((minetest.get_node({x=pos.x+1,y=pos.y,z=pos.z})).name,"owl_tech_fluid_pipe")>0 
    and owl_tech.get_pull_volume(minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z}),1)<owl_tech.get_pull_volume(meta,1) then
        local meta_up = minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z})
        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
        local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
        if can_do and can_do2 then
            all_sede_table[3]=  owl_tech.get_pull_volume(minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z}),1)
        end
    end
    ---X
    if  minetest.get_item_group((minetest.get_node({x=pos.x-1,y=pos.y,z=pos.z})).name,"owl_tech_fluid_pipe")>0 
    and owl_tech.get_pull_volume(minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z}),1)<owl_tech.get_pull_volume(meta,1) then
        local meta_up = minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z})
        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
        local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
        if can_do and can_do2 then
            all_sede_table[4]= owl_tech.get_pull_volume(minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z}),1)
        end
    end
    --+Z
    if  minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+1})).name,"owl_tech_fluid_pipe")>0 
    and owl_tech.get_pull_volume(minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1}),1)<owl_tech.get_pull_volume(meta,1) then
        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1})
        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
        local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
        if can_do and can_do2 then
            all_sede_table[5]= owl_tech.get_pull_volume(minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1}),1)
        end
    end
    ---Z
    if  minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z-1})).name,"owl_tech_fluid_pipe")>0 
    and owl_tech.get_pull_volume(minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1}),1)<owl_tech.get_pull_volume(meta,1) then
        local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1})
        local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,flyid_name,send_amount)
        local can_do2 ,_ , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,flyid_name,send_amount)
        if can_do and can_do2 then
            all_sede_table[6]=  owl_tech.get_pull_volume(minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1}),1)
        end
    end 
    local side_andres =-1
    local min = math.huge
    for i = 1, #all_sede_table  do
        if min > all_sede_table[i]  then
            min = all_sede_table[i] 
            side_andres =i 
        end
    end
    return side_andres 
end