-- prepear for vertical steam miner
function owl_tech.prepear_for_verticak_steam_miner(pos)
    local  meta = minetest.get_meta(pos)
    owl_tech.add_new_pull(meta,16000,1,0) --steam  eat 25 steam in 1 tick
	owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
    meta:set_int("curent-y",1) -- use pos - curent-y for get diginbg pos
    meta:set_int("get_bedrock",0)
end
------------------------------------
--Get node drop
function owl_tech.get_node_drop(node_name)
    local drop=  minetest.get_node_drops(node_name, "toolname")
    return drop[1]
end
----------------------------------
-- can steam miner mine this node 
function owl_tech.can_vertical_steam_miner_mine_this(pos,meta)
    local node_mine = minetest.get_node_or_nil({x=pos.x,y=(pos.y-meta:get_int("curent-y")),z=pos.z})
    local can_do = true
    if node_mine~=nil then
        if node_mine.name =='mcl_core:bedrock'  then --bedrock mcl_core:bedrock
            can_do =false
            meta:set_int("get_bedrock",1)
            return can_do
        end
        for i, value in ipairs(GLOBAL_FLUID_LIST) do
            if node_mine.name ==GLOBAL_FLUID_LIST[i]  and not can_do then --finde fluid 
                can_do =false
                return can_do
            end
        end
        return can_do
    end
end
-----------------------------------
--mine verical steam
function owl_tech.vertical_steam_mine(pos,meta,can_mine)
    if can_mine then
        local inv = meta:get_inventory()
        local dst_its = inv:get_stack('dst', 1)
        local node_mine = minetest.get_node_or_nil({x=pos.x,y=(pos.y-meta:get_int("curent-y")),z=pos.z})
        if dst_its:is_empty() or (
            dst_its:get_stack_max()-dst_its:get_count()>0 and 
            node_mine.name == dst_its:get_name()
        ) then
            if dst_its:is_empty() and node_mine.name~='air'  then
                local new_stack = {name=owl_tech.get_node_drop(node_mine.name), count=1, wear=0, metadata=""}
                inv:set_stack('dst', 1,new_stack)
                minetest.set_node({x=pos.x,y=(pos.y-meta:get_int("curent-y")),z=pos.z}, {name="owl_tech:decor_bronze_pipe"})
                meta:set_int("curent-y",meta:get_int("curent-y")+1)
            elseif node_mine.name~='air' then
                dst_its:set_count(dst_its:get_count()+1)
                inv:set_stack('dst', 1,dst_its)
                minetest.set_node({x=pos.x,y=(pos.y-meta:get_int("curent-y")),z=pos.z}, {name="owl_tech:decor_bronze_pipe"})
                meta:set_int("curent-y",meta:get_int("curent-y")+1)
            elseif node_mine.name=='air' then
                minetest.set_node({x=pos.x,y=(pos.y-meta:get_int("curent-y")),z=pos.z}, {name="owl_tech:decor_bronze_pipe"})
                meta:set_int("curent-y",meta:get_int("curent-y")+1)
            end
        end
    end    
end
--------------------------------
--remove all vertical pipe
function owl_tech.remove_all_pipe_vertical(pos)
    pos.y=pos.y-1
    while minetest.get_node_or_nil(pos).name =="owl_tech:decor_bronze_pipe" do
        minetest.set_node(pos, {name="air"})
        pos.y=pos.y-1
    end
    do return end
end
-----------------------------------
-- Prerpear for quarry
function owl_tech.prepera_quarry(pos,max_distance)
    local  meta = minetest.get_meta(pos)
    owl_tech.add_new_pull(meta,32000,1,0) --steam 
	owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
    meta:set_int('horiz_max_distance',max_distance)
    meta:set_int("curent-y",1) -- use pos - curent-y for get diginbg pos
    meta:set_int("curent-x",0)
    meta:set_int("max_x",0)
    meta:set_int('min_x',0)
    meta:set_int("curent-z",0)
    meta:set_int("max_z",0)
    meta:set_int('min_z',0)
    meta:set_int("get_bedrock",0)
end
------------------------------------
--scan for point
function owl_tech.scan_for_point(pos,meta,scan_disctance)
    local x_pos = 0
    for i = (-1*scan_disctance), scan_disctance, 1 do
        local node = minetest.get_node_or_nil({x=pos.x+i,y=pos.y,z=pos.z})
        if node~=nil and node.name =="owl_tech:quary_point" then
            x_pos=i
        end
    end
    local z_pos = 0
    for i = (-1*scan_disctance), scan_disctance, 1 do
        local node = minetest.get_node_or_nil({x=pos.x,y=pos.y,z=pos.z+i})
        if node~=nil and node.name =="owl_tech:quary_point" then
            z_pos=i
        end
    end
    if x_pos~=0 and z_pos~=0 then
        if pos.x+x_pos>pos.x and pos.z+z_pos>pos.z  then -- ++
            meta:set_int("max_x",pos.x+x_pos)
            meta:set_int("min_x",pos.x )
            meta:set_int("max_z",pos.z+z_pos)
            meta:set_int("min_z",pos.z )
        elseif pos.x+x_pos>pos.x and pos.z+z_pos<pos.z  then -- +-
            meta:set_int("max_x",pos.x+x_pos)
            meta:set_int("min_x",pos.x )
            meta:set_int("max_z",pos.z)
            meta:set_int("min_z",pos.z+z_pos)
        elseif pos.x+x_pos<pos.x and pos.z+z_pos>pos.z  then -- -+
            meta:set_int("max_x",pos.x)
            meta:set_int("min_x",pos.x+x_pos)
            meta:set_int("max_z",pos.z+z_pos)
            meta:set_int("min_z",pos.z )
        elseif pos.x+x_pos<pos.x and pos.z+z_pos<pos.z  then -- --
            meta:set_int("max_x",pos.x)
            meta:set_int("min_x",pos.x+x_pos)
            meta:set_int("max_z",pos.z)
            meta:set_int("min_z",pos.z+z_pos)
        end
    else
        return
    end
    meta:set_int("curent-x",meta:get_int('min_x'))
    meta:set_int("curent-z",meta:get_int('min_z'))
end
-------------------------------------
--diging the quary
function owl_tech.diging_quary(pos,meta,can_mine)
    if can_mine then
        local inv = meta:get_inventory()
        local dst_its = inv:get_stack('dst', 1)
        local pos_dig={
            x=meta:get_int("curent-x"),
            y=pos.y-meta:get_int("curent-y"),
            z=meta:get_int("curent-z")
        }
        local node_mine = minetest.get_node_or_nil(pos_dig)
        if dst_its:is_empty() or (
            dst_its:get_stack_max()-dst_its:get_count()>0 and 
            node_mine.name == dst_its:get_name()
        ) then
            if dst_its:is_empty() and node_mine.name~='air'  then
                local new_stack = {name=owl_tech.get_node_drop(node_mine.name), count=1, wear=0, metadata=""}
                inv:set_stack('dst', 1,new_stack)
                minetest.remove_node(pos_dig)
                owl_tech.calculate_new_pos_for_quarry(meta)
            elseif node_mine.name~='air' then
                dst_its:set_count(dst_its:get_count()+1)
                inv:set_stack('dst', 1,dst_its)
                minetest.remove_node(pos_dig)
                owl_tech.calculate_new_pos_for_quarry(meta)
            elseif node_mine.name=='air' then
                minetest.remove_node(pos_dig)
                owl_tech.calculate_new_pos_for_quarry(meta)
            end
        end
    end  
end
------------------------------------
--calculate new pos for qurey
function owl_tech.calculate_new_pos_for_quarry(meta)
    if  meta:get_int("curent-x")< meta:get_int("max_x") then --x
        meta:set_int("curent-x",meta:get_int("curent-x")+1)
        do return end
    end
    if meta:get_int("curent-x")==meta:get_int("max_x")  then
        if meta:get_int("curent-z")==meta:get_int("max_z") then
            meta:set_int("curent-x",meta:get_int("min_x")) 
            meta:set_int("curent-z",meta:get_int("min_z")) 
            meta:set_int("curent-y",meta:get_int("curent-y")+1)
            do return end
        end
        meta:set_int("curent-x",meta:get_int("min_x")) 
        meta:set_int("curent-z",meta:get_int("curent-z")+1) 
    end
end
----------------------------------
-- can quary mine this node 
function owl_tech.can_quary_mine_this(pos,meta)
    local pos_dig={
        x=meta:get_int("curent-x"),
        y=pos.y-meta:get_int("curent-y"),
        z=meta:get_int("curent-z")
    }
    local node_mine = minetest.get_node_or_nil(pos_dig)
    local can_do = true
    if node_mine~=nil then
        if node_mine.name =='mcl_core:bedrock'  then --bedrock mcl_core:bedrock
            can_do =false
            meta:set_int("get_bedrock",1)
            return can_do
        end
        for i, value in ipairs(GLOBAL_FLUID_LIST) do
            if node_mine.name ==GLOBAL_FLUID_LIST[i]  and not can_do then --finde fluid 
                minetest.remove_node(pos_dig)
                return can_do
            end
        end
        return can_do
    end
end