--Setter and getter pull_amount
function owl_tech.get_pull_amount(meta)
   return meta:get_int("pull_amount")
end

function owl_tech.set_pull_amount(meta,new_amount)
    meta:set_int("pull_amount",new_amount)
    return owl_tech.get_pull_amount(meta)
end
------------------------------
--Setter and getter pull_volume
function owl_tech.get_pull_volume(meta,namber)
    return meta:get_float(namber.."_current_volume")
end

function owl_tech.set_pull_volume(meta,namber,volume)
    meta:set_float(namber.."_current_volume",volume)
    return owl_tech.get_pull_volume(meta,namber)
end
------------------------------
--Setter and getter pull_max_volume
function owl_tech.get_pull_max_volume(meta,namber)
    return meta:get_float(namber.."_max_volume")
end

function owl_tech.set_pull_max_volume(meta,namber,volume)
    meta:set_float(namber.."_max_volume",volume)
    return owl_tech.get_pull_max_volume(meta,namber)
end
------------------------------
--Setter and getter fluid name
function owl_tech.get_pull_fluid_name(meta,namber)
    return meta:get_string(namber.."_fluid_name")
end

function owl_tech.set_pull_fluid_name(meta,namber,volume)
    meta:set_string(namber.."_fluid_name",volume)
    return owl_tech.get_pull_fluid_name(meta,namber)
end
------------------------------
--Setter and getter can fluid input use 0 and 1!!!
function owl_tech.get_pull_fluid_input(meta,namber)
    return meta:get_int(namber.."_input")
end

function owl_tech.set_pull_fluid_input(meta,namber,input)
    meta:set_int(namber.."_input",input)
    return owl_tech.get_pull_fluid_input(meta,namber)
end
------------------------------
--Setter and getter can fluid output 0 and 1!!!
function owl_tech.get_pull_fluid_output(meta,namber)
    return meta:get_int(namber.."_output")
end

function owl_tech.set_pull_fluid_output(meta,namber,input)
    meta:set_int(namber.."_output",input)
    return owl_tech.get_pull_fluid_output(meta,namber)
end
------------------------------
--whitlist for fluid tanks (Only 1 fluid can put in whitelist)
function owl_tech.get_pull_fluid_whitlist(meta,namber)
    return meta:get_string(namber.."_whitlist")
end

function owl_tech.set_pull_fluid_whitlist(meta,namber,name)
    meta:set_string(namber.."_whitlist",name)
    return owl_tech.get_pull_fluid_whitlist(meta,namber)
end

function owl_tech.remove_pull_fluid_whitlist(meta,namber)
    meta:set_string(namber.."_whitlist","none")
    return owl_tech.get_pull_fluid_whitlist(meta,namber)
end
------------------------------
--Add new pull 
function owl_tech.add_new_pull(meta,max_volume,input,output)
    owl_tech.set_pull_amount(meta,owl_tech.get_pull_amount(meta)+1)
    local pull_amount = owl_tech.get_pull_amount(meta)
    owl_tech.set_pull_max_volume(meta,pull_amount,max_volume)
    owl_tech.set_pull_volume(meta,pull_amount,0)
    owl_tech.set_pull_fluid_name(meta,pull_amount,"none")
    owl_tech.set_pull_fluid_input(meta,pull_amount,input)--use int not bool((
    owl_tech.set_pull_fluid_output(meta,pull_amount,output)
    owl_tech.remove_pull_fluid_whitlist(meta,pull_amount)
end
------------------------------
--finde tank in  node for add some fluid(local func )
function owl_tech.finde_tank_in_node_for_add_fluid_amount(meta,pulls,fluid_indef,fluid_amount)
    local ret =0
    for i = 0, pulls, 1 do
        local pull_max_volume = owl_tech.get_pull_max_volume(meta,i)
        local pull_curent_volume = owl_tech.get_pull_volume(meta,i)
        local difer = pull_curent_volume+fluid_amount
        if owl_tech.get_pull_fluid_name(meta,i)=="none" or (owl_tech.get_pull_fluid_name(meta,i)==fluid_indef and difer<=pull_max_volume )then
            ret =i
            break  
        end
    end
    return ret

end
------------------------------
--finde tank in side node for remove some fluid(local func )
function owl_tech.finde_tank_in_node_for_remove_fluid_amount(meta,pulls,fluid_indef,fluid_amount)
    for i = 0, pulls, 1 do
        local pull_curent_volume = owl_tech.get_pull_volume(meta,i)
        local difer = pull_curent_volume-fluid_amount
        if (owl_tech.get_pull_fluid_name(meta,i)==fluid_indef and difer>=0 )then
            return i   
        end
    end
end
------------------------------
--Fluid from itemslot
function owl_tech.add_fluid_in_pull_from_itemslot(meta,itemstak_buck,inv,itemstak_dst,slot_name_fuel,slot_name_dst)
    local pulls=owl_tech.get_pull_amount(meta)
    local name_item_fluid= itemstak_buck:get_name() 
    local fluid_indef=""
    local epmty_sel_name =""
    if name_item_fluid=="mcl_buckets:bucket_water" or name_item_fluid=="mcl_buckets:bucket_river_water" then
        fluid_indef="mcl_core:water_source"
        epmty_sel_name= "mcl_buckets:bucket_empty"
    elseif name_item_fluid=="mcl_buckets:bucket_lava" then
        fluid_indef="mcl_core:lava_source"
        epmty_sel_name= "mcl_buckets:bucket_empty"
    end
    local can_return_buck= false
    if itemstak_dst:is_empty() then
        can_return_buck =true
    elseif  itemstak_dst:get_free_space()>0 and itemstak_dst:get_name()==epmty_sel_name then
        can_return_buck =true
    end
    if pulls>0 and fluid_indef~="" and can_return_buck then
        local inde_pull =owl_tech.finde_tank_in_node_for_add_fluid_amount(meta,pulls,fluid_indef,1000)--1000 is 1 bucket\cell
        if inde_pull~=0 and  (owl_tech.get_pull_fluid_whitlist(meta,inde_pull)==fluid_indef  or owl_tech.get_pull_fluid_whitlist(meta,inde_pull)=="none" )then
            local volume = owl_tech.get_pull_volume(meta,inde_pull)+1000
            owl_tech.set_pull_volume(meta,inde_pull,volume)
            local amount_buck = itemstak_buck:get_count()
            owl_tech.set_pull_fluid_name(meta,inde_pull,fluid_indef)
            if amount_buck>1 then
                itemstak_buck:set_count(amount_buck-1)
                inv:set_stack(slot_name_fuel, 1, itemstak_buck)
                if itemstak_dst:is_empty() then --retrun emty sell
                    local item ={name=epmty_sel_name, count=1, wear=0, metadata=""}
                    itemstak_dst:add_item(item)
                    inv:set_stack(slot_name_dst, 1, itemstak_dst)
                    return true
                elseif not  itemstak_dst:is_empty() then
                    local item ={name=epmty_sel_name, count=itemstak_dst:get_count()+1, wear=0, metadata=""}
                    itemstak_dst:add_item(item)
                    inv:set_stack(slot_name_dst, 1, itemstak_dst)
                    return true
                end
            else 
                itemstak_buck:clear()
                inv:set_stack(slot_name_fuel, 1, itemstak_buck)
                if itemstak_dst:is_empty() then --retrun emty sell
                    local item ={name=epmty_sel_name, count=itemstak_dst:get_count()+1, wear=0, metadata=""}
                    itemstak_dst:add_item(item)
                    inv:set_stack(slot_name_dst, 1, itemstak_dst)
                    return true
                elseif not  itemstak_dst:is_empty() then
                    itemstak_dst:set_count(itemstak_dst:get_count()+1)
                    inv:set_stack(slot_name_dst, 1, itemstak_dst)
                    return true
                end
            end
        else
            return false 
        end
    end
end
------------------------------
--can add fluid in node pulls 
function owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name,fluid_amount)
    local pulls=owl_tech.get_pull_amount(meta)
    if pulls>0  then
        local inde_pull =owl_tech.finde_tank_in_node_for_add_fluid_amount(meta,pulls,fluid_name,fluid_amount)
        if inde_pull~=0 and (owl_tech.get_pull_fluid_whitlist(meta,inde_pull)==fluid_name  or owl_tech.get_pull_fluid_whitlist(meta,inde_pull)=="none" )then
            return true ,inde_pull
        else
            return false ,inde_pull
        end
    end
end
------------------------------
--add fluid in node 
function owl_tech.add_fluid_in_node_pull(meta,fluid_name,fluid_amount,pull_number)
    local volume = owl_tech.get_pull_volume(meta,pull_number)+fluid_amount
    owl_tech.set_pull_volume(meta,pull_number,volume)
    owl_tech.set_pull_fluid_name(meta,pull_number,fluid_name)  
end
------------------------------
--can remove fluid in node pulls 
function owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name,fluid_amount)
    local pulls=owl_tech.get_pull_amount(meta)
    local remove_amount=0 
    if pulls>0  then
        local inde_pull=owl_tech.finde_tank_in_node_for_remove_fluid_amount(meta,pulls,fluid_name,fluid_amount)
        if inde_pull~=nil and inde_pull~=0 and (owl_tech.get_pull_fluid_whitlist(meta,inde_pull)==fluid_name  or owl_tech.get_pull_fluid_whitlist(meta,inde_pull)=="none" )then
            if owl_tech.get_pull_volume(meta,inde_pull)>=fluid_amount then
                remove_amount=fluid_amount
            else
                remove_amount=owl_tech.get_pull_volume(meta,inde_pull)
            end
            return true ,inde_pull ,remove_amount
        else
            return false ,inde_pull ,remove_amount
        end
    end
end
------------------------------
--remove  fluid in node 
function owl_tech.remove_fluid_in_node_pull(meta,fluid_amount,pull_number)
    local pull_curent_volume = owl_tech.get_pull_volume(meta,pull_number)
    local difer = pull_curent_volume-fluid_amount
    owl_tech.set_pull_volume(meta,pull_number,difer)
    if difer ==0 then
        owl_tech.set_pull_fluid_name(meta,pull_number,"none")  
    end
end
------------------------------
--Chek for name thit 0 amount fluid and has name
function owl_tech.delit_name_fluid_if_0(meta)
    local pulls_amount = owl_tech.get_pull_amount(meta)
    for i = 1,pulls_amount, 1 do
        if owl_tech.get_pull_volume(meta,i)==0 then
            owl_tech.set_pull_fluid_name(meta,i,"none")  
        end
    end
end
---------------------------------
--Getter and setter conection amount 
function owl_tech.get_conection_amount_item(meta)
    return meta:get_int("conection_amount")
end
function owl_tech.set_conection_amount_item(meta,number)
    meta:set_int("conection_amount",number)
end
function  owl_tech.add_1_conection_amount_itemd(meta)
    meta:set_int("conection_amount",meta:get_int("conection_amount")+1)
end
-------------------------------------------