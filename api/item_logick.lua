--all logick of item 
--settera and geters for calculate item_redines (always max -100)
function owl_tech.get_item_redines(meta)
    return meta:get_int("_redines")
end
function owl_tech.set_item_redines(meta,amount)
    meta:set_int("_redines", amount)
end
function owl_tech.reset_item_redines(meta)
    meta:set_int("_redines", 0)
end
function owl_tech.add_item_redines(meta,amount)
    meta:set_int("_redines",owl_tech.get_item_redines(meta)+amount)
end
----------------------------------------------
-- calculate add bnus for speed work if recipt tire is low that tire mashine ( 1 lv give 20%)
function owl_tech.calculate_recipe_tire_bonus(meta,recip_tire,recip_dens)
    return recip_dens+(recip_dens*(owl_tech.get_mashine_tire(meta)-recip_tire)*0.2)
end
----------------------------------------------
----Logick for item pipe
function owl_tech.set_item_pipe_logick(meta)
    local inv = meta:get_inventory()
    inv:set_size("pipe_inv", 1) 
end
----------------------------------------------
--get item from inventory to pipe 
function owl_tech.finde_inventory_slot_for_removing_item_around(pos)
    local mass_pos ={
        {x=pos.x,y=pos.y+1,z=pos.z},
        {x=pos.x,y=pos.y-1,z=pos.z},
        {x=pos.x+1,y=pos.y,z=pos.z},
        {x=pos.x-1,y=pos.y,z=pos.z},
        {x=pos.x,y=pos.y,z=pos.z+1},
        {x=pos.x,y=pos.y,z=pos.z-1}
    }
    local meta = minetest.get_meta(pos)
    local inv = meta:get_inventory() -- pipe
    for i, value in ipairs(mass_pos) do
        for j, value1 in pairs(OUTPUT_SLOT_NAME) do
            if minetest.get_item_group((minetest.get_node(mass_pos[i])).name,OUTPUT_SLOT_NAME[j])>0 then-- for special item slots
                local meta_source = minetest.get_meta(mass_pos[i])
                local inv_sourc =  meta_source:get_inventory()
                if inv:is_empty("pipe_inv") and  not inv_sourc:is_empty(OUTPUT_SLOT_NAME[j])  then
                    if inv:room_for_item("pipe_inv", inv_sourc:get_stack(OUTPUT_SLOT_NAME[j], 1)) and meta:get_int("has_filte")==0 then-- no filters
                        inv:add_item("pipe_inv", inv_sourc:get_stack(OUTPUT_SLOT_NAME[j], 1))
                        inv_sourc:set_stack(OUTPUT_SLOT_NAME[j],1,'')
                        do return end                        
                    end
                    if inv:room_for_item("pipe_inv", inv_sourc:get_stack(OUTPUT_SLOT_NAME[j], 1)) and meta:get_int("has_filte")==1 then-- filters
                        local filte_stack = inv:get_stack("filte_itemsteack", 1)
                        if filte_stack:get_name()== (inv_sourc:get_stack(OUTPUT_SLOT_NAME[j], 1)):get_name() then
                            inv:add_item("pipe_inv", inv_sourc:get_stack(OUTPUT_SLOT_NAME[j], 1))
                            inv_sourc:set_stack(OUTPUT_SLOT_NAME[j],1,'')
                            do return end   
                        end
                    end
                end 
            end
        end
        if minetest.get_node(mass_pos[i]).name=='mcl_chests:chest_small' 
        or minetest.get_node(mass_pos[i]).name=="mcl_chests:ender_chest"
        or minetest.get_node(mass_pos[i]).name=="mcl_chests:ender_chest_small" then -- for chest type
            local meta_source = minetest.get_meta(mass_pos[i]) 
            local inv_sourc =  meta_source:get_inventory()
            if inv:is_empty("pipe_inv") and  not inv_sourc:is_empty("main")then
                for k = 1, #(inv_sourc:get_list("main")),1 do
                    if inv:room_for_item("pipe_inv", inv_sourc:get_stack("main", k)) and not (inv_sourc:get_stack("main", k)):is_empty() then
                        inv:add_item("pipe_inv", inv_sourc:get_stack("main", k))
                        inv_sourc:set_stack("main", k,'')
                        do return end                        
                    end
                end
            end 
        end
        if minetest.get_item_group((minetest.get_node(mass_pos[i])).name,"owl_tech_can_pipe_output")>0 then
            local meta_source = minetest.get_meta(mass_pos[i])
            local inv_sourc =  meta_source:get_inventory()
            if (not inv:is_empty("pipe_inv")) and inv_sourc:is_empty("pipe_inv")  then
                if inv_sourc:room_for_item("pipe_inv", inv:get_stack("pipe_inv", 1)) then
                    local itemstack=  inv:get_stack("pipe_inv", 1)
                    inv_sourc:add_item("pipe_inv",itemstack)
                    inv:set_stack("pipe_inv", 1, '')
                    do return end                        
                end
            end 
        end
    end
end
------------------------------------------- 
--get item pipe to pipe 
function owl_tech.finde_pipe_and_send_item(pos)
    local mass_pos ={
        {x=pos.x,y=pos.y+1,z=pos.z},
        {x=pos.x,y=pos.y-1,z=pos.z},
        {x=pos.x+1,y=pos.y,z=pos.z},
        {x=pos.x-1,y=pos.y,z=pos.z},
        {x=pos.x,y=pos.y,z=pos.z+1},
        {x=pos.x,y=pos.y,z=pos.z-1}
    }
    local meta = minetest.get_meta(pos)
    local inv = meta:get_inventory()
    local rand = math.random(1,6)
    if minetest.get_item_group((minetest.get_node(mass_pos[rand])).name,"owl_tech_can_pipe_output")>0 then
        local meta_source = minetest.get_meta(mass_pos[rand])
        local inv_sourc =  meta_source:get_inventory()
        if (not inv:is_empty("pipe_inv")) and inv_sourc:is_empty("pipe_inv") then
            if inv_sourc:room_for_item("pipe_inv", inv:get_stack("pipe_inv", 1)) then
                local itemstack=  inv:get_stack("pipe_inv", 1)
                inv_sourc:add_item("pipe_inv",itemstack)
                inv:set_stack("pipe_inv", 1, '')
                do return end
            end
        end 
    end
end
-----------------------------------------------------
--Send item in pipe
function owl_tech.send_item_in_inventory(pos)
    local meta = minetest.get_meta(pos)
    local mass_pos ={
        {x=pos.x,y=pos.y+1,z=pos.z},
        {x=pos.x,y=pos.y-1,z=pos.z},
        {x=pos.x+1,y=pos.y,z=pos.z},
        {x=pos.x-1,y=pos.y,z=pos.z},
        {x=pos.x,y=pos.y,z=pos.z+1},
        {x=pos.x,y=pos.y,z=pos.z-1}
    }
    for i, value in ipairs(mass_pos) do
        local name= minetest.get_node(mass_pos[i]).name
        for j, value in pairs(INPUT_SLOT_NAME) do
            local meta = minetest.get_meta(pos)
            local inv = meta:get_inventory() -- pipe
            if minetest.get_item_group((minetest.get_node(mass_pos[i])).name,"owl_tech_can_pipe_output")>0 then --get item from enather pipe
                local meta_source = minetest.get_meta(mass_pos[i])
                local inv_sourc =  meta_source:get_inventory()
                if inv:is_empty("pipe_inv") and  not inv_sourc:is_empty("pipe_inv")  then
                    if inv:room_for_item("pipe_inv", inv_sourc:get_stack("pipe_inv", 1)) and not (inv_sourc:get_stack("pipe_inv", 1)):is_empty() then
                        inv:set_stack("pipe_inv", 1, inv_sourc:get_stack("pipe_inv", 1))
                        inv_sourc:set_stack("pipe_inv", 1,'' )
                    end
                end 
            end
            if minetest.get_node(mass_pos[i]).name=='mcl_chests:chest_small'
                or minetest.get_node(mass_pos[i]).name=="mcl_chests:trapped_chest_small"
                or minetest.get_node(mass_pos[i]).name=="mcl_chests:ender_chest"
                or minetest.get_node(mass_pos[i]).name=="mcl_chests:ender_chest_small" then -- for chest 
                local meta_source = minetest.get_meta(mass_pos[i])
                local inv_sourc =  meta_source:get_inventory()
                if not  inv:is_empty("pipe_inv")  then
                    for k = 1, #(inv_sourc:get_list("main")),1 do
                        if (inv_sourc:room_for_item("main", inv:get_stack("pipe_inv", 1)) and not ((inv:get_stack("pipe_inv", 1)):is_empty() ))then
                            inv_sourc:add_item("main", inv:get_stack("pipe_inv", 1))
                            inv:set_stack("pipe_inv", 1,'' )
                            do return end                        
                        end
                    end
                end 
            end
            for j, value1 in pairs(INPUT_SLOT_NAME) do
                if minetest.get_item_group((minetest.get_node(mass_pos[i])).name,INPUT_SLOT_NAME[j])>0 then-- for special item slots
                    local meta_source = minetest.get_meta(mass_pos[i])
                    local inv_sourc =  meta_source:get_inventory()
                    if not inv:is_empty("pipe_inv") and  inv_sourc:is_empty(INPUT_SLOT_NAME[j])  then
                        if inv_sourc:room_for_item(INPUT_SLOT_NAME[j], inv:get_stack("pipe_inv", 1)) and meta:get_int("has_filte")==0 then-- no filters
                            inv_sourc:add_item(INPUT_SLOT_NAME[j], inv:get_stack("pipe_inv", 1))
                            inv:set_stack("pipe_inv",1,'')
                            do return end                        
                        end
                    end 
                end
            end
        end
    end
end
