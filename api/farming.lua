--Is wood in pos
function owl_tech.is_wood_in_pos(pos)
   if minetest.get_item_group((minetest.get_node(pos)).name,"tree")>0 then
        return true
   end 
   return false
end
---------------------------------------------
--Is leaves in pos
function owl_tech.is_leaf_in_pos(pos)
    if minetest.get_item_group((minetest.get_node(pos)).name,"leaves")>0 then
         return true
    end 
    return false
end
-------------------------------------------------
--is sapling in pos
function owl_tech.is_sapling(pos)
    if minetest.get_item_group((minetest.get_node(pos)).name,"sapling")>0 then
        return true
   end 
   return false
end
----------------------------------------------
--can plant plant in pos
function owl_tech.can_plant_plant(pos)
    if (minetest.get_node(pos)).name== "mcl_farming:soil_wet"
    and minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z}).name =="air"then
        return true
    end
    return false
end 
----------------------------------------------
--can plant plant in pos
function owl_tech.can_plant_sapling(pos)
    if minetest.get_item_group(minetest.get_node(pos).name,"soil_sapling")>1
    and minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z}).name =="air" then
        return true
    end
    return false
end 
-----------------------------------------------
--prepear for plant sampling
function owl_tech.prepear_for_sapling_plant(pos,distance)
    local meta= minetest.get_meta(pos)
    meta:set_int("distance",distance)
end
-----------------------------------------------
--looking for new sapling
function owl_tech.look_for_place_new_sapling(pos,meta)
    local meta= minetest.get_meta(pos)
    local inv = meta:get_inventory()
    local fluid_its = inv:get_stack('it_sapling', 1)
    for i = (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--x
        for j = (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--z
            if owl_tech.can_plant_sapling({x=pos.x+i,y=pos.y,z=pos.z+j}) and fluid_its:get_count()>0 
            and not fluid_its:is_empty()  then
                minetest.set_node({x=pos.x+i,y=pos.y+1,z=pos.z+j}, {name=fluid_its:get_name()})
                local inv=meta:get_inventory() 
                local fluid_its = inv:get_stack('it_sapling', 1)
                fluid_its:set_count(fluid_its:get_count()-1)
                inv:set_stack('it_sapling', 1, fluid_its)
                local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
                owl_tech:set_charge(meta,steam_new)
                return --  plant 1 plant for each( if plant vore - hase bug1)
            end
        end
    end        
end
------------------------------------------------
--get wood or leavs in slot 
function owl_tech.get_wood_or_leaves(pos,inv,wood_its,leaves_its,meta)
    if owl_tech.is_wood_in_pos(pos)
    and (minetest.get_node(pos).name== wood_its:get_name() or wood_its:is_empty())
    and wood_its:get_count()<64 then
        local cut_wood = minetest.get_node(pos).name
        if not wood_its:is_empty() then-- Add  main output 
            wood_its:set_count(wood_its:get_count()+1) 
            inv:set_stack('wood', 1, wood_its)
        else
            local item ={name=cut_wood, count=1, wear=0, metadata=""}
            wood_its:add_item(item)
            inv:set_stack('wood', 1, wood_its)
        end
        minetest.remove_node(pos)
        local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
        owl_tech:set_charge(meta,steam_new)
    end
    if owl_tech.is_leaf_in_pos(pos) 
    and( minetest.get_node(pos).name== leaves_its:get_name() or leaves_its:is_empty())
    and leaves_its:get_count()<64 then
        local cut_leaf = minetest.get_node(pos).name
        if not leaves_its:is_empty() then-- Add  main output 
            leaves_its:set_count(leaves_its:get_count()+1) 
            inv:set_stack('leaves', 1, leaves_its)
        else
            local item ={name=cut_leaf, count=1, wear=0, metadata=""}
            leaves_its:add_item(item)
            inv:set_stack('leaves', 1, leaves_its)
        end
        minetest.remove_node(pos)
        local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
        owl_tech:set_charge(meta,steam_new)
    end
end
------------------------------------------------
--looking fop trees 
function owl_tech:look_for_trees(pos,meta)
    local meta =minetest.get_meta(pos)
    local inv = meta:get_inventory()
    local wood_its = inv:get_stack('wood', 1)
    local leaves_its = inv:get_stack('leaves', 1)
    local rotation = minetest.get_node(pos).param2 % 32 --get rotattion ( return int )
    if wood_its:is_empty()or (wood_its:get_count()<64) and leaves_its:is_empty()or (leaves_its:get_count()<64) then
        if rotation==0 then --z-1
            for i = (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--x
                for j = 1,(meta:get_int("distance")*2)+1, 1 do--z
                    for k = 32,0, -1 do--y
                        owl_tech.get_wood_or_leaves({x=pos.x+i,y=pos.y+k,z=pos.z-j},inv,wood_its,leaves_its,meta)
                    end
                end
            end
        end
        if rotation==2 then --z+1
            for i = (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--x
                for j = 1,(meta:get_int("distance")*2)+1, 1 do--z
                    for k =32,0, -1 do--y
                        owl_tech.get_wood_or_leaves({x=pos.x+i,y=pos.y+k,z=pos.z+j},inv,wood_its,leaves_its,meta)
                    end
                end
            end
        end
        if rotation==3 then --x-1
            for i = 1,(meta:get_int("distance")*2)+1, 1 do--x
                for j =  (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--z
                    for k = 32 ,0, -1 do--y
                        owl_tech.get_wood_or_leaves({x=pos.x+i,y=pos.y+k,z=pos.z+j},inv,wood_its,leaves_its,meta)
                    end
                end
            end
        end
        if rotation==1 then --x+1
            for i = 1,(meta:get_int("distance")*2)+1, 1 do--x
                for j =  (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--z
                    for k = 32,0,-1 do--y
                        owl_tech.get_wood_or_leaves({x=pos.x+i,y=pos.y+k,z=pos.z+j},inv,wood_its,leaves_its,meta)
                    end
                end
            end
        end
    end
end
-------------------------------------------------------------
--Look for pos to plant
function owl_tech:look_for_place_new_sids(pos,meta)
    local meta= minetest.get_meta(pos)
    local inv = meta:get_inventory()
    local fluid_its = inv:get_stack('it_sapling', 1) --
    local name_seed =string.gsub(fluid_its:get_name(),"owl_tech:","")
    name_seed = string.gsub(name_seed,"mcl_farming:","")
    for i = (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--x
        for j = (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--z
            if owl_tech.can_plant_plant({x=pos.x+i,y=pos.y,z=pos.z+j}) and fluid_its:get_count()>0  then
                minetest.set_node({x=pos.x+i,y=pos.y+1,z=pos.z+j}, {name=CROPS_VALID[name_seed][1]})
                local inv=meta:get_inventory() 
                local fluid_its = inv:get_stack('it_sapling', 1)
                fluid_its:set_count(fluid_its:get_count()-1)
                inv:set_stack('it_sapling', 1, fluid_its)
                local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
                owl_tech:set_charge(meta,steam_new)
                return --  plant 1 plant for each( if plant vore - hase bug1)
            end
        end
    end 
end
-----------------------------------------------------------------
--Get redy plant in pos
function owl_tech:get_ready_plant_in_pos(pos,inv,meta)--ready_to_harvest
    local wood_its = inv:get_stack('wood', 1)
    local leaves_its = inv:get_stack('leaves', 1)
    local vanila_plants= {"wheat","beetroot","potato"}
    local crop_node=minetest.get_node(pos)
    for i = 1, #vanila_plants, 1 do
        if minetest.get_node(pos).name ==vanila_plants[i] then
            local cut_wood = owl_tech.get_node_drop(crop_node.name,"toolname")
            if ( cut_wood== wood_its:get_name() or wood_its:is_empty()) 
            and wood_its:get_count()<64   then
                if not wood_its:is_empty() then-- Add  main output 
                    wood_its:set_count(wood_its:get_count()+1) 
                    inv:set_stack('wood', 1, wood_its)
                else
                    local item ={name=cut_wood, count=1, wear=0, metadata=""}
                    wood_its:add_item(item)
                    inv:set_stack('wood', 1, wood_its)
                end
                local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
                owl_tech:set_charge(meta,steam_new)
                minetest.remove_node(pos)
            end
            if minetest.get_node(pos).name== "wheat"then
                if ("mcl_farming:wheat_item"== leaves_its:get_name() or leaves_its:is_empty())
                and leaves_its:get_count()+1<=64 then
                    if not leaves_its:is_empty() then-- Add  main output 
                        leaves_its:set_count(leaves_its:get_count()+1) 
                        inv:set_stack('leaves', 1, leaves_its)
                    else
                        local item ={name="mcl_farming:wheat_item", count=1, wear=0, metadata=""}
                        leaves_its:add_item(item)
                        inv:set_stack('leaves', 1, leaves_its)
                    end
                    local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
					owl_tech:set_charge(meta,steam_new)
                    minetest.remove_node(pos)
                end
            end
            if minetest.get_node(pos).name== "beetroot" then
                if ("mcl_farming:beetroot_item"== leaves_its:get_name() or leaves_its:is_empty())
                and leaves_its:get_count()+1<=64 then
                    if not leaves_its:is_empty() then-- Add  main output 
                        leaves_its:set_count(leaves_its:get_count()+1) 
                        inv:set_stack('leaves', 1, leaves_its)
                    else
                        local item ={name="mcl_farming:beetroot_item", count=1, wear=0, metadata=""}
                        leaves_its:add_item(item)
                        inv:set_stack('leaves', 1, leaves_its)
                    end
                    local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
					owl_tech:set_charge(meta,steam_new)
                    minetest.remove_node(pos)
                end
            end
            if minetest.get_node(pos).name== "potato" then
                if ("mcl_farming:potato_item"== leaves_its:get_name() or leaves_its:is_empty())
                and leaves_its:get_count()+1<=64 then
                    if not leaves_its:is_empty() then-- Add  main output 
                        leaves_its:set_count(leaves_its:get_count()+1) 
                        inv:set_stack('leaves', 1, leaves_its)
                    else
                        local item ={name="mcl_farming:potato_item", count=1, wear=0, metadata=""}
                        leaves_its:add_item(item)
                        inv:set_stack('leaves', 1, leaves_its)
                    end
                    minetest.remove_node(pos)
                    local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
					owl_tech:set_charge(meta,steam_new)
                end
            end
            minetest.remove_node(pos)

            


        end  
    end
    if minetest.get_item_group((minetest.get_node(pos)).name,"ready_to_harvest")>0 then
        local cut_wood = owl_tech.get_node_drop(crop_node.name,"toolname")
        if ( cut_wood== wood_its:get_name() or wood_its:is_empty()) 
        and wood_its:get_count()<64   then
            if not wood_its:is_empty() then-- Add  main output 
                wood_its:set_count(wood_its:get_count()+1) 
                inv:set_stack('wood', 1, wood_its)
            else
                local item ={name=cut_wood, count=1, wear=0, metadata=""}
                wood_its:add_item(item)
                inv:set_stack('wood', 1, wood_its)
            end
        end
        local loot= ""
        local count = 0
        for i = 1, #LIST_ALL_CROPS, 1 do
            if "owl_tech:plant_"..LIST_ALL_CROPS[i][1]..LIST_ALL_CROPS[i][4]==crop_node.name then
                loot =  LIST_ALL_CROPS[i][6]
                count = LIST_ALL_CROPS[i][7]
            end
        end
        if ( loot== leaves_its:get_name() or leaves_its:is_empty())
        and leaves_its:get_count()+count<=64 then
            if not leaves_its:is_empty() then-- Add  main output 
                leaves_its:set_count(leaves_its:get_count()+count) 
                inv:set_stack('leaves', 1, leaves_its)
            else
                local item ={name=loot, count=count, wear=0, metadata=""}
                leaves_its:add_item(item)
                inv:set_stack('leaves', 1, leaves_its)
            end
            minetest.remove_node(pos)
            local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
            owl_tech:set_charge(meta,steam_new)
        end
        minetest.remove_node(pos)
    end
end
------------------------------------------------------------------
--look for plants
function owl_tech:look_for_plants(pos,meta)
    local meta =minetest.get_meta(pos)
    local inv = meta:get_inventory()
    local wood_its = inv:get_stack('wood', 1)
    local leaves_its = inv:get_stack('leaves', 1)
    local rotation = minetest.get_node(pos).param2 % 32 --get rotattion ( return int )
    if wood_its:is_empty()or (wood_its:get_count()<64) and leaves_its:is_empty()or (leaves_its:get_count()<64) then
        if rotation==0 then --z-1
            for i = (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--x
                for j = 1,(meta:get_int("distance")*2)+1, 1 do--z
                    owl_tech:get_ready_plant_in_pos({x=pos.x+i,y=pos.y,z=pos.z-j},inv,meta)
                end
            end
        end
        if rotation==2 then --z+1
            for i = (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--x
                for j = 1,(meta:get_int("distance")*2)+1, 1 do--z
                    owl_tech:get_ready_plant_in_pos({x=pos.x+i,y=pos.y,z=pos.z+j},inv,meta)
                end
            end
        end
        if rotation==3 then --x-1
            for i = 1,(meta:get_int("distance")*2)+1, 1 do--x
                for j =  (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--z
                    owl_tech:get_ready_plant_in_pos({x=pos.x+i,y=pos.y,z=pos.z+j},inv,meta)
                end
            end
        end
        if rotation==1 then --x+1
            for i = 1,(meta:get_int("distance")*2)+1, 1 do--x
                for j =  (meta:get_int("distance")*-1), meta:get_int("distance"), 1 do--z
                    owl_tech:get_ready_plant_in_pos({x=pos.x-i,y=pos.y,z=pos.z-j},inv,meta)
                end
            end
        end
    end
end