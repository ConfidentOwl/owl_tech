local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)
local energy_hatch_table ={
    {"base","Base",32,64000,"#575757","owl_tech:copper_electro_wire","steel"}
}
for i, value in ipairs(energy_hatch_table) do

	local function set_formspect_battery(meta)
		local charge_curent = owl_tech:get_charge(meta)
		local charge = owl_tech:get_charge_max(meta)
		local formspec = "size[9,8.75]"..
		"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
		"list[current_player;main;0,4.5;9,3;9]"..
		mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
		"list[current_player;main;0,7.74;9,1;]"..
		mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
		"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Battery"))).."]"..
		"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", ("Charge --"..charge_curent.."/"..charge))).."]"..
		"list[context;input_in;2.5,1.5;1,1;]"..
		mcl_formspec.get_itemslot_bg(2.5,1.5,1,1)..
		"list[context;dst;3.5,1.5;1,1;]"..
		mcl_formspec.get_itemslot_bg(3.5,1.5,1,1)..
		"list[context;input_in_add;2.5,3.5;1,1;]"..
		mcl_formspec.get_itemslot_bg(2.5,3.5,1,1)..
		"list[context;dst_add;3.5,3.5;1,1;]"..
		mcl_formspec.get_itemslot_bg(3.5,3.5,1,1)..
		"listring[context;dst]"..
		"listring[current_player;main]"..
		"listring[current_player;main]"..
		"listring[context;input_in]"..
		"listring[context;input_in_add]"..
		"listring[context;dst_add]"..
		"listring[current_player;main]"
		meta:set_string("formspec", formspec)
	end

	minetest.register_node("owl_tech:"..energy_hatch_table[i][1].."_battery", {
		description = S(energy_hatch_table[i][2].." baattery"),
		_doc_items_longdesc = S("Contains electricity tire"..i), -- 
		tiles = {
			"(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_steam_input.png",
			"(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_steam_output.png",
			"(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
			"(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
			"(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
			"(owl_tech_base_meshanism_side.png^[colorize:"..energy_hatch_table[i][5]..":128)^owl_tech_base_battary_side.png",
		},
		is_ground_content = false,
		stack_max = 64,
		groups = {pickaxey=2, owl_tech_electro_battery=1},
		sounds = mcl_sounds.node_sound_metal_defaults(),
		paramtype2 = "facedir",
		_mcl_blast_resistance = 6,
		_mcl_hardness = 5,
		on_construct = function(pos)
			local meta = minetest.get_meta(pos)
			owl_tech.set_mashine_tire(meta,i+1) 
			owl_tech:add_electro(pos,energy_hatch_table[i][3],energy_hatch_table[i][4])
			local inv =  meta:get_inventory()
			inv:set_size("input_in", 1) --for input
			inv:set_size("dst", 1) --for input 
			inv:set_size("input_in_add", 1) --for output
			inv:set_size("dst_add", 1) --for output
			local timer =minetest.get_node_timer(pos)
			meta:set_string("infotext",owl_tech:get_charge(meta).."/"..owl_tech:get_charge_max(meta))
			set_formspect_battery(meta)
			timer:start(0.2)
		end,
		on_timer = function(pos, elapsed)
			local timer = minetest.get_node_timer(pos)
			local meta = minetest.get_meta(pos)

			local inv =  meta:get_inventory()
			local cell_input_1 = inv:get_stack("input_in", 1)
			local cell_output_1 = inv:get_stack("dst", 1)
			local cell_input_2 = inv:get_stack("input_in_add", 1)
			local cell_output_2 = inv:get_stack("dst_add", 1)
			
			if not cell_input_1:is_empty() and minetest.get_item_group(cell_input_1:get_name(), "electro_tool")   and owl_tech:get_charge(meta)>=owl_tech:get_charge_max(meta) then
				minetest.chat_send_all(cell_input_1:get_wear())
			end
			
			if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
			and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
				local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
				if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
					owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
				end
			end
			if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"owl_tech_electro_wire")>0
			and owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)>=0 then --Send to wire electro
				local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
				if owl_tech:get_charge(meta_up)+owl_tech:get_voltage(meta_up)<=owl_tech:get_charge_max(meta_up) and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
					owl_tech.send_electro_from_wire_in_pos(meta,{x=pos.x,y=pos.y-1,z=pos.z})
				end
			end
			meta:set_string("infotext",owl_tech:get_charge(meta).."/"..owl_tech:get_charge_max(meta))
			set_formspect_battery(meta)
			timer:start(0.2)
		end
	})
	--Crafte iron fluid pipe
	minetest.register_craft({
		type = "shaped",
		output = "owl_tech:"..energy_hatch_table[i][1].."_battery",
		recipe = {
			{"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"},
			{energy_hatch_table[i][6],"owl_tech:"..energy_hatch_table[i][7].."_frames",energy_hatch_table[i][6]},
			{"owl_tech:"..energy_hatch_table[i][7].."_plate","owl_tech:batteri_tire_"..i,"owl_tech:"..energy_hatch_table[i][7].."_plate"}
		}
	})
end