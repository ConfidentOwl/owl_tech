--Setter and getter max_heat (USE UNT!)
function owl_tech.get_max_heat(meta)
    return meta:get_float("max_heat")
 end
 
function owl_tech.set_max_heat(meta,new_amount)
    meta:set_float("max_heat",new_amount)
    return owl_tech.get_max_heat(meta)
end
 ------------------------------
 --Setter and getter curent_heat (USE UNT!)
function owl_tech.get_curent_heat(meta)
    return meta:get_float("curent_heat")
 end
 
function owl_tech.set_curent_heat(meta,new_amount)
    meta:set_float("curent_heat",new_amount)
    return owl_tech.get_curent_heat(meta)
end
 ------------------------------
  --Setter and getter speed_heat(USE UNT!)
function owl_tech.get_speed_heat(meta)
    return meta:get_float("speed_heat")
 end
 
function owl_tech.set_speed_heat(meta,new_amount)
    meta:set_float("speed_heat",new_amount)
    return owl_tech.get_speed_heat(meta)
end
 ------------------------------
  --Setter and getter fuel_time(USE UNT!)
function owl_tech.get_fuel_time(meta)
    return meta:get_float("fuel_time")
end
 
function owl_tech.set_fuel_time(meta,new_amount)
    meta:set_float("fuel_time",new_amount)
    return owl_tech.get_fuel_time(meta)
end
------------------------------
--Setter and getter fuel consumption(USE UNT!)
function owl_tech.get_fuel_consumption(meta)
    return meta:get_float("fuel_consumption")
end
 
function owl_tech.set_fuel_consumption(meta,new_amount)
    meta:set_float("fuel_consumption",new_amount)
    return owl_tech.get_fuel_consumption(meta)
end
 ------------------------------
 --Water in steam in 1 time
function owl_tech.get_water_in_steam(meta)
    return meta:get_float("water_in_steam")
end
 
function owl_tech.set_water_in_steam(meta,new_amount)
    meta:set_float("water_in_steam",new_amount)
    return owl_tech.get_water_in_steam(meta)
end
 ------------------------------
--Prepea mashine for make steam  --call ONLY in on_construct = function(pos)
function owl_tech.prepear_for_make_steam(meta,max_heat,heat_speed,fuel_consumption,water_in_steam)
    owl_tech.set_max_heat(meta,max_heat)
    owl_tech.set_curent_heat(meta,0)
    owl_tech.set_speed_heat(meta,heat_speed)
    owl_tech.set_fuel_time(meta,0)
    owl_tech.set_fuel_consumption(meta,fuel_consumption)
    owl_tech.set_water_in_steam(meta,water_in_steam)
end
----------------------------------------------
--load fuel from slot 
function owl_tech.load_fuel_in_boiler(meta,inv,slot_name,out_fuel,fuel_its)
    local fuel_time=out_fuel.time
    local bon=  math.modf(owl_tech.get_curent_heat(meta)/5)
    owl_tech.set_fuel_time(meta,(bon+fuel_time)*10)
    fuel_its:set_count(fuel_its:get_count()-1)
    inv:set_stack(slot_name, 1, fuel_its)
end
----------------------------------------------
--Fuel burn
function owl_tech.fuel_burning(meta)
    owl_tech.set_fuel_time(meta,owl_tech.get_fuel_time(meta)-owl_tech.get_fuel_consumption(meta))--remove fuel 
    if owl_tech.get_curent_heat(meta)<owl_tech.get_max_heat(meta) then
        owl_tech.set_curent_heat(meta,owl_tech.get_curent_heat(meta)+owl_tech.get_speed_heat(meta))--++heat
    end
end
---------------------------------------------
--Gen steam
function owl_tech.gen_steam_in_node_pull(meta,number_of_pull_water,number_of_pull_steam,water_for_oper)
    owl_tech.set_pull_volume(meta,number_of_pull_water,owl_tech.get_pull_volume(meta,number_of_pull_water)-water_for_oper)--set water new amount
    owl_tech.set_pull_volume(meta,number_of_pull_steam,owl_tech.get_pull_volume(meta,number_of_pull_steam)+(water_for_oper*5))--set steam new amount(x2)
end
---------------------------------------------
--colling 
function owl_tech.cooling_boiler(meta)
    owl_tech.set_curent_heat(meta,owl_tech.get_curent_heat(meta)-(owl_tech.get_speed_heat(meta)/4))
    if owl_tech.get_curent_heat(meta) <0 then
        owl_tech.set_curent_heat(meta,0)
    end
end
---------------------------------------------
