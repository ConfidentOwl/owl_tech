local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

local function set_formspect_base_boiler(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_2_name = owl_tech.get_pull_fluid_name(meta,2)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)	
	local fluid_2_volume = owl_tech.get_pull_volume(meta,2)
	local heat = owl_tech.get_curent_heat(meta)
	local max_heat = owl_tech.get_max_heat(meta)
	local fuel_burn =owl_tech.get_fuel_time(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Boiler"))).."]"..
	"list[context;fluid_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;fuel;2.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,1.5,1,1)..
	"list[context;dst;5.75,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.75,1.5,1,1)..
	"label[0,0.5;"..minetest.formspec_escape(minetest.colorize("#313131", ("Heat-"..heat.."--"..max_heat))).."]"..
	"label[0,1;"..minetest.formspec_escape(minetest.colorize("#313131", ("Fuel brun-"..fuel_burn))).."]"..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_2_name.."--"..fluid_2_volume))).."]"..
	"listring[context;dst]"..
	"listring[current_player;main]"..
	"listring[context;fluid_in]"..
	"listring[current_player;main]"..
	"listring[context;fuel]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end


minetest.register_node("owl_tech:bronze_boiler", {
	description = S("Bronze boiler"),
	_doc_items_longdesc = S("First wey generate steam"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_steam_output.png",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_boiler_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_generator=1 ,fluid_in=1,fuel=1,dst=1 },
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,8000,1,0) --water 
		owl_tech.set_pull_fluid_whitlist(meta,1,"mcl_core:water_source") --whitlist for make input ONLY Water
		owl_tech.add_new_pull(meta,8000,0,1) --steam 
		owl_tech.set_pull_fluid_whitlist(meta,2,"owl_tech_steam")--whitlist for make and output ONLY Steam
		set_formspect_base_boiler(meta)
		owl_tech.prepear_for_make_steam(meta,256,0.5,1,0.25) --prepea al meta info for work like boiler
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("fluid_in", 1)
		inv:set_size("fuel", 1)
		inv:set_size("dst", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local fluid_its = inv:get_stack('fluid_in', 1)
		local fuel_its = inv:get_stack('fuel', 1)
		local dst_its = inv:get_stack('dst', 1)
		if not fluid_its:is_empty() then --Load fluid from buckets
			owl_tech.add_fluid_in_pull_from_itemslot(meta,fluid_its,inv,dst_its,'fluid_in','dst')
		end
		if owl_tech.get_fuel_time(meta)>0 then --burn process
			owl_tech.fuel_burning(meta)
		end
		if owl_tech.get_curent_heat(meta)>0 and owl_tech.get_fuel_time(meta)==0 then --coling boiler if not burn
			owl_tech.cooling_boiler(meta)
		end
		if owl_tech.get_curent_heat(meta)>100 
			and owl_tech.get_pull_volume(meta,2)+owl_tech.get_water_in_steam(meta)*5<=owl_tech.get_pull_max_volume(meta,2)
			and owl_tech.get_pull_volume(meta,1)-owl_tech.get_water_in_steam(meta)>=0 then --make steam
			owl_tech.gen_steam_in_node_pull(meta,1,2,owl_tech.get_water_in_steam(meta))
			if owl_tech.get_pull_fluid_name(meta,2)~="owl_tech_steam" then
				owl_tech.set_pull_fluid_name(meta,2,"owl_tech_steam")
			end
		end
		if not fuel_its:is_empty() and owl_tech.get_fuel_time(meta)==0 then --Load fluid from buckets
			local out_fuel= minetest.get_craft_result({method = "fuel", width = 1, items = {fuel_its}})
			if out_fuel.time>0 then
				owl_tech.load_fuel_in_boiler(meta,inv,'fuel',out_fuel,fuel_its)
			end
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --sand in pipe
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta_up,"owl_tech_steam",remove_amount,inde_pull)
				local pull_curent_volume = owl_tech.get_pull_volume(meta,2)
				local difer = pull_curent_volume-remove_amount
				owl_tech.set_pull_volume(meta,2,difer)
			end
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"fluid_pipe") 
		and  owl_tech.get_pull_fluid_name(minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z}),1)=="mcl_core:water_source" then --get from pipe water
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,1)
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_base_boiler(meta)
		timer:start(0.2)
	end
})
--Crafte bronze boiler
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:bronze_boiler",
    recipe = {
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","mcl_furnaces:furnace","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","mcl_core:brick_block","owl_tech:bronze_plate"}
    }
})
--Steal boiler
minetest.register_node("owl_tech:steel_boiler", {
	description = S("Steel boiler"),
	_doc_items_longdesc = S("Single node to get generate steam"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_steam_output.png",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_boiler_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_generator=1 ,fluid_in=1,fuel=1,dst=1 },
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,12000,1,0) --water 
		owl_tech.set_pull_fluid_whitlist(meta,1,"mcl_core:water_source") --whitlist for make input ONLY Water
		owl_tech.add_new_pull(meta,12000,0,1) --steam 
		owl_tech.set_pull_fluid_whitlist(meta,2,"owl_tech_steam")--whitlist for make and output ONLY Steam
		set_formspect_base_boiler(meta)
		owl_tech.prepear_for_make_steam(meta,384,0.75,1.25,0.5) --prepea al meta info for work like boiler
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("fluid_in", 1)
		inv:set_size("fuel", 1)
		inv:set_size("dst", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local fluid_its = inv:get_stack('fluid_in', 1)
		local fuel_its = inv:get_stack('fuel', 1)
		local dst_its = inv:get_stack('dst', 1)
		if not fluid_its:is_empty() then --Load fluid from buckets
			owl_tech.add_fluid_in_pull_from_itemslot(meta,fluid_its,inv,dst_its,'fluid_in','dst')
		end
		if owl_tech.get_fuel_time(meta)>0 then --burn process
			owl_tech.fuel_burning(meta)
		end
		if owl_tech.get_curent_heat(meta)>0 and owl_tech.get_fuel_time(meta)==0 then --coling boiler if not burn
			owl_tech.cooling_boiler(meta)
		end
		if owl_tech.get_curent_heat(meta)>100 
			and owl_tech.get_pull_volume(meta,2)+owl_tech.get_water_in_steam(meta)*5<=owl_tech.get_pull_max_volume(meta,2)
			and owl_tech.get_pull_volume(meta,1)-owl_tech.get_water_in_steam(meta)>=0 then --make steam
			owl_tech.gen_steam_in_node_pull(meta,1,2,owl_tech.get_water_in_steam(meta))
			if owl_tech.get_pull_fluid_name(meta,2)~="owl_tech_steam" then
				owl_tech.set_pull_fluid_name(meta,2,"owl_tech_steam")
			end
		end
		if not fuel_its:is_empty() and owl_tech.get_fuel_time(meta)==0 then --Load fluid from buckets
			local out_fuel= minetest.get_craft_result({method = "fuel", width = 1, items = {fuel_its}})
			if out_fuel.time>0 then
				owl_tech.load_fuel_in_boiler(meta,inv,'fuel',out_fuel,fuel_its)
			end
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --sand in pipe
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta_up,"owl_tech_steam",remove_amount,inde_pull)
				local pull_curent_volume = owl_tech.get_pull_volume(meta,2)
				local difer = pull_curent_volume-remove_amount
				owl_tech.set_pull_volume(meta,2,difer)
			end
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"fluid_pipe") 
		and  owl_tech.get_pull_fluid_name(minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z}),1)=="mcl_core:water_source" then --get from pipe water
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,1)
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 	then
				owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_base_boiler(meta)
		timer:start(0.2)
	end
})
--Crafte bronze boiler
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:steal_boiler",
    recipe = {
        {"owl_tech:steal_plate","owl_tech:steal_plate","owl_tech:steal_plate"},
        {"owl_tech:steal_plate","mcl_furnaces:furnace","owl_tech:steal_plate"},
        {"owl_tech:steal_plate","mcl_core:brick_block","owl_tech:steal_plate"}
    }
})