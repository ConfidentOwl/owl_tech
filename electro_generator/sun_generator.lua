local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)
--[[
	1)name 2)Name 3)material mame 4)add_coponent ,5)circuit 6)fluid input 7)energy modif 8)Voltage 9)max-electro 

]]
local small_turbins_conf={
	{"base_sun_generator","Base sun generator","steel","owl_tech:gold_stick","owl_tech:circuit_tire_1",2,32,16000},
}
for i = 1, #small_turbins_conf, 1 do

	local function set_formspect_base_steam_turbine(meta)
		local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
		local fluid_1_volume = owl_tech.get_pull_volume(meta,1)	
		local max_charge = owl_tech:get_charge_max(meta) 
		local curent_charge = owl_tech:get_charge(meta)
		local voltage = owl_tech:get_voltage(meta)
		local formspec = "size[9,8.75]"..
		"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
		"list[current_player;main;0,4.5;9,3;9]"..
		mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
		"list[current_player;main;0,7.74;9,1;]"..
		mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
		"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S(small_turbins_conf[i][2]))).."]"..
		"label[0,2.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..voltage))).."]"..
		"list[context;battari_slot;2.5,2.5;1,1;]"..
		mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
		"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
		"listring[current_player;main]"..
		"listring[context;battari_slot]"..
		"listring[current_player;main]"..
		"listring[current_player;main]"
		meta:set_string("formspec", formspec)
	end


	minetest.register_node("owl_tech:"..small_turbins_conf[i][1].."sun_gen", {
		description = S(small_turbins_conf[i][2]),
		_doc_items_longdesc = S("Generate electro from sun"),-- owl_tech_base_electro_turbine.png
		tiles = {
			"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_base_solat_panel.png",
			"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
			"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
			"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
			"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
			"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		},
		is_ground_content = false,
		stack_max = 64,
		groups = {pickaxey=2,owl_tech_electro_gen=1 ,fluid_in=1,fuel=1,dst=1 ,owl_tech_generator=1},
		sounds = mcl_sounds.node_sound_metal_defaults(),
		paramtype2 = "facedir",
		_mcl_blast_resistance = 6,
		_mcl_hardness = 5,
		on_construct = function(pos)
			local meta = minetest.get_meta(pos)
			owl_tech:add_electro(pos,small_turbins_conf[i][7],small_turbins_conf[i][8])
			set_formspect_base_steam_turbine(meta)
			local timer =minetest.get_node_timer(pos)
			local inv = meta:get_inventory()
			inv:set_size("battari_slot", 1) ---Can charge items in slot WIP
			timer:start(0.2)
		end,
		on_timer = function(pos, elapsed)
			local timer = minetest.get_node_timer(pos)
			local meta = minetest.get_meta(pos)
			local inv = meta:get_inventory()
			inv:set_size("battari_slot", 1) 
            
            if minetest.get_node_light({x=pos.x,y=pos.y+1,z=pos.z}, minetest.get_timeofday())>8 and minetest.get_timeofday()>0.25 and minetest.get_timeofday()<0.75 then
                if owl_tech:get_charge_max(meta)>=owl_tech:get_charge(meta)+(owl_tech:get_voltage(meta)/16) then
                    owl_tech:set_charge(meta,owl_tech:get_charge(meta)+(owl_tech:get_voltage(meta)/16))
                end
            end

			if owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>owl_tech:get_voltage(meta)*small_turbins_conf[i][6] and owl_tech.get_pull_volume(meta,1)>=owl_tech:get_voltage(meta)  then
				owl_tech.set_pull_volume(meta,1, owl_tech.get_pull_volume(meta,1)-owl_tech:get_voltage(meta) )
				owl_tech:set_charge(meta,owl_tech:get_charge(meta)+owl_tech:get_voltage(meta)*small_turbins_conf[i][6])
			end

			if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"owl_tech_electro_wire")>0
			and owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)>=0 then --Send to wire electro
				local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
				if owl_tech:get_charge(meta_up)+owl_tech:get_voltage(meta_up)<=owl_tech:get_charge_max(meta_up) and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
					owl_tech.send_electro_from_wire_in_pos(meta,{x=pos.x,y=pos.y-1,z=pos.z})
				end
			end
			set_formspect_base_steam_turbine(meta)
			timer:start(0.2)
		end
	})
	--Crafte bronze boiler
	minetest.register_craft({
		type = "shaped",
		output = "owl_tech:"..small_turbins_conf[i][1].."sun_gen",
		recipe = {
			{"owl_tech:"..small_turbins_conf[i][3].."_plate",small_turbins_conf[i][4],"owl_tech:"..small_turbins_conf[i][3].."_plate"},
			{small_turbins_conf[i][4],"owl_tech:"..small_turbins_conf[i][3].."_frames",small_turbins_conf[i][4]},
			{"owl_tech:"..small_turbins_conf[i][3].."_plate",small_turbins_conf[i][5],"owl_tech:"..small_turbins_conf[i][3].."_plate"}
		}
	})
end