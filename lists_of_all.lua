--Global lists 
GLOBAL_FLUID_LIST ={
    "mcl_core:lava_source","mcl_core:water_source","mclx_core:river_water_source","owl_tech:tar_source","owl_tech:metan_source"
} 
INPUT_SLOT_NAME= {
    "fluid_in","fuel",'input_in',"input_in_add","sapling"
}
OUTPUT_SLOT_NAME={
    'dst_add','dst',"wood","leaves"
}
FLUID_NAME_TO_CELL={
    lava_source="owl_tech:lava_cell",
    water_source="owl_tech:water_cell",
    river_water_source="owl_tech:water_cell",
    tar_source="owl_tech:tar_cell",
    metan_source="owl_tech:metan_cell",
}
FLUID_CELL_TO_NAME={
    lava_cell="mcl_core:lava_source",
    water_cell="mcl_core:water_source",
    tar_cell ="owl_tech:tar_source",
    metan_cell="owl_tech:metan_source"
}

local ore_duration = 12
local ingot_duration = 24
local smel_metal_duration = 4
local biomassa_duration =4 
local mixer_durtation= 2
local fluid_separatopr = 2 

-- key-input_name ----1)input_amount 2)recip_tire 3)output_name 4)output_amount 5)duration(use for 100) 6)seconds_chanse(0-100) 7)output_name_second 8)output_amount_second 9)Input full name 
-- minus 10 key
MACERATO_RECIPS = {
    --ore 
    iron_ore= {(1),(1),("owl_tech:iron_dirt_dust"),(2),(ore_duration),(25),("owl_tech:tin_dirt_dust"),(1),("owl_tech:iron_ore")},
    tin_ore = {(1),(1),("owl_tech:tin_dirt_dust"),(2),(ore_duration),(25),("owl_tech:iron_dirt_dust"),(1),("owl_tech:tin_ore")},
    copper_ore = {(1),(1),("owl_tech:copper_dirt_dust"),(2),(ore_duration),(25),("owl_tech:gold_dirt_dust"),(1),("owl_tech:copper_ore")},
    gold_ore = {(1),(1),("owl_tech:gold_dirt_dust"),(2),(ore_duration),(25),("owl_tech:copper_dirt_dust"),(1),("owl_tech:gold_ore")},
    silver_ore = {(1),(1),("owl_tech:silver_dirt_dust"),(2),(ore_duration),(25),("owl_tech:lead_dirt_dust"),(1),("owl_tech:silver_ore")},
    lead_ore = {(1),(1),("owl_tech:lead_dirt_dust"),(2),(ore_duration),(25),("owl_tech:silver_dirt_dust"),(1),("owl_tech:lead_ore")},
    coal_ore = {(1),(1),("owl_tech:coal_dirt_dust"),(2),(ore_duration),(25),("owl_tech:coal_dirt_dust"),(1),("owl_tech:coal_ore")},
    saltpeter_ore = {(1),(1),("owl_tech:saltpeter_dirt_dust"),(2),(ore_duration),(25),("owl_tech:sulfur_dirt_dust"),(1),("owl_tech:saltpeter_ore")},
    sulfur_ore = {(1),(1),("owl_tech:sulfur_dirt_dust"),(2),(ore_duration),(25),("owl_tech:saltpeter_dirt_dust"),(1),("owl_tech:sulfur_ore")},
    diamond_ore = {(1),(1),("owl_tech:diamond_dirt_dust"),(2),(ore_duration),(25),("owl_tech:coal_dirt_dust"),(1),("owl_tech:diamond_ore")},
    --ingots  
    iron_ingot = {(1),(1),("owl_tech:iron_dust"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:iron_ingot")},
    tin_ingot = {(1),(1),("owl_tech:tin_dust"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:tin_ingot")},
    copper_ingot = {(1),(1),("owl_tech:copper_dust"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:copper_ingot")},
    gold_ingot = {(1),(1),("owl_tech:gold_dust"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:gold_ingot")},
    silver_ingot = {(1),(1),("owl_tech:silver_dust"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:silver_ingot")},
    lead_ingot = {(1),(1),("owl_tech:lead_dust"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:lead_ingot")},
    steel_ingot = {(1),(1),("owl_tech:steel_dust"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:steel_ingot")},
    bronze_ingot = {(1),(1),("owl_tech:bronze_dust"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:bronze_ingot")},
    --make biomassa
    acacialeaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("owl_tech:biomassa"),(1),("mcl_core:acacialeves")},
    birchleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("owl_tech:biomassa"),(1),("mcl_core:birchleaves")},
    darkleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("owl_tech:biomassa"),(1),("mcl_core:darkleaves")},
    jungleleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("owl_tech:biomassa"),(1),("mcl_core:jungleleaves")},
    leaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("owl_tech:biomassa"),(1),("mcl_core:leaves")},
    spruceleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("owl_tech:biomassa"),(1),("mcl_core:spruceleaves")},
    mangroveleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("owl_tech:biomassa"),(1),("mcl_mangrove:mangroveleaves")},
    --wood dust
    tree = {(1),(1),("owl_tech:wood_dust"),(1),(biomassa_duration),(25),("owl_tech:biomassa"),(0),("mcl_core:tree")},
    darktree = {(1),(1),("owl_tech:wood_dust"),(1),(biomassa_duration),(25),("owl_tech:biomassa"),(0),("mcl_core:darktree")},
    acaciatree = {(1),(1),("owl_tech:wood_dust"),(1),(biomassa_duration),(25),("owl_tech:biomassa"),(0),("mcl_core:acaciatree")},
    sprucetree = {(1),(1),("owl_tech:wood_dust"),(1),(biomassa_duration),(25),("owl_tech:biomassa"),(0),("mcl_core:sprucetree")},
    birchtree = {(1),(1),("owl_tech:wood_dust"),(1),(biomassa_duration),(25),("owl_tech:biomassa"),(0),("mcl_core:birchtree")},
    jungletree = {(1),(1),("owl_tech:wood_dust"),(1),(biomassa_duration),(25),("owl_tech:biomassa"),(0),("mcl_core:jungletree")},
}
FLUID_SEPARATOR={
    tar_source = {(100),(2),("owl_tech:metan_source"),(75),(fluid_separatopr),(0),("none"),(0),("mcl_core:tar_source")},

}
-- key-input_name ----1)1 input amount 2)2 input amount 3)output name 4)output amount 5)recipt tire 6)duration(use for 100) 7)name 1 input 8)name 2 input 
SMELTER_RECIPS={
    tin_ingotcopper_ingot={(1),(3),("owl_tech:bronze_ingot"),(4),(1),(smel_metal_duration),'owl_tech:tin_ingot','owl_tech:copper_ingot'},
    copper_ingottin_ingot={(3),(1),("owl_tech:bronze_ingot"),(4),(1),(smel_metal_duration),'owl_tech:copper_ingot','owl_tech:tin_ingot'},
    gold_ingotsilver_ingot={(1),(1),("owl_tech:electrum_ingot"),(1),(1),(smel_metal_duration),'owl_tech:gold_ingot','owl_tech:silver_ingot'},
    silver_ingotgold_ingot={(1),(1),("owl_tech:electrum_ingot"),(1),(1),(smel_metal_duration),'owl_tech:silver_ingot','owl_tech:gold_ingot'},
}
MIXER_RESIPS={
    tin_dustcopper_dust={(1),(3),("owl_tech:bronze_dust"),(4),(1),(mixer_durtation),'owl_tech:tin_dust','owl_tech:copper_dust'},
    copper_dusttin_dust={(3),(1),("owl_tech:bronze_dust"),(4),(1),(mixer_durtation),'owl_tech:copper_dust','owl_tech:tin_dust'},
    gold_dustsilver_dust={(1),(1),("owl_tech:electrum_dust"),(2),(1),(mixer_durtation),'owl_tech:gold_dust','owl_tech:silver_dust'},
    silver_dustgold_dust={(1),(1),("owl_tech:electrum_dust"),(2),(1),(mixer_durtation),'owl_tech:silver_dust','owl_tech:gold_dust'},
    wood_dustbiomassa={(1),(1),("owl_tech:wood_and_biomass"),(2),(1),(mixer_durtation),'owl_tech:wood_dust','owl_tech:biomassa'},
}
-- key-input_name ----1)input_amount 2)recip_tire 3)output_name 4)output_amount 5)duration(use for 100) 6)seconds_chanse(0-100) 7)output_name_second 8)output_amount_second 9)Input full name 
SIEV_RESIPS={
    iron_dirt_dust= {(1),(1),("owl_tech:iron_dust"),(2),(ore_duration),(25),("owl_tech:tin_dust"),(1),("owl_tech:iron_dirt_dust")},
    tin_dirt_dust= {(1),(1),("owl_tech:tin_dust"),(2),(ore_duration),(25),("owl_tech:iron_dust"),(1),("owl_tech:tin_dirt_dust")},
    copper_dirt_dust= {(1),(1),("owl_tech:copper_dust"),(2),(ore_duration),(25),("owl_tech:gold_dust"),(1),("owl_tech:copper_dirt_dust")},
    gold_dirt_dust= {(1),(1),("owl_tech:gold_dust"),(2),(ore_duration),(25),("owl_tech:copper_dust"),(1),("owl_tech:gold_dirt_dust")},
    silver_dirt_dust= {(1),(1),("owl_tech:silver_dust"),(2),(ore_duration),(25),("owl_tech:lead_dust"),(1),("owl_tech:silver_dirt_dust")},
    lead_dirt_dust= {(1),(1),("owl_tech:lead_dust"),(2),(ore_duration),(25),("owl_tech:silver_dust"),(1),("owl_tech:lead_dirt_dust")},
    coal_dirt_dust= {(1),(1),("owl_tech:coal_dust"),(2),(ore_duration),(25),("owl_tech:coal_dust"),(1),("owl_tech:coal_dirt_dust")},
    saltpeter_dirt_dust= {(1),(1),("owl_tech:saltpeter_dust"),(2),(ore_duration),(25),("owl_tech:sulfur_dust"),(1),("owl_tech:saltpeter_dirt_dust")},
    sulfur_dirt_dust= {(1),(1),("owl_tech:sulfur_dust"),(2),(ore_duration),(25),("owl_tech:saltpeter_dust"),(1),("owl_tech:sulfur_dirt_dust")},
    diamond_dirt_dust= {(1),(1),("owl_tech:diamond_dust"),(2),(ore_duration),(25),("owl_tech:diamond_dust"),(1),("owl_tech:diamond_dirt_dust")},
    --sapling
    acacialeaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("mcl_core:acaciasapling"),(1),("mcl_core:acacialeaves")},
    birchleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("mcl_core:birchsapling"),(1),("mcl_core:birchleaves")},
    darkleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("mcl_core:darksapling"),(1),("mcl_core:darkleaves")},
    jungleleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("mcl_core:junglesapling"),(1),("mcl_core:jungleleaves")},
    leaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("mcl_core:sapling"),(1),("mcl_core:leaves")},
    spruceleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("mcl_core:sprucesapling"),(1),("mcl_core:spruceleaves")},
    --mangroveleaves = {(1),(2),("owl_tech:biomassa"),(2),(biomassa_duration),(25),("mcl_core:"),(1),("mcl_mangrove:mangroveleaves")},    
}
-- key-input_name ----1)input_amount 2)recip_tire 3)output_name 4)output_amount 5)duration(use for 100) 6)seconds_chanse(0-100) 7)output_name_second 8)output_amount_second 9)Input full name 
--froge hammer resips 
FORGE_HAMMER_RECIPS={
    iron_ingot = {(1),(1),("owl_tech:iron_plate"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:iron_ingot")},
    tin_ingot = {(1),(1),("owl_tech:tin_plate"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:tin_ingot")},
    copper_ingot = {(1),(1),("owl_tech:copper_plate"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:copper_ingot")},
    gold_ingot = {(1),(1),("owl_tech:gold_plate"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:gold_ingot")},
    silver_ingot = {(1),(1),("owl_tech:silver_plate"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:silver_ingot")},
    lead_ingot = {(1),(1),("owl_tech:lead_plate"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:lead_ingot")},
    steel_ingot = {(1),(1),("owl_tech:steel_plate"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:steel_ingot")},
    bronze_ingot = {(1),(1),("owl_tech:bronze_plate"),(1),(ingot_duration),(0),("none"),(0),("owl_tech:bronze_ingot")},
}
--List of all crops
LIST_CROSS_CROPS={
    "owl_tech:ruber_wheat","mcl_farming:wheat","mcl_farming:pumpkintige_unconnct","mcl_farming:beetroot","mcl_farming:melotige_unconnct","mcl_farming:carrot","mcl_farming:potato_item","mcl_farming:wheat","owl_tech:steel_ball","owl_tech:silverr_char","owl_tech:tin_plague","owl_tech:diamond_king","owl_tech:iron_maiden"
    ,"owl_tech:copgrow","owl_tech:coaltus"
}
--List all crops breed  1)time ,2)name new crop 
CROPS_BREED={
    wheatwheat={100,"owl_tech:plant_ruber_wheat1"},--ruber
    beetrootbeetroot={100,"owl_tech:plant_copgrow1"},--copper
    carrotcarrot={100,"owl_tech:plant_coaltus1"},--coal
    potatopotato={100,"owl_tech:plant_ironmaiden1"},--iron
    plant_ironmaidenplant_coaltus={100,"owl_tech:plant_steel_ball1"},--steel
    plant_coaltusplant_ironmaiden={100,"owl_tech:plant_steel_ball1"},
    plant_coaltusplant_coaltus={100,"owl_tech:plant_diamond_king1"},--diamond
    plant_ironmaidenplant_copgrow={100,"owl_tech:plant_tin_plague1"},--tin
    plant_copgrowplant_ironmaiden={100,"owl_tech:plant_tin_plague1"},
    tin_plaguetin_plague={100,"owl_tech:plant_silverr_char1"}
}
CROPS_VALID={
    seeds_steel_ball={"owl_tech:plant_steel_ball1"},
    seeds_silverr_char={"owl_tech:plant_silverr_char1"},
    seeds_tin_plague={"owl_tech:plant_tin_plague1"},
    seeds_diamond_king={"owl_tech:plant_diamond_king1"},
    seeds_iron_maiden={"owl_tech:plant_ironmaiden1"},
    seeds_copgrow={"owl_tech:plant_copgrow1"},
    seeds_coaltus={"owl_tech:plant_coaltus1"},
    seeds_ruber_wheat={"owl_tech:plant_ruber_wheat1"},
    wheat_seeds={"mcl_farming:wheat_1"},
    beetroot_seeds={"mcl_farming:beetroot_1"},
    carrot={"mcl_farming:carrot_1"},
    potato_item={"mcl_farming:mcl_farming:potato_1"},
}

LIST_ALL_CROPS ={
    {"ruber_wheat","Ruber Wheat","owl_tech_ruber_wheat_",8,"owl_tech.seed_ruber_wheat.png","owl_tech:ruber_dust",2,50,"none"},--ruber
    {"coaltus","Coaltus","owl_tech_coaltus_",8,"owl_tech.seed_coaltus.png","owl_tech:coal_dust",1,50,"owl_tech:coal_block"},--coal
    {"copgrow","Copgrow","owl_tech_copgrow_",8,"owl_tech.seed_copgrow.png","owl_tech:copper_dust",1,50,"owl_tech:copper_block"},--copper
    {"iron_maiden","Iron Maiden","owl_tech_iron_maiden_",8,"owl_tech.seed_iron_maiden.png","owl_tech:iron_dust",1,50,"owl_tech:iron_block"},--gold
    {"diamond_king","Diamond king","owl_tech_diamond_king_",8,"owl_tech.seed_king_diamond.png","owl_tech:diamond_dust",1,50,"owl_tech:diamond_block"},--diamond
    {"tin_plague","Tin Plague","owl_tech_tin_plague_",8,"owl_tech.seed_tin_plague.png","owl_tech:tin_dust",1,50,"owl_tech:tin_block"},--tin
    {"silverr_char","Silver char","owl_tech_silver_char_",8,"owl_tech.seed_silver_char.png","owl_tech:silver_dust",1,50,"owl_tech:silver_block"},--silver
    {"steel_ball","Steel ball","owl_tech_steel_ball_",8,"owl_tech.seed_steel_ball.png","owl_tech:steel_dust",1,50,"owl_tech:steel_block"}
}