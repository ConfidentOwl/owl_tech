local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)


minetest.register_craftitem("owl_tech:stick_of_truef", {
	description = S("Stick of truef"),
	_doc_items_longdesc = S("get all info about node and envirment"),
	inventory_image = "owl_tech_stick_of_trufh.png",
	stack_max = 64,
	groups = { book=1, craftitem = 1  },
	on_use = function(itemstack, user, pointed_thing)
        local name = user:get_player_name()
        local pos = {}
        --local bioms_table = biomeinfo.get_active_v6_biomes()
        minetest.chat_send_player(name, "-------------------------------------------")
        if pointed_thing.type == "node" then
            pos = pointed_thing.under
            minetest.chat_send_player(name, "Node pos-"..(minetest.pos_to_string(pos))) 
            minetest.chat_send_player(name, "Node name-"..((minetest.get_node(pos)).name))
            minetest.chat_send_player(name, "Biom name -"..(biomeinfo.get_v6_biome(pos)))
            minetest.chat_send_player(name, "Biom humidity -"..(biomeinfo.get_v6_humidity(pos)))
            minetest.chat_send_player(name, "Biom heat -"..(biomeinfo.get_v6_heat(pos)))
            minetest.chat_send_player(name, "Time of day -"..(minetest.get_timeofday())) 
        elseif pointed_thing.type == "object" then
            pos = pointed_thing.ref:get_pos()
            minetest.chat_send_player(name, "Object pos-"..(minetest.pos_to_string(pos))) 
            minetest.chat_send_player(name, "Biom name -"..(biomeinfo.get_v6_biome(pos)))
            minetest.chat_send_player(name, "Biom humidity -"..(biomeinfo.get_v6_humidity(pos)))
            minetest.chat_send_player(name, "Biom heat -"..(biomeinfo.get_v6_heat(pos))) 
            minetest.chat_send_player(name, "Time of day -"..(minetest.get_timeofday())) 
        else 
            minetest.chat_send_player(name, "Dont use it in air -finde node or some object") 
        end
    end
})


