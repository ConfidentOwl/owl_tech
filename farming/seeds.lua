local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

local function spawn_crop_from_seed(pos,itemstack,crop_name)
    minetest.place_node(pos, crop_name)
    local item_amount =  itemstack:get_count()
    if item_amount==1 then
        itemstack:clear()
    end
end 

local sel_heights = { --take from mc2
	-5/16,
	-2/16,
	0,
	3/16,
	5/16,
	6/16,
	7/16,
}

local function get_dirt_under(pos)
    if minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z}).name =='mcl_core:soil_wet' then
        if minetest.get_node({x=pos.x,y=pos.y-2,z=pos.z}).name =='mcl_core:dirt'  then
            if minetest.get_node({x=pos.x,y=pos.y-3,z=pos.z}).name =='mcl_core:dirt'  then
                return 3
            else
                return 2
            end
        else
            return 1
        end
    else
        return 0
    end 
end

local function get_air_aboth(pos)
    local a = 0 
    for i = 1, 30, 1 do
        if minetest.get_node({x=pos.x,y=pos.y+i,z=pos.z}).name =='air' then
            a = i
        else 
            break
        end
    end
    return math.modf(a/10)
end

local function calculate_grow_cof(pos)
    local a = get_dirt_under(pos) --dirt under  
    local b = get_air_aboth(pos) -- air aboth
    local c = biomeinfo.get_v6_humidity(pos)*3 -- humidity (0-1)
    local d = biomeinfo.get_v6_heat(pos)*3 --heat (0-1)
    return a+b+c+d
end

--1)name 2)Name 3)texture base name 4)stages 5)seed texture 6)drop name 7)drop amount 8)cof of grow to up 9) Special node under dirt



for i = 1, #LIST_ALL_CROPS, 1 do

    local create, name, longdesc
	if i == 1 then
		create = true
		name = S("Premature "..LIST_ALL_CROPS[i][2] )
		longdesc = S(
			"Premature  plants grow on farmland under sunlight in 8 stages.On hydrated farmland, they grow faster. They can be harvested at any time but will only yield a profit when mature.")
	else
		create = false
	end


    minetest.register_craftitem("owl_tech:seeds_"..LIST_ALL_CROPS[i][1], {
        description = S(LIST_ALL_CROPS[i][2].." Seeds"),
        _tt_help = S("Grows on farmland"),
        _doc_items_longdesc = S("Grows into a wheat plant."),
        _doc_items_usagehelp = S(" Place the wheat seeds on farmland (which can be created with a hoe) to plant  plant."),
        groups = {craftitem = 1, compostability = 30},
        inventory_image = LIST_ALL_CROPS[i][5],
        on_place = function(itemstack, placer, pointed_thing)
            if pointed_thing.type =="node" then
                local node_chek = minetest.get_node(pointed_thing.under)
                if node_chek.name=="mcl_farming:soil" or node_chek.name=="mcl_farming:soil_wet" then 
                    if itemstack:get_count()>1 then
                        itemstack:set_count(itemstack:get_count()-1)
                    else
                        itemstack:clear()
                    end
                    minetest.set_node({x=pointed_thing.under.x,y=pointed_thing.under.y+1,z=pointed_thing.under.z},{name="owl_tech:plant_"..LIST_ALL_CROPS[i][1]..1})
                    return itemstack
                end
            end
        end
    })

    for j = 1, LIST_ALL_CROPS[i][4]-1, 1 do
        minetest.register_node("owl_tech:plant_"..LIST_ALL_CROPS[i][1]..j, {
            description = S("Premature "..LIST_ALL_CROPS[i][2]),
            _doc_items_create_entry = create,
            _doc_items_entry_name = name,
            _doc_items_longdesc = longdesc,
            paramtype = "light",
            paramtype2 = "meshoptions",
            place_param2 = 3,
            sunlight_propagates = true,
            walkable = false,
            drawtype = "plantlike",
            drop = "owl_tech:seeds_"..LIST_ALL_CROPS[i][1],
            tiles = {"owl_tech_ruber_wheat_"..j..".png"},
            inventory_image = "owl_tech_ruber_wheat_"..j..".png",
            wield_image =  "owl_tech_ruber_wheat_"..j..".png",
            selection_box = {
                type = "fixed",
                fixed = {
                    {-0.5, -0.5, -0.5, 0.5, sel_heights[i], 0.5}
                },
            },
            groups = {dig_immediate=3, not_in_creative_inventory=1, plant=1,attached_node=1,
                dig_by_water=1,destroy_by_lava_flow=1, dig_by_piston=1},
            sounds = mcl_sounds.node_sound_leaves_defaults(),
            _mcl_blast_resistance = 0,
            on_construct = function(pos)
                local meta= minetest.get_meta(pos)--calculate_grow_cof(pos)
                meta:set_int("curent_grow",0)
                meta:set_int("max_cof",LIST_ALL_CROPS[i][8])
                local timer =minetest.get_node_timer(pos)
                timer:start(10)
            end,
            on_timer = function(pos, elapsed)
                local meta = minetest.get_meta(pos)
                meta:set_int("curent_grow",meta:get_int("curent_grow")+calculate_grow_cof(pos))
                if meta:get_int("max_cof")<=meta:get_int("curent_grow") 
                and (LIST_ALL_CROPS[i][9]=="none" or LIST_ALL_CROPS[i][9] == minetest.get_node({x=pos.x,y=pos.y-4,z=pos.z}).name ) then
                    minetest.set_node(pos,{name="owl_tech:plant_"..LIST_ALL_CROPS[i][1]..j+1})
                end
                local timer =minetest.get_node_timer(pos)
                timer:start(10)
            end
        })
    end

    minetest.register_node("owl_tech:plant_"..LIST_ALL_CROPS[i][1]..LIST_ALL_CROPS[i][4], {
        description = S("Mature "..LIST_ALL_CROPS[i][2]),
        _doc_items_longdesc = S("Mature  plants are ready to be harvested for wheat and seeds."),
        sunlight_propagates = true,
        paramtype = "light",
        paramtype2 = "meshoptions",
        place_param2 = 3,
        walkable = false,
        drawtype = "plantlike",
        tiles = {LIST_ALL_CROPS[i][3].."8.png"},
        inventory_image = LIST_ALL_CROPS[i][3].."8.png",
        wield_image = LIST_ALL_CROPS[i][3].."8.png",
        drop = {
            max_items = 4,
            items = {
                { items = {"owl_tech:seeds_"..LIST_ALL_CROPS[i][1]} },
                { items = {"owl_tech:seeds_"..LIST_ALL_CROPS[i][1]}, rarity = 2},
                { items = {"owl_tech:seeds_"..LIST_ALL_CROPS[i][1]}, rarity = 5},
                { items = {LIST_ALL_CROPS[i][6].." "..LIST_ALL_CROPS[i][7]} },
                { items = {LIST_ALL_CROPS[i][6].." "..LIST_ALL_CROPS[i][7]+1} , rarity = 4}
            }
        },
        groups = {dig_immediate=3, not_in_creative_inventory=1, plant=1, attached_node=1,
            dig_by_water=1,destroy_by_lava_flow=1, dig_by_piston=1, ready_to_harvest=1},
        sounds = mcl_sounds.node_sound_leaves_defaults(),
        _mcl_blast_resistance = 0,
    })
end