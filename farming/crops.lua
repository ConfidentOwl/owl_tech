local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

local function set_crops_in_meta_name(meta,name)
    if meta:get_string("crop1")=='none' then
        meta:set_string("crop1",name)
        return
    end
    if meta:get_string("crop2")=='none' then 
        meta:set_string("crop2",name)
        return
    end
end

--Crops
minetest.register_node("owl_tech:crops",{
    description = "Crops",
    _tt_help = S("Crops for grow crops"),
    _doc_items_longdesc = S(""),
    _doc_items_usagehelp = S("Right-click the fcrops to put a seed and begin grow"),
    tiles = {"mcl_core_log_big_oak.png"},
    paramtype = "light",
    is_ground_content = false,
    stack_max = 64,
    sunlight_propagates = true,
    walkable = true,
    groups = {handy = 1, axey = 1, choppy = 1},
    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed ={{-0.45,-0.5,-0.45,-0.4,0.5,-0.4},
                {0.45,-0.5,-0.45,0.4,0.5,-0.4},
                {-0.45,-0.5,0.45,-0.4,0.5,0.4},
                {0.45,-0.5,0.45,0.4,0.5,0.4}
                },
    },
    sounds = mcl_sounds.node_sound_wool_defaults(),
    _mcl_hardness = 0.1,
    _mcl_blast_resistance = 0.1,
    on_construct = function(pos)
        local meta =  minetest.get_meta(pos)

        meta:set_string("infotext", "empty")
        meta:set_int("state",0)
        meta:set_int("max_redines",0)
        meta:set_int("redines",0)
        
        meta:set_string("pos-x", 'none')
        meta:set_string("pos+x", 'none')
        meta:set_string("pos-z", 'none')
        meta:set_string("pos+z", 'none')

        meta:set_string("crop1", 'none')
        meta:set_string("crop2", 'none')

        meta:set_string("grow_in",'')

        local timer =minetest.get_node_timer(pos)
        timer:start(0.2)
    end,
    on_timer = function (pos, elapsed)
        local meta =  minetest.get_meta(pos)
        local timer =minetest.get_node_timer(pos)
        if meta:get_int("state")==0 and minetest.get_node({x=pos.x ,y=pos.y-1,z=pos.z}).name== 'mcl_farming:soil_wet' then
            local get_two_seed_around = 0
            for j, value1 in pairs(LIST_CROSS_CROPS) do
                if minetest.get_node({x=pos.x-1 ,y=pos.y,z=pos.z}).name == LIST_CROSS_CROPS[j] and meta:get_string("pos-x")=="none" then
                    meta:set_string("pos-x",minetest.get_node({x=pos.x-1 ,y=pos.y,z=pos.z}).name)
                    get_two_seed_around = get_two_seed_around + 1
                end
                if minetest.get_node({x=pos.x+1 ,y=pos.y,z=pos.z}).name == LIST_CROSS_CROPS[j] and meta:get_string("pos+x")=="none"then
                    meta:set_string("pos+x",minetest.get_node({x=pos.x+1 ,y=pos.y,z=pos.z}).name)
                    get_two_seed_around = get_two_seed_around + 1
                end
                if minetest.get_node({x=pos.x ,y=pos.y,z=pos.z-1}).name == LIST_CROSS_CROPS[j] and meta:get_string("pos-z")=="none"then
                    meta:set_string("pos-z",minetest.get_node({x=pos.x ,y=pos.y,z=pos.z-1}).name)
                    get_two_seed_around = get_two_seed_around + 1
                end
                if minetest.get_node({x=pos.x ,y=pos.y,z=pos.z+1}).name == LIST_CROSS_CROPS[j] and meta:get_string("pos+z")=="none"then
                    meta:set_string("pos+z",minetest.get_node({x=pos.x ,y=pos.y,z=pos.z+1}).name)
                    get_two_seed_around = get_two_seed_around + 1
                end
            end
            if get_two_seed_around==2 then
                meta:set_int("state",1)
            end
        end
        if meta:get_int("state")==1 then --look for name of crops found
            if meta:get_string("pos-x")~='none' then
                set_crops_in_meta_name(meta,meta:get_string("pos-x"))
            end
            if meta:get_string("pos+x")~='none' then
                set_crops_in_meta_name(meta,meta:get_string("pos+x"))
            end
            if meta:get_string("pos-z")~='none' then
                set_crops_in_meta_name(meta,meta:get_string("pos-z"))
            end
            if meta:get_string("pos+z")~='none' then
                set_crops_in_meta_name(meta,meta:get_string("pos+z"))
            end
            if meta:get_string("crop1")~='none'and meta:get_string("crop2")~='none' then
                meta:set_int("state",2)
            end
        end
        if meta:get_int("state")==2 then --spawn new crop
            meta:set_string('crop1',string.gsub(meta:get_string("crop1"),"mcl_farming:",''))
            meta:set_string('crop2',string.gsub(meta:get_string("crop2"),"mcl_farming:",''))
            meta:set_string('crop1',string.gsub(meta:get_string("crop1"),"owl_tech:",''))
            meta:set_string('crop2',string.gsub(meta:get_string("crop2"),"owl_tech:",''))

            if CROPS_BREED[meta:get_string("crop1")..meta:get_string("crop2")] then
                meta:set_int("state",3)
                meta:set_string("grow_in",CROPS_BREED[meta:get_string("crop1")..meta:get_string("crop2")][2])
                meta:set_int("max_redines", CROPS_BREED[meta:get_string("crop1")..meta:get_string("crop2")][1])
            elseif CROPS_BREED[meta:get_string("crop2")..meta:get_string("crop1")] then
                meta:set_int("state",3)
                meta:set_string("grow_in",CROPS_BREED[meta:get_string("crop2")..meta:get_string("crop1")][2])
                meta:set_int("max_redines", CROPS_BREED[meta:get_string("crop2")..meta:get_string("crop1")][1])
            end
        end
        if meta:get_int("state")==3 then
            meta:set_string("infotext", "somthing_grow")
            if meta:get_int("redines")>=meta:get_int("max_redines") then
                meta:set_int("redines",meta:get_int("redines")+1)
            else
                minetest.set_node(pos, {name=meta:get_string("grow_in")})
            end
        end
        timer:start(0.2)
    end
})

minetest.register_craft({
    type = "shaped",
    output = "owl_tech:crops",
    recipe = {
        {"mcl_core:stick","","mcl_core:stick"},
        {"mcl_core:stick","","mcl_core:stick"},
        {"","",""},
    }
})