local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

dofile(path .. "/farming/crops.lua") --Farming
dofile(path .. "/farming/seeds.lua") --Farming