local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

function owl_tech.prepear_for_pump(meta,max_horizontal_distance)
    meta:set_int("check_y",-1)
    meta:set_int("check_x",max_horizontal_distance)
    meta:set_int("check_z",max_horizontal_distance)
    meta:set_int("max_horizontal_distance",max_horizontal_distance)
end 

function owl_tech.get_max_horizontal_distance(meta)
	return meta:get_int("max_horizontal_distance")
end

function owl_tech.get_y_pump(meta)
    return meta:get_int("check_y")
end

function owl_tech.get_x_pump(meta)
    return meta:get_int("check_x")
end

function owl_tech.get_z_pump(meta)
    return meta:get_int("check_z")
end

function owl_tech.add_y_pump(meta)
    meta:set_int("check_y",owl_tech.get_y_pump(meta)-1)
end

function owl_tech.add_x_pump(meta)
    meta:set_int("check_x",owl_tech.get_x_pump(meta)-1)
end

function owl_tech.add_z_pump(meta)
    meta:set_int("check_z",owl_tech.get_z_pump(meta)-1)
end

function owl_tech.reset_x_pump(meta)
    meta:set_int("check_x",meta:get_int("max_horizontal_distance"))
end

function owl_tech.reset_z_pump(meta)
    meta:set_int("check_z",meta:get_int("max_horizontal_distance"))
end

function owl_tech.pos_pumping_update(meta)
	if (-owl_tech.get_max_horizontal_distance(meta))>owl_tech.get_x_pump(meta)then --x
		owl_tech.reset_x_pump(meta)
		owl_tech.add_z_pump(meta)
	end
end
function owl_tech.finde_fluid_on_level(meta,pos)
	local finde = false
	local new_pos = {
		x=pos.x+owl_tech.get_max_horizontal_distance(meta),
		y=pos.y+owl_tech.get_y_pump(meta),
		z=pos.z+owl_tech.get_max_horizontal_distance(meta)
	}
	for i = owl_tech.get_max_horizontal_distance(meta), -owl_tech.get_max_horizontal_distance(meta), -1 do --x
		new_pos.x= new_pos.x-1 
		for j = owl_tech.get_max_horizontal_distance(meta), -owl_tech.get_max_horizontal_distance(meta), -1 do--z
			new_pos.z= new_pos.z-1
			if (minetest.get_node(new_pos).name)=="mclx_core:river_water_source" or (minetest.get_node(new_pos).name)=="mcl_core:water_source" or (minetest.get_node(new_pos).name)=="mcl_core:lava_source" then
				finde =true
				return true
			end
		end
	end
	return finde
end

local function set_formspect_base_pump(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
    local fluid_2_name = owl_tech.get_pull_fluid_name(meta,2)
	local fluid_2_volume = owl_tech.get_pull_volume(meta,2)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Stean pump"))).."]"..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
   	"label[0,2.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_2_name.."--"..fluid_2_volume))).."]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end
--decor pump fluid 
minetest.register_node("owl_tech:decor_bronze_pipe",{
    description = "Decor fluid pipe ",
    _tt_help = S("Safe for decor"),
    tiles = {"(owl_tech_base_fluid_pipe.png^[colorize:#a35900:128)"},
    paramtype = "light",
    is_ground_content = false,
    stack_max = 64,
    drop ="",
    sunlight_propagates = true,
    walkable = true,
    groups = {owl_tech_decor_fluid_pipe=1},
    drawtype = "nodebox",
    node_box = {
        type = "connected",
        fixed = {-0.125,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_top = {-0.125,-0.125,-0.125,0.125,0.5,0.125} ,
        connect_bottom = {-0.125,-0.5,-0.125,0.125,0.125,0.125} ,
        connect_front = {-0.125,-0.125,-0.5,0.125,0.125,0.125} ,
        connect_left = {-0.5,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_back = {-0.125,-0.125,-0.125,0.125,0.125,0.5} ,
        connect_right = {-0.125,-0.125,-0.125,0.5,0.125,0.125} ,
    },
    connects_to = {"group:owl_tech_decor_fluid_pipe","group:owl_tech_machine"},
    sounds = mcl_sounds.node_sound_wool_defaults(),
    _mcl_hardness = 0.1,
    _mcl_blast_resistance = 0.1,
})
--bronze_pump"
minetest.register_node("owl_tech:bronze_pump", {
	description = S("Bronze pump"),
	_doc_items_longdesc = S("Pump fluid"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_base_pump.png",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_base_pump.png",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_base_pump.png",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_base_pump.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,8000,1,0) --steam  eat 25 steam in 1 tick
        owl_tech.add_new_pull(meta,8000,0,1) 
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,1) --all steam mashine has 1 tire
        owl_tech.prepear_for_pump(meta,2)
		set_formspect_base_pump(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
        --gete fluid down air mcl_core:lava_source mclx_core:water_source mcl_core:water_source
		local new_pos = {
			x=pos.x+owl_tech.get_x_pump(meta),
			y=pos.y+owl_tech.get_y_pump(meta),
			z=pos.z+owl_tech.get_z_pump(meta)
		}if owl_tech.get_pull_volume(meta,1)>100 then
			if minetest.get_node({x=pos.x,y=pos.y+owl_tech.get_y_pump(meta),z=pos.z}).name=="air" and 
			not owl_tech.finde_fluid_on_level(meta,pos) then -- place decor pipe
				minetest.set_node({x=pos.x,y=pos.y+owl_tech.get_y_pump(meta),z=pos.z},{name="owl_tech:decor_bronze_pipe"})
				owl_tech.reset_x_pump(meta)
				owl_tech.reset_z_pump(meta)
				owl_tech.add_y_pump(meta)
			end
			if (minetest.get_node(new_pos).name)=="mclx_core:river_water_source" or (minetest.get_node(new_pos).name)=="mcl_core:water_source" or (minetest.get_node(new_pos).name)=="mcl_core:lava_source" then
				if (owl_tech.get_pull_fluid_name(meta,2)=='none' or owl_tech.get_pull_fluid_name(meta,2)=='mcl_core:water_source' or owl_tech.get_pull_fluid_name(meta,2)=='mclx_core:river_water_source' or (minetest.get_node(new_pos).name)=="mcl_core:lava_source")
				and owl_tech.get_pull_max_volume(meta,2)-owl_tech.get_pull_volume(meta,2)>=1000 then
					owl_tech.add_fluid_in_node_pull(meta,minetest.get_node(new_pos).name,1000,2)
					owl_tech.set_pull_volume(meta,1, owl_tech.get_pull_volume(meta,1)-100)
					minetest.set_node(new_pos,{name="air"})
				end
			end
			owl_tech.pos_pumping_update(meta)
			owl_tech.add_x_pump(meta)
		end
        --
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x+1,y=pos.y,z=pos.z})).name,"fluid_pipe") then --sand in pipe
			local meta_up = minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z})
			local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,2)
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
				local pull_curent_volume = owl_tech.get_pull_volume(meta,2)
				local difer = pull_curent_volume-remove_amount
				owl_tech.set_pull_volume(meta,2,difer)
			end
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x-1,y=pos.y,z=pos.z})).name,"fluid_pipe") then --sand in pipe
			local meta_up = minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z})
			local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,2)
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
				local pull_curent_volume = owl_tech.get_pull_volume(meta,2)
				local difer = pull_curent_volume-remove_amount
				owl_tech.set_pull_volume(meta,2,difer)
			end
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+1})).name,"fluid_pipe") then --sand in pipe
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1})
			local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,2)
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
				local pull_curent_volume = owl_tech.get_pull_volume(meta,2)
				local difer = pull_curent_volume-remove_amount
				owl_tech.set_pull_volume(meta,2,difer)
			end
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z-1})).name,"fluid_pipe") then --sand in pipe
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1})
			local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,2)
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
				local pull_curent_volume = owl_tech.get_pull_volume(meta,2)
				local difer = pull_curent_volume-remove_amount
				owl_tech.set_pull_volume(meta,2,difer)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_base_pump(meta)
		timer:start(0.2)
	end
})
--Crafte iron fluid pipe
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:bronze_pump",
    recipe = {
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"},
        {"owl_tech:bronze_fluid_pipe","owl_tech:bronze_frames","owl_tech:bronze_fluid_pipe"},
        {"owl_tech:bronze_plate","owl_tech:bronze_fluid_pipe","owl_tech:bronze_plate"}
    }
})