local S = minetest.get_translator(minetest.get_current_modname())

minetest.register_node("owl_tech:iron_fluid_pipe",{
    description = "Fluid iron pipe tire 1",
    _tt_help = S("Transport fluids"),
    _doc_items_longdesc = S("Fence gates can be opened or closed and can't be jumped over. Fences will connect nicely to fence gates."),
    _doc_items_usagehelp = S("Right-click the fence gate to open or close it."),
    tiles = {"owl_tech_base_fluid_pipe.png"},
    paramtype = "light",
    is_ground_content = false,
    stack_max = 64,
    sunlight_propagates = true,
    walkable = true,
    groups = {pickaxey = 1,owl_tech_fluid_pipe=1},
    drawtype = "nodebox",
    node_box = {
        type = "connected",
        fixed = {-0.125,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_top = {-0.125,-0.125,-0.125,0.125,0.5,0.125} ,
        connect_bottom = {-0.125,-0.5,-0.125,0.125,0.125,0.125} ,
        connect_front = {-0.125,-0.125,-0.5,0.125,0.125,0.125} ,
        connect_left = {-0.5,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_back = {-0.125,-0.125,-0.125,0.125,0.125,0.5} ,
        connect_right = {-0.125,-0.125,-0.125,0.5,0.125,0.125} ,
    },
    connects_to = {"group:owl_tech_fluid_pipe","group:owl_tech_generator","group:owl_tech_machine"},
    sounds = mcl_sounds.node_sound_wool_defaults(),
    _mcl_hardness = 0.1,
    _mcl_blast_resistance = 0.1,
    on_construct = function(pos)
        local meta =  minetest.get_meta(pos)
        owl_tech.add_new_pull(meta,300,1,1)
        owl_tech.set_fluid_sand_in_tick(meta,60)
        owl_tech.check_all_side_for_fluid_work(pos,meta)
        owl_tech.update_fluid_pipe_around(pos) 
        local info_set = owl_tech.get_pull_volume(meta,1)
        meta:set_string("infotext", info_set)
        local timer =minetest.get_node_timer(pos)
        timer:start(0.2)
    end,
    on_timer = function (pos, elapsed)
        local meta =  minetest.get_meta(pos)
        local timer =minetest.get_node_timer(pos)
        if owl_tech.get_pull_volume(meta,1)>0  then
            owl_tech.send_for_all_sides_fluid_pipe(meta,pos)
        end
        local info_set = owl_tech.get_pull_volume(meta,1)
        meta:set_string("infotext", info_set)
        timer:start(0.01)
    end
})
--Crafte iron fluid pipe
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:iron_fluid_pipe 4",
    recipe = {
        {"owl_tech:iron_plate","mcl_core:glass","owl_tech:iron_plate"},
        {"","",""},
        {"","",""}
    }
--Bronze pipe 
})minetest.register_node("owl_tech:bronze_fluid_pipe",{
    description = "Fluid bronze pipe tire 1",
    _tt_help = S("Transport fluids"),
    _doc_items_longdesc = S("Fence gates can be opened or closed and can't be jumped over. Fences will connect nicely to fence gates."),
    _doc_items_usagehelp = S("Right-click the fence gate to open or close it."),
    tiles = {"owl_tech_base_fluid_pipe.png^[colorize:#a35900:128"},
    paramtype = "light",
    is_ground_content = false,
    stack_max = 64,
    sunlight_propagates = true,
    walkable = true,
    groups = {pickaxey = 1,owl_tech_fluid_pipe=1},
    drawtype = "nodebox",
    node_box = {
        type = "connected",
        fixed = {-0.125,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_top = {-0.125,-0.125,-0.125,0.125,0.5,0.125} ,
        connect_bottom = {-0.125,-0.5,-0.125,0.125,0.125,0.125} ,
        connect_front = {-0.125,-0.125,-0.5,0.125,0.125,0.125} ,
        connect_left = {-0.5,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_back = {-0.125,-0.125,-0.125,0.125,0.125,0.5} ,
        connect_right = {-0.125,-0.125,-0.125,0.5,0.125,0.125} ,
    },
    connects_to = {"group:owl_tech_fluid_pipe","group:owl_tech_generator","group:owl_tech_machine"},
    sounds = mcl_sounds.node_sound_wool_defaults(),
    _mcl_hardness = 0.1,
    _mcl_blast_resistance = 0.1,
    on_construct = function(pos)
        local meta =  minetest.get_meta(pos)
        owl_tech.add_new_pull(meta,300,1,1)
        owl_tech.set_fluid_sand_in_tick(meta,60)
        owl_tech.check_all_side_for_fluid_work(pos,meta)
        owl_tech.update_fluid_pipe_around(pos) 
        local info_set = owl_tech.get_pull_volume(meta,1)
        meta:set_string("infotext", info_set)
        local timer =minetest.get_node_timer(pos)
        timer:start(0.2)
    end,
    on_timer = function (pos, elapsed)
        local meta =  minetest.get_meta(pos)
        local timer =minetest.get_node_timer(pos)
        if owl_tech.get_pull_volume(meta,1)>0  then
            owl_tech.send_for_all_sides_fluid_pipe(meta,pos)
        end
        local info_set = owl_tech.get_pull_volume(meta,1)
        meta:set_string("infotext", info_set)
        timer:start(0.01)
    end
})
--Crafte iron fluid pipe
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:bronze_fluid_pipe 4",
    recipe = {
        {"owl_tech:bronze_plate","mcl_core:glass","owl_tech:bronze_plate"},
        {"","",""},
        {"","",""}
    }
})
