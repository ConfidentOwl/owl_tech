local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

minetest.register_node("owl_tech:iron_item_pipe_input",{
    description = "Item input pipe tire 1",
    _tt_help = S("Input items in pipe systems"),
    _doc_items_longdesc = S("Use for input items in pipe "),
    _doc_items_usagehelp = S("Put it in the world to start pulling items from inventory."),
    tiles = {"owl_tech_base_fluid_pipe.png^[colorize:#0066ff:128"},
    paramtype = "light",
    is_ground_content = false,
    stack_max = 64,
    sunlight_propagates = true,
    walkable = true,
    groups = {pickaxey = 1,owl_tech_item_pipe=1},
    drawtype = "nodebox",
    node_box = {
        type = "connected",
        fixed = {-0.125,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_top = {-0.125,-0.125,-0.125,0.125,0.5,0.125} ,
        connect_bottom = {-0.125,-0.5,-0.125,0.125,0.125,0.125} ,
        connect_front = {-0.125,-0.125,-0.5,0.125,0.125,0.125} ,
        connect_left = {-0.5,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_back = {-0.125,-0.125,-0.125,0.125,0.125,0.5} ,
        connect_right = {-0.125,-0.125,-0.125,0.5,0.125,0.125} ,
    },
    connects_to = {"group:owl_tech_item_pipe","group:owl_tech_generator","group:owl_tech_machine","mcl_chests:chest_small"},
    sounds = mcl_sounds.node_sound_wool_defaults(),
    _mcl_hardness = 0.1,
    _mcl_blast_resistance = 0.1,
    on_construct = function(pos)
        local meta =  minetest.get_meta(pos)
        meta:set_int("has_filte",0)
        local inv= meta:get_inventory()
        local itemstack = inv:get_stack("pipe_inv", 1)
        owl_tech.set_item_pipe_logick(meta)
        local info_set =("empty")
        if not itemstack:is_empty() then
            info_set = (itemstack:get_name().."-"..itemstack:get_count())
        end
        meta:set_string("infotext", info_set)
        local timer =minetest.get_node_timer(pos)
        timer:start(0.2)
    end,
    on_timer = function (pos, elapsed)
        local meta =  minetest.get_meta(pos)
        local inv= meta:get_inventory()
        local itemstack = inv:get_stack("pipe_inv", 1)
        local timer =minetest.get_node_timer(pos)
        owl_tech.finde_pipe_and_send_item(pos)
        local info_set =("empty")
        if not itemstack:is_empty() then
            info_set = (itemstack:get_name().."-"..itemstack:get_count())
        end
        owl_tech.finde_inventory_slot_for_removing_item_around(pos)
        meta:set_string("infotext", info_set)
        timer:start(0.2)
    end
})
--Crafte input pipe
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:iron_item_pipe_input",
    recipe = {
        {"owl_tech:iron_plate","mesecons_pistons:piston_normal_off","owl_tech:iron_plate"},
        {"","",""},
        {"","",""}
    }
})
minetest.register_node("owl_tech:iron_item_pipe",{
    description = "Item pipe tire 1",
    _tt_help = S("Transport items"),
    _doc_items_longdesc = S("Fence gates can be opened or closed and can't be jumped over. Fences will connect nicely to fence gates."),
    _doc_items_usagehelp = S("Put it in the world to let objects move through it."),
    tiles = {"owl_tech_base_fluid_pipe.png"},
    paramtype = "light",
    is_ground_content = false,
    stack_max = 64,
    sunlight_propagates = true,
    walkable = true,
    groups = {pickaxey = 1,owl_tech_item_pipe=1,owl_tech_can_pipe_output=1},
    drawtype = "nodebox",
    node_box = {
        type = "connected",
        fixed = {-0.125,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_top = {-0.125,-0.125,-0.125,0.125,0.5,0.125} ,
        connect_bottom = {-0.125,-0.5,-0.125,0.125,0.125,0.125} ,
        connect_front = {-0.125,-0.125,-0.5,0.125,0.125,0.125} ,
        connect_left = {-0.5,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_back = {-0.125,-0.125,-0.125,0.125,0.125,0.5} ,
        connect_right = {-0.125,-0.125,-0.125,0.5,0.125,0.125} ,
    },
    connects_to = {"group:owl_tech_item_pipe","group:owl_tech_generator","group:owl_tech_machine"},
    sounds = mcl_sounds.node_sound_wool_defaults(),
    _mcl_hardness = 0.1,
    _mcl_blast_resistance = 0.1,
    on_construct = function(pos)
        local meta =  minetest.get_meta(pos)
        local inv= meta:get_inventory()
        local itemstack = inv:get_stack("pipe_inv", 1)
        owl_tech.set_item_pipe_logick(meta)
        local info_set =("empty")
        if not itemstack:is_empty() then
            info_set = (itemstack:get_name().."-"..itemstack:get_count())
        end
        meta:set_string("infotext", info_set)
        local timer =minetest.get_node_timer(pos)
        timer:start(0.2)
    end,
    on_timer = function (pos, elapsed)
        local meta =  minetest.get_meta(pos)
        local inv= meta:get_inventory()
        local itemstack = inv:get_stack("pipe_inv", 1)
        local timer =minetest.get_node_timer(pos)
        owl_tech.finde_pipe_and_send_item(pos)
        local info_set =("empty")
        if not itemstack:is_empty() then
            info_set = (itemstack:get_name().."-"..itemstack:get_count())
        end
        meta:set_string("infotext", info_set)
        timer:start(0.01)
    end
})
--Crafte input pipe
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:iron_item_pipe 4",
    recipe = {
        {"owl_tech:iron_plate","mcl_core:glass","owl_tech:iron_plate"},
        {"","",""},
        {"","",""}
    }
})
minetest.register_node("owl_tech:iron_item_pipe_output",{
    description = "Item pipe tire 1 output",
    _tt_help = S("Move item from pipe to invenotry"),
    _doc_items_longdesc = S("Fence gates can be opened or closed and can't be jumped over. Fences will connect nicely to fence gates."),
    _doc_items_usagehelp = S("Put it in the world to put the item in inventory."),
    tiles = {"owl_tech_base_fluid_pipe.png^[colorize:#ff3300:128"},
    paramtype = "light",
    is_ground_content = false,
    stack_max = 64,
    sunlight_propagates = true,
    walkable = true,
    groups = {pickaxey = 1,owl_tech_item_pipe=1,owl_tech_can_pipe_output=1},
    drawtype = "nodebox",
    node_box = {
        type = "connected",
        fixed = {-0.125,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_top = {-0.125,-0.125,-0.125,0.125,0.5,0.125} ,
        connect_bottom = {-0.125,-0.5,-0.125,0.125,0.125,0.125} ,
        connect_front = {-0.125,-0.125,-0.5,0.125,0.125,0.125} ,
        connect_left = {-0.5,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_back = {-0.125,-0.125,-0.125,0.125,0.125,0.5} ,
        connect_right = {-0.125,-0.125,-0.125,0.5,0.125,0.125} ,
    },
    connects_to = {"group:owl_tech_item_pipe","group:owl_tech_generator","group:owl_tech_machine"},
    sounds = mcl_sounds.node_sound_wool_defaults(),
    _mcl_hardness = 0.1,
    _mcl_blast_resistance = 0.1,
    on_construct = function(pos)
        local meta =  minetest.get_meta(pos)
        local inv= meta:get_inventory()
        local itemstack = inv:get_stack("pipe_inv", 1)
        owl_tech.set_item_pipe_logick(meta)
        local info_set =("empty")
        if not itemstack:is_empty() then
            info_set = (itemstack:get_name().."-"..itemstack:get_count())
        end
        meta:set_string("infotext", info_set)
        local timer =minetest.get_node_timer(pos)
        timer:start(0.2)
    end,
    on_timer = function (pos, elapsed)
        local meta =  minetest.get_meta(pos)
        local inv= meta:get_inventory()
        local itemstack = inv:get_stack("pipe_inv", 1)
        local timer =minetest.get_node_timer(pos)
        owl_tech.send_item_in_inventory(pos)
        local info_set =("empty")
        if not itemstack:is_empty() then
            info_set = (itemstack:get_name().."-"..itemstack:get_count())
        end
        meta:set_string("infotext", info_set)
        timer:start(0.2)
    end
})
--Crafte input pipe
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:iron_item_pipe_output",
    recipe = {
        {"owl_tech:iron_plate","mesecons_pistons:piston_sticky_off","owl_tech:iron_plate"},
        {"","",""},
        {"","",""}
    }
})
-----------------------------------------------------------
-- Copper item pipe ( add simple filter )
local function set_formspect_copper_pipe(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
    "list[context;filte_itemsteack;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Copper pipe"))).."]"..
	"label[0,0.5;"..minetest.formspec_escape(minetest.colorize("#313131", ("Filte - input only this item"))).."]"..
	"listring[context;filte_itemsteack]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end

minetest.register_node("owl_tech:copper_item_pipe_input",{
    description = "Item input pipe tire 2",
    _tt_help = S("Input items in pipe systems and simple filter by name (name take from item in inventory)"),
    _doc_items_longdesc = S("Use for input items in pipe "),
    _doc_items_usagehelp = S("Put it in the world to start pulling items from inventory."),
    tiles = {"(owl_tech_base_fluid_pipe.png^[colorize:#0066ff:128)^owl_tech_base_fluid_pipe_copper_parts.png"},
    paramtype = "light",
    is_ground_content = false,
    stack_max = 64,
    sunlight_propagates = true,
    walkable = true,
    groups = {owl_tech_item_pipe=1},
    drawtype = "nodebox",
    node_box = {
        type = "connected",
        fixed = {-0.125,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_top = {-0.125,-0.125,-0.125,0.125,0.5,0.125} ,
        connect_bottom = {-0.125,-0.5,-0.125,0.125,0.125,0.125} ,
        connect_front = {-0.125,-0.125,-0.5,0.125,0.125,0.125} ,
        connect_left = {-0.5,-0.125,-0.125,0.125,0.125,0.125} ,
        connect_back = {-0.125,-0.125,-0.125,0.125,0.125,0.5} ,
        connect_right = {-0.125,-0.125,-0.125,0.5,0.125,0.125} ,
    },
    connects_to = {"group:owl_tech_item_pipe","group:owl_tech_generator","group:owl_tech_machine","mcl_chests:chest_small"},
    sounds = mcl_sounds.node_sound_wool_defaults(),
    _mcl_hardness = 0.1,
    _mcl_blast_resistance = 0.1,
    on_construct = function(pos)
        local meta =  minetest.get_meta(pos)
        meta:set_int("has_filte",1)
        local inv= meta:get_inventory()
        local itemstack = inv:get_stack("pipe_inv", 1)
        local filte_itemstack = inv:get_stack("filte_itemsteack",1)
        owl_tech.set_item_pipe_logick(meta)
        local info_set =("empty")
        if not itemstack:is_empty() and not filte_itemstack:is_empty() then
            info_set = (itemstack:get_name().."-"..itemstack:get_count())
        end
        meta:set_string("infotext", info_set)
        set_formspect_copper_pipe(meta)
        inv:set_size("filte_itemsteack",1)
        local timer =minetest.get_node_timer(pos)
        timer:start(0.2)
    end,
    on_timer = function (pos, elapsed)
        local meta =  minetest.get_meta(pos)
        local inv= meta:get_inventory()
        local itemstack = inv:get_stack("pipe_inv", 1)
        local filte_itemstack = inv:get_stack("filte_itemsteack",1)
        local timer =minetest.get_node_timer(pos)
        owl_tech.finde_pipe_and_send_item(pos)
        local info_set =("empty")
        if not itemstack:is_empty() then
            info_set = (itemstack:get_name().."-"..itemstack:get_count())
        end
        owl_tech.finde_inventory_slot_for_removing_item_around(pos)
        set_formspect_copper_pipe(meta)
        meta:set_string("infotext", info_set)
        timer:start(0.2)
    end
})
--Crafte input pipe
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:iron_item_pipe_input",
    recipe = {
        {"owl_tech:copper_plate","mesecons_pistons:piston_normal_off","owl_tech:copper_plate"},
        {"","",""},
        {"","",""}
    }
})