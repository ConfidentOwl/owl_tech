local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

dofile(minetest.get_modpath("owl_tech") .. "/pipe/fluid_pipe.lua") --fluid pipe logick
dofile(minetest.get_modpath("owl_tech") .. "/pipe/fluid_pump.lua") --fluid pump logick
dofile(minetest.get_modpath("owl_tech") .. "/pipe/fluid_tank.lua") --fluid tank logick
dofile(minetest.get_modpath("owl_tech") .. "/pipe/item_pipe.lua") --item pipe logick
dofile(minetest.get_modpath("owl_tech") .. "/pipe/water_concetrator.lua") --water_concetrator