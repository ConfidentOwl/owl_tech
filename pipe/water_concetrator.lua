local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

--owl_tech_water_con_planks.png
minetest.register_node("owl_tech:water_concetrate_planks", {
    description = S("Place 2 planks on a top of water concetrator to work"),
    _doc_items_longdesc = S("Place 2 planks on a top of water concetrator to work"),
    tiles = {"owl_tech_water_con_planks.png"},
    is_ground_content = false,
    stack_max = 64,
    groups = {pickaxey=1, building_block=1},
    sounds = mcl_sounds.node_sound_metal_defaults(),
    _mcl_blast_resistance = 2,
    _mcl_hardness = 1,
})
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:water_concetrate_planks",
    recipe = {
        {"owl_tech:iron_plate","owl_tech:iron_plate",""},
        {"group:wood","group:wood",""},
        {"","",""},
    }
})

local function set_formspect_tank(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)	
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Water Concetrator"))).."]"..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"list[context;input_in;2.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,1.5,1,1)..
	"list[context;dst;3.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(3.5,1.5,1,1)..
	"list[context;input_in_add;2.5,3.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,3.5,1,1)..
	"list[context;dst_add;3.5,3.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(3.5,3.5,1,1)..
	"listring[context;dst]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[context;input_in_add]"..
	"listring[context;dst_add]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end

minetest.register_node("owl_tech:water_concetrate", {
	description = S("Water Concetrator"),
	_doc_items_longdesc = S("Concetrate water and output in pipe"),
	tiles = {
		"owl_tech_water_con_planks.png",
		"owl_tech_water_con_planks.png",
		"(owl_tech_base_briks.png^[colorize:#a35900:128)",
		"(owl_tech_base_briks.png^[colorize:#a35900:128)",
		"(owl_tech_base_briks.png^[colorize:#a35900:128)",
		"(owl_tech_base_briks.png^[colorize:#a35900:128)",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,16000,1,1)
        owl_tech.set_pull_fluid_whitlist(meta,1,"mcl_core:water_source") --whitlist for make input ONLY Water
        owl_tech.set_mashine_tire(meta,1) 
		owl_tech.update_fluid_pipe_around(pos)
		owl_tech.set_fluid_sand_in_tick(meta,60)
		local inv =  meta:get_inventory()
		inv:set_size("input_in", 1) --for input
		inv:set_size("dst", 1) --for input 
		inv:set_size("input_in_add", 1) --for output
		inv:set_size("dst_add", 1) --for output
		local timer =minetest.get_node_timer(pos)
        meta:set_string("infotext",owl_tech.get_pull_volume(meta,1).."--"..owl_tech.get_pull_fluid_name(meta,1).."--"..owl_tech.get_pull_max_volume(meta,1))
		set_formspect_tank(meta)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv =  meta:get_inventory()
		local cell_input_1 = inv:get_stack("input_in", 1)
		local cell_output_1 = inv:get_stack("dst", 1)
		local cell_input_2 = inv:get_stack("input_in_add", 1)
		local cell_output_2 = inv:get_stack("dst_add", 1)
		if (minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name =="owl_tech:water_concetrate_planks" and (minetest.get_node({x=pos.x,y=pos.y+2,z=pos.z})).name =="owl_tech:water_concetrate_planks" then --get from pipe steam
            if owl_tech.get_pull_max_volume(meta,1)> owl_tech.get_pull_volume(meta,1)+1 then
                owl_tech.set_pull_fluid_name(meta,1,"mcl_core:water_source")
                owl_tech.set_pull_volume(meta,1,owl_tech.get_pull_volume(meta,1)+1)
            end
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"owl_tech_fluid_pipe") then --sand in pipe
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,1)
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
				local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
				local difer = pull_curent_volume-remove_amount
				owl_tech.set_pull_volume(meta,2,difer)
			end
		end
		if  minetest.get_item_group(cell_input_2:get_name(),"load_cell")>0 and ((owl_tech.get_pull_max_volume(meta,1)-owl_tech.get_pull_volume(meta,1))>=1000 )then
			local fluid_name = string.sub(cell_input_2:get_name() , 10)
			if  FLUID_CELL_TO_NAME[fluid_name]~=nil and FLUID_CELL_TO_NAME[fluid_name]~="" and  
			(cell_output_2:is_empty() or (cell_output_2:get_name()=="owl_tech:empty_cell" and cell_output_2:get_free_space()>0 )) then
				if not cell_output_2:is_empty() then-- Add  main output 
					cell_output_2:set_count(cell_output_2:get_count()+1) 
					inv:set_stack('dst_add', 1, cell_output_2)
				else
					local item ={name="owl_tech:empty_cell", count=1, wear=0, metadata=""}
					cell_output_2:add_item(item)
					inv:set_stack('dst_add', 1, cell_output_2)
				end
				owl_tech.set_pull_volume(meta,1,owl_tech.get_pull_volume(meta,1)+1000)
				owl_tech.set_pull_fluid_name(meta,1,FLUID_CELL_TO_NAME[fluid_name])
				cell_input_2:set_count(cell_input_2:get_count()-1)
				inv:set_stack('input_in_add', 1, cell_input_2)
			end	
		end
		if not cell_input_1:is_empty() and cell_input_1:get_name() =="owl_tech:empty_cell" and owl_tech.get_pull_volume(meta,1)>=1000 then
			local fluid_name = string.sub(owl_tech.get_pull_fluid_name(meta,1) , 10)
			if  FLUID_NAME_TO_CELL[fluid_name]~=nil and FLUID_NAME_TO_CELL[fluid_name]~="" and  
			(cell_output_1:is_empty() or (cell_output_1:get_name()==FLUID_NAME_TO_CELL[fluid_name] and cell_output_1:get_free_space()>0 )) then
				if not cell_output_1:is_empty() then-- Add  main output 
					cell_output_1:set_count(cell_output_1:get_count()+1) 
					inv:set_stack('dst', 1, cell_output_1)
				else
					local item ={name=FLUID_NAME_TO_CELL[fluid_name], count=1, wear=0, metadata=""}
					cell_output_1:add_item(item)
					inv:set_stack('dst', 1, cell_output_1)
				end
				owl_tech.set_pull_volume(meta,1,owl_tech.get_pull_volume(meta,1)-1000)
				cell_input_1:set_count(cell_input_1:get_count()-1)
				inv:set_stack('input_in', 1, cell_input_1)
			end	
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_tank(meta)
        meta:set_string("infotext",owl_tech.get_pull_volume(meta,1).."--"..owl_tech.get_pull_fluid_name(meta,1).."--"..owl_tech.get_pull_max_volume(meta,1))
		timer:start(0.2)
	end
})
--Crafte iron fluid pipe
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:water_concetrate",
    recipe = {
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","owl_tech:water_concetrate_planks","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","owl_tech:bronze_fluid_pipe","owl_tech:bronze_plate"}
    }
})