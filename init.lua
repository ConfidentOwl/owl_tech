local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

local i3_name = minetest.get_modpath('i3')

dofile(path .."/config.txt")-- all config load
dofile(path .. "/api/init.lua") --all base func
dofile(path .. "/tools/init.lua") --Tools
dofile(path .. "/ore/init.lua") --ore loads
dofile(path .. "/debug_tools/init.lua") --debug_tools don.t use in surv game
if owl_tech_x_ray_look then
    dofile(path .. "/xray/init.lua") --xray don.t use in surv game
end
dofile(path .. "/steam/init.lua") --main fail about all steam gen 
dofile(path .. "/pipe/init.lua") --main fail about all pipes  
dofile(path .. "/custom_recips/init.lua") --main fail about all custom recips  
dofile(path .. "/mashins/init.lua") --main fail about all pipes   
dofile(path .. "/lists_of_all.lua") --GLOBAL carialbe
if i3_name~=nil then
    dofile(path .. "/i3_integration/init.lua") --i3 integration
end
dofile(path .. "/multiblocks/init.lua") --Multi-nodes   
dofile(path .. "/farming/init.lua") --Farming 
dofile(path .. "/electro_wire/init.lua")--electro_vire    
dofile(path .. "/electro_generator/init.lua")--electro generator    
dofile(path .. "/battery/init.lua")-- all electro battaries