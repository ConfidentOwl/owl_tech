local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

minetest.register_node("owl_tech:quary_point", {
	description = S("Quary point"),
	_doc_items_longdesc = S("Point the pos for quary "),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_quary_point.png",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_quary_point.png",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_quary_point.png",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_quary_point.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,dst=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
})
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:quary_point",
    recipe = {
        {"mesecons_torch:mesecon_torch_on","owl_tech:bronze_plate","mesecons_torch:mesecon_torch_on"},
        {"owl_tech:bronze_plate","owl_tech:bronze_frames","owl_tech:bronze_plate"},
        {"mesecons_torch:mesecon_torch_on","owl_tech:bronze_plate","mesecons_torch:mesecon_torch_on"}
    }
})
if OWL_TECH_ADVANCED_STEAM_MACHINS then

	local function set_formspect_steam_quarry(meta)
		local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
		local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
		local delay = meta:get_int("work_delay")
		local formspec = "size[9,8.75]"..
		"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
		"list[current_player;main;0,4.5;9,3;9]"..
		mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
		"list[current_player;main;0,7.74;9,1;]"..
		mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
		"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Steam vertical miner"))).."]"..
		"list[context;dst;5.5,1.5;1,1;]"..
		mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
		"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
		"label[0,2.5;"..minetest.formspec_escape(minetest.colorize("#313131", (delay.."--delay"))).."]"..
		"listring[context;dst]"..
		"listring[current_player;main]"..
		"listring[current_player;main]"..
		"listring[current_player;main]"
		meta:set_string("formspec", formspec)
	end

	minetest.register_node("owl_tech:steam_quarry", {
		description = S("Steam quarry"),
		_doc_items_longdesc = S("Dig the Hole"),
		tiles = {
			"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_steam_input.png",
			"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
			"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_quary.png",
			"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_quary.png",
			"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_quary.png",
			"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_quary.png",
		},
		is_ground_content = false,
		stack_max = 64,
		groups = {pickaxey=2, owl_tech_machine=1,dst=1},
		sounds = mcl_sounds.node_sound_metal_defaults(),
		paramtype2 = "facedir",
		_mcl_blast_resistance = 6,
		_mcl_hardness = 5,
		on_construct = function(pos)
			local meta = minetest.get_meta(pos)
			owl_tech.add_new_pull(meta,8000,1,0) --steam  eat 25 steam in 1 tick
			owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
			owl_tech.set_mashine_tire(meta,1) --all steam mishine has 1 tire
			meta:set_int("work_delay",0)
			set_formspect_steam_quarry(meta)
			owl_tech.prepear_for_verticak_steam_miner(pos)
			local timer =minetest.get_node_timer(pos)
			local inv = meta:get_inventory()
			inv:set_size("dst", 1)
			timer:start(2)
		end,
		on_timer = function(pos, elapsed)
			local meta = minetest.get_meta(pos)
			local timer = minetest.get_node_timer(pos)
			if meta:get_int("min_x")==0 and meta:get_int("min_z")==0 then
				owl_tech.scan_for_point(pos,meta,8)
			end
			if meta:get_int("get_bedrock")==0 and meta:get_int("work_delay")==0 and 
			owl_tech.get_pull_volume(meta,1)>=15 then
				local inv = meta:get_inventory()
				owl_tech.set_pull_volume(meta,1,owl_tech.get_pull_volume(meta,1)-15)
				local can_mine = owl_tech.can_quary_mine_this(pos,meta)
				owl_tech.diging_quary(pos,meta,can_mine)
				meta:set_int("work_delay",20)
			end
			if meta:get_int("work_delay")>0 then
				meta:set_int("work_delay",meta:get_int("work_delay")-1)
			end
			if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
				local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
				local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
				local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
				if can_do and can_do2 then
					owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
					owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
				end
			end
			owl_tech.delit_name_fluid_if_0(meta)
			set_formspect_steam_quarry(meta)
			timer:start(0.2)
		end
	})
	--Crafte controller 
	minetest.register_craft({
		type = "shaped",
		output = "owl_tech:steam_quarry",
		recipe = {
			{"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"},
			{"owl_tech:bronze_plate","owl_tech:bronze_frames","owl_tech:bronze_plate"},
			{"owl_tech:steam_vertical_miner","owl_tech:steam_vertical_miner","owl_tech:steam_vertical_miner"}
		}
	})
end
