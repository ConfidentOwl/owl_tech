local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

dofile(path .. "/mashins/steam_machins.lua") --steam mashins 
dofile(path .. "/mashins/vertical_miner.lua")   
dofile(path .. "/mashins/quarry.lua") 
dofile(path .. "/mashins/electro_machins.lua")
dofile(path .."/mashins/solar_panel.lua") 