local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

	--[[USE for get tire machine 1)Name 2)name 3)voltage 4)max_charge 5)color_code  6)metal name for crafte 7)has_macerator 8)help item list 9)has_alloy_smeter 
	10)has_siev 11)has_furnance 12)forge hammer  13)has vertical miner 14)delay verticalminer 15)has quarry 16 )distance quarry 17)wood planter 18)wood_chopper 19)distance
	20)seed planter 21)seed harvester 22)has mixer 23)Fluid separartor]]

local table_all_machines={
	{"Base","base",32,1600,"#575757","steel",true,{"owl_tech:diamond","owl_tech:copper_heat_element","owl_tech:steel_mesh","mesecons_pistons:piston_normal_off","owl_tech:bronze_sharp_blade"},true,true
	,true,true,true,18,true,16,true,true,2,true,true,true,true}
}

for i = 1, #table_all_machines, 1 do
	if  table_all_machines[i][7]then
		local function set_formspect_base_electro_macerator(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local redines = owl_tech.get_item_redines(meta)
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S( table_all_machines[i][1].." macerator"))).."]"..
			"list[context;input_in;2.5,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
			"list[context;dst;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"list[context;dst_add;6.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(6.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
			"listring[context;dst]"..
			"listring[context;dst_add]"..
			"listring[current_player;main]"..
			"listring[context;input_in]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		
		--base macerator
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_macerator", {
			description = S(table_all_machines[i][1].."electro macerator"),
			_doc_items_longdesc = S("Firts electro macerater (tire"..i..")"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_macerator_face.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,dst=1,dst_add=1,owl_tech_electro_mashine=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				owl_tech.reset_item_redines(meta)
				meta:set_int("eat_in_tick",0)
				meta:set_int("bonus",0)
				set_formspect_base_electro_macerator(meta)
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("input_in", 1) 
				inv:set_size("dst", 1)
				inv:set_size("dst_add", 1)
				timer:start(0.2)
			end,
			on_timer = function(pos, elapsed)
				local timer = minetest.get_node_timer(pos)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				local input_its = inv:get_stack('input_in', 1)
				local dst_its = inv:get_stack('dst', 1)
				local dst_add_its = inv:get_stack('dst_add', 1)
				if not input_its:is_empty() then -- chek for item in slots  
					local name =""
					if "mcl_mangrove:mangroveleaves"==input_its:get_name() then
						name= "mangroveleaves"
					else
						name= string.sub(input_its:get_name(), 10)
					end
					if MACERATO_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
						if owl_tech.can_macaerat_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
							local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
							owl_tech:set_charge(meta,steam_new)
							owl_tech.add_item_redines(meta,meta:get_int("bonus"))
						end
					elseif MACERATO_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>=100 then
						if owl_tech.can_macaerat_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
							
							input_its:set_count(input_its:get_count()-1) --Remove item from input slot
							inv:set_stack('input_in', 1, input_its)

							if not dst_its:is_empty() then-- Add  main output 
								dst_its:set_count(dst_its:get_count()+MACERATO_RECIPS[name][4]) 
								inv:set_stack('dst', 1, dst_its)
							else
								local item ={name=MACERATO_RECIPS[name][3], count=MACERATO_RECIPS[name][4], wear=0, metadata=""}
								dst_its:add_item(item)
								inv:set_stack('dst', 1, dst_its)
							end

							local random = math.random(1,100) --chans to get second  output
							if MACERATO_RECIPS[name][6]>random then-- Add  second output if has 
								if not dst_add_its:is_empty() then
									dst_add_its:set_count(dst_add_its:get_count()+MACERATO_RECIPS[name][8]) 
									inv:set_stack('dst_add', 1, dst_add_its)
								else
									local item ={name=MACERATO_RECIPS[name][7], count=MACERATO_RECIPS[name][8], wear=0, metadata=""}
									dst_add_its:add_item(item)
									inv:set_stack('dst_add', 1, dst_add_its)
								end
							end
							meta:set_int("eat_in_tick",0)
							meta:set_int("bonus",0)
							owl_tech.reset_item_redines(meta)
						end
					elseif MACERATO_RECIPS[name]~=nil and  100/MACERATO_RECIPS[name][5]*12<= owl_tech:get_charge(meta)  and owl_tech.get_item_redines(meta)==0 then --begin for work
						local amount=owl_tech.calculate_recipe_tire_bonus(meta,MACERATO_RECIPS[name][2],MACERATO_RECIPS[name][5])
						meta:set_int("eat_in_tick",((100/MACERATO_RECIPS[name][5]*12)/(100/amount)))
						meta:set_int("bonus",amount)
						local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
						owl_tech:set_charge(meta,steam_new)
						owl_tech.add_item_redines(meta,meta:get_int("bonus"))
					end
				else 
					owl_tech.reset_item_redines(meta)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_base_electro_macerator(meta)
				timer:start(0.2)
			end
		})
		--Crafte 
		minetest.register_craft({
			type = "shaped",
			output = "owl_tech:"..table_all_machines[i][2].."_macerator",
			recipe = {
				{table_all_machines[i][8][1],table_all_machines[i][8][1],table_all_machines[i][8][1]},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_plate"}
			}
		})
	end
	if table_all_machines[i][9] then
		--alloy smelter
		local function set_formspect_base_alloy_smelter(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local redines = owl_tech.get_item_redines(meta)
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Steam alloy smelter"))).."]"..
			"list[context;input_in;2.5,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
			"list[context;input_in_add;4,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(4,2.5,1,1)..
			"list[context;dst;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
			"listring[context;dst]"..
			"listring[current_player;main]"..
			"listring[context;input_in]"..
			"listring[context;input_in_add]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		-------------------------------------------
		--alloy_smelter
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_alloy_smelter", {
			description = S( table_all_machines[i][1].." alloy smelter"),
			_doc_items_longdesc = S("Smelt alll"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_smelter_face.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,owl_tech_electro_mashine=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1)
				owl_tech.reset_item_redines(meta)
				meta:set_int("eat_in_tick",0)
				meta:set_int("bonus",0)
				set_formspect_base_alloy_smelter(meta)
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("input_in", 1) 
				inv:set_size("input_in_add", 1) 
				inv:set_size("dst", 1)
				timer:start(0.2)
			end,
			on_timer = function(pos, elapsed)
				local timer = minetest.get_node_timer(pos)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				local input_its = inv:get_stack('input_in', 1)
				local input_its_add = inv:get_stack('input_in_add', 1)
				local dst_its = inv:get_stack('dst', 1)
				if not input_its:is_empty() then -- chek for item in slots  
					local name1= string.sub(input_its:get_name(), 10)
					local name2 = string.sub(input_its_add:get_name(), 10)
					local name =""
					local recipt_true = false 
					if SMELTER_RECIPS[name1..name2]~=nil then
						recipt_true= true
						name=name1..name2
					elseif SMELTER_RECIPS[name2..name1]~=nil then
						recipt_true= true
						name=name2..name1
					end
					if recipt_true and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
						if owl_tech.can_smelter_recips_recips_go_in_this_mashien(meta,name,dst_its)  then-- chek for all itemslot and machine tire
							local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
							owl_tech:set_charge(meta,steam_new)
							owl_tech.add_item_redines(meta,meta:get_int("bonus"))
						end
					elseif recipt_true and owl_tech.get_item_redines(meta)>=100 then
						if owl_tech.can_smelter_recips_recips_go_in_this_mashien(meta,name,dst_its)   then-- chek for all itemslot and machine tire
							input_its:set_count(input_its:get_count()-SMELTER_RECIPS[name][1]) --Remove item from input slot
							inv:set_stack('input_in', 1, input_its)
							input_its_add:set_count(input_its_add:get_count()-SMELTER_RECIPS[name][2]) --Remove item from input slot
							inv:set_stack('input_in_add', 1, input_its_add )

							if not dst_its:is_empty() then-- Add  main output 
								dst_its:set_count(dst_its:get_count()+SMELTER_RECIPS[name][4]) 
								inv:set_stack('dst', 1, dst_its)
							else
								local item ={name=SMELTER_RECIPS[name][3], count=SMELTER_RECIPS[name][4], wear=0, metadata=""}
								dst_its:add_item(item)
								inv:set_stack('dst', 1, dst_its)
							end
							meta:set_int("eat_in_tick",0)
							meta:set_int("bonus",0)
							owl_tech.reset_item_redines(meta)
						end
					elseif recipt_true and  100/SMELTER_RECIPS[name][6]*12<=owl_tech:get_charge(meta) and owl_tech.get_item_redines(meta)==0 then --begin for work
						local amount=owl_tech.calculate_recipe_tire_bonus(meta,SMELTER_RECIPS[name][5],SMELTER_RECIPS[name][6])
						meta:set_int("eat_in_tick",((100/SMELTER_RECIPS[name][6]*12)/(100/amount)))
						meta:set_int("bonus",amount)
						local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
						owl_tech:set_charge(meta,steam_new)
						owl_tech.add_item_redines(meta,meta:get_int("bonus"))
					end

				else 
					owl_tech.reset_item_redines(meta)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_base_alloy_smelter(meta)
				timer:start(0.2)
			end
		})
		--Crafte 
		minetest.register_craft({
			type = "shaped",
			output ="owl_tech:"..table_all_machines[i][2].."_alloy_smelter",
			recipe = {
				{"owl_tech:"..table_all_machines[i][6].."_plate",table_all_machines[i][8][2],"owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:circuit_tire_"..i},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_plate"}
			}
		})
	end
	if table_all_machines[i][10] then
		local function set_formspect_base_sieve(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local redines = owl_tech.get_item_redines(meta)
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S( table_all_machines[i][1].." Siev"))).."]"..
			"list[context;input_in;2.5,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
			"list[context;dst;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"list[context;dst_add;6.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(6.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
			"listring[context;dst]"..
			"listring[context;dst_add]"..
			"listring[current_player;main]"..
			"listring[context;input_in]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		--siev
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_sieve", {
			description = S( table_all_machines[i][2].." sieve"),
			_doc_items_longdesc = S("Siev tire "..i),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_sieve_face.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,owl_tech_electro_mashine=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				owl_tech.reset_item_redines(meta)
				meta:set_int("eat_in_tick",0)
				meta:set_int("bonus",0)
				set_formspect_base_sieve(meta)
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("input_in", 1) 
				inv:set_size("dst", 1)
				inv:set_size("dst_add", 1)
				timer:start(0.2)
			end,
			on_timer = function(pos, elapsed)
				local timer = minetest.get_node_timer(pos)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				local input_its = inv:get_stack('input_in', 1)
				local dst_its = inv:get_stack('dst', 1)
				local dst_add_its = inv:get_stack('dst_add', 1)
				if not input_its:is_empty() then -- chek for item in slots  
					local name= string.sub(input_its:get_name(), 10)
					if SIEV_RESIPS[name]~=nil and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
						if owl_tech.can_siev_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
							local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
							owl_tech:set_charge(meta,steam_new)
							owl_tech.add_item_redines(meta,meta:get_int("bonus"))
						end
					elseif SIEV_RESIPS[name]~=nil and owl_tech.get_item_redines(meta)>=100 then
						if owl_tech.can_siev_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
							
							input_its:set_count(input_its:get_count()-1) --Remove item from input slot
							inv:set_stack('input_in', 1, input_its)
		
							if not dst_its:is_empty() then-- Add  main output 
								dst_its:set_count(dst_its:get_count()+SIEV_RESIPS[name][4]) 
								inv:set_stack('dst', 1, dst_its)
							else
								local item ={name=SIEV_RESIPS[name][3], count=SIEV_RESIPS[name][4], wear=0, metadata=""}
								dst_its:add_item(item)
								inv:set_stack('dst', 1, dst_its)
							end
		
							local random = math.random(1,100) --chans to get second  output
							if SIEV_RESIPS[name][6]>random then-- Add  second output if has 
								if not dst_add_its:is_empty() then
									dst_add_its:set_count(dst_add_its:get_count()+SIEV_RESIPS[name][8]) 
									inv:set_stack('dst_add', 1, dst_add_its)
								else
									local item ={name=SIEV_RESIPS[name][7], count=SIEV_RESIPS[name][8], wear=0, metadata=""}
									dst_add_its:add_item(item)
									inv:set_stack('dst_add', 1, dst_add_its)
								end
							end
							meta:set_int("eat_in_tick",0)
							meta:set_int("bonus",0)
							owl_tech.reset_item_redines(meta)
						end
					elseif SIEV_RESIPS[name]~=nil and  100/SIEV_RESIPS[name][5]*12<= owl_tech:get_charge(meta)  and owl_tech.get_item_redines(meta)==0 then --begin for work
						local amount=owl_tech.calculate_recipe_tire_bonus(meta,SIEV_RESIPS[name][2],SIEV_RESIPS[name][5])
						meta:set_int("eat_in_tick",((100/SIEV_RESIPS[name][5]*12)/(100/amount)))
						meta:set_int("bonus",amount)
						local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
						owl_tech:set_charge(meta,steam_new)
						owl_tech.add_item_redines(meta,meta:get_int("bonus"))
					end
				else 
					owl_tech.reset_item_redines(meta)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_base_sieve(meta)
				timer:start(0.2)
			end
		})
		--Crafte 
		minetest.register_craft({
			type = "shaped",
			output ="owl_tech:"..table_all_machines[i][2].."_sieve",
			recipe = {
				{"owl_tech:"..table_all_machines[i][6].."_plate",table_all_machines[i][8][3],"owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:circuit_tire_"..i},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:bronze_plate","owl_tech:"..table_all_machines[i][6].."_plate"}
			}
		})
	end
	if table_all_machines[i][11] then
		local function set_formspect_base_furnance(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local redines = owl_tech.get_item_redines(meta)
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Bronze furnance"))).."]"..
			"list[context;input_in;2.5,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
			"list[context;dst;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
			"listring[context;dst]"..
			"listring[current_player;main]"..
			"listring[context;input_in]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		--furnance
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_furnance", {
			description = S(table_all_machines[i][1].." furnance"),
			_doc_items_longdesc = S("Just a furnance - but use electro energy tire"..i),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_forge_face.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,owl_tech_electro_mashine=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				owl_tech.reset_item_redines(meta)
				set_formspect_base_furnance(meta)
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("input_in", 1) 
				inv:set_size("dst", 1)
				timer:start(0.2)
			end,
			on_timer = function(pos, elapsed)
				local timer = minetest.get_node_timer(pos)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				local input_its = inv:get_stack('input_in', 1)
				local dst_its = inv:get_stack('dst', 1)
				local aftercooked = minetest.get_craft_result({method = "cooking", width = 1, items = {input_its:get_name()}})
				if aftercooked.time>0  then
					if owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
						local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)
						owl_tech:set_charge(meta,steam_new)
						owl_tech.add_item_redines(meta,10)
					elseif  owl_tech.get_item_redines(meta)>=100 then
							
							input_its:set_count(input_its:get_count()-1) --Remove item from input slot
							inv:set_stack('input_in', 1, input_its)

							if not dst_its:is_empty() then-- Add  main output 
								dst_its:set_count(dst_its:get_count()+aftercooked.item:get_count()) 
								inv:set_stack('dst', 1, dst_its)
							else
								local item =aftercooked.item
								dst_its:add_item(item)
								inv:set_stack('dst', 1, dst_its)
							end
							local steam_new =owl_tech.get_pull_volume(meta,1)-10
							owl_tech.set_pull_volume(meta,1,steam_new)
							owl_tech.reset_item_redines(meta)
					elseif owl_tech:get_charge(meta)*10<= owl_tech:get_charge(meta) and owl_tech.get_item_redines(meta)==0 then --begin for work
						local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)
						owl_tech:set_charge(meta,steam_new)
						owl_tech.add_item_redines(meta,10)
					end
				else 
					owl_tech.reset_item_redines(meta)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_base_furnance(meta)
				timer:start(0.2)
			end
		})
		--Crafte 
		minetest.register_craft({
			type = "shaped",
			output ="owl_tech:"..table_all_machines[i][2].."_furnance",
			recipe = {
				{"owl_tech:"..table_all_machines[i][6].."_plate",table_all_machines[i][8][2],"owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:circuit_tire_"..i},
				{"owl_tech:"..table_all_machines[i][6].."_plate",table_all_machines[i][8][2],"owl_tech:"..table_all_machines[i][6].."_plate"}
			}
		})
	end
	if table_all_machines[i][12] then
		local function set_formspect_base_forge_hammer(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local redines = owl_tech.get_item_redines(meta)
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S(table_all_machines[i][2].." Forge Hammer"))).."]"..
			"list[context;input_in;2.5,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
			"list[context;dst;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"list[context;dst_add;6.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(6.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
			"listring[context;dst]"..
			"listring[context;dst_add]"..
			"listring[current_player;main]"..
			"listring[context;input_in]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		--froge hammer"
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_forge_hammer", {
			description = S(table_all_machines[i][1].." forge hammer"),
			_doc_items_longdesc = S("Automative forge tire"..i),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_forge_face.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,owl_tech_electro_mashine=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				owl_tech.reset_item_redines(meta)
				meta:set_int("eat_in_tick",0)
				meta:set_int("bonus",0)
				set_formspect_base_forge_hammer(meta)
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("input_in", 1) 
				inv:set_size("dst", 1)
				inv:set_size("dst_add", 1)
				timer:start(0.2)
			end,
			on_timer = function(pos, elapsed)
				local timer = minetest.get_node_timer(pos)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				local input_its = inv:get_stack('input_in', 1)
				local dst_its = inv:get_stack('dst', 1)
				local dst_add_its = inv:get_stack('dst_add', 1)
				if not input_its:is_empty() then -- chek for item in slots  
					local name= string.sub(input_its:get_name(), 10)
					if FORGE_HAMMER_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
						if owl_tech.can_forge_hammer_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
							local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
							owl_tech:set_charge(meta,steam_new)
							owl_tech.add_item_redines(meta,meta:get_int("bonus"))
						end
					elseif FORGE_HAMMER_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>=100 then
						if owl_tech.can_forge_hammer_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
							
							input_its:set_count(input_its:get_count()-1) --Remove item from input slot
							inv:set_stack('input_in', 1, input_its)
		
							if not dst_its:is_empty() then-- Add  main output 
								dst_its:set_count(dst_its:get_count()+FORGE_HAMMER_RECIPS[name][4]) 
								inv:set_stack('dst', 1, dst_its)
							else
								local item ={name=FORGE_HAMMER_RECIPS[name][3], count=FORGE_HAMMER_RECIPS[name][4], wear=0, metadata=""}
								dst_its:add_item(item)
								inv:set_stack('dst', 1, dst_its)
							end
		
							local random = math.random(1,100) --chans to get second  output
							if FORGE_HAMMER_RECIPS[name][6]>random then-- Add  second output if has 
								if not dst_add_its:is_empty() then
									dst_add_its:set_count(dst_add_its:get_count()+FORGE_HAMMER_RECIPS[name][8]) 
									inv:set_stack('dst_add', 1, dst_add_its)
								else
									local item ={name=FORGE_HAMMER_RECIPS[name][7], count=FORGE_HAMMER_RECIPS[name][8], wear=0, metadata=""}
									dst_add_its:add_item(item)
									inv:set_stack('dst_add', 1, dst_add_its)
								end
							end
							meta:set_int("eat_in_tick",0)
							meta:set_int("bonus",0)
							owl_tech.reset_item_redines(meta)
						end
					elseif FORGE_HAMMER_RECIPS[name]~=nil and  100/FORGE_HAMMER_RECIPS[name][5]*12<= owl_tech:get_charge(meta)  and owl_tech.get_item_redines(meta)==0 then --begin for work
						local amount=owl_tech.calculate_recipe_tire_bonus(meta,FORGE_HAMMER_RECIPS[name][2],FORGE_HAMMER_RECIPS[name][5])
						meta:set_int("eat_in_tick",((100/FORGE_HAMMER_RECIPS[name][5]*12)/(100/amount)))
						meta:set_int("bonus",amount)
						local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
						owl_tech:set_charge(meta,steam_new)
						owl_tech.add_item_redines(meta,meta:get_int("bonus"))
					end
				else 
					owl_tech.reset_item_redines(meta)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_base_forge_hammer(meta)
				timer:start(0.2)
			end
		})
		--Crafte
		minetest.register_craft({
			type = "shaped",
			output ="owl_tech:"..table_all_machines[i][2].."_forge_hammer",
			recipe = {
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_plate"},
				{table_all_machines[i][8][4],"owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:circuit_tire_"..i},
				{"owl_tech:"..table_all_machines[i][6].."_plate","mcl_anvils:anvil","owl_tech:"..table_all_machines[i][6].."_plate"}
			}
		})
	end
	if table_all_machines[i][13] then
		local function set_formspect_steam_vertical_miner(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local redines = owl_tech.get_item_redines(meta)
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S(table_all_machines[i][1].."Vertical miner"))).."]"..
			"list[context;dst;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..			"listring[context;dst]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_vertical_miner", {
			description = S(table_all_machines[i][1].." vertical miner"),
			_doc_items_longdesc = S("Dig the hole"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_vertical_miner_face.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_vertical_miner_face.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_vertical_miner_face.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_vertical_miner_face.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,dst=1,owl_tech_electro_mashine=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1)
				meta:set_int("work_delay",0)
				set_formspect_steam_vertical_miner(meta)
				owl_tech.prepear_for_verticak_steam_miner(pos)
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("dst", 1)
				timer:start(2)
			end,
			on_timer = function(pos, elapsed)
				local meta = minetest.get_meta(pos)
				local timer = minetest.get_node_timer(pos)
				if meta:get_int("get_bedrock")==0 and 
				owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) and meta:get_int("work_delay")==0 then
					local inv = meta:get_inventory()
					local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)
					owl_tech:set_charge(meta,steam_new)
					owl_tech.vertical_steam_mine(pos,meta,owl_tech.can_vertical_steam_miner_mine_this(pos,meta))
					meta:set_int("work_delay",table_all_machines[i][14])	
				end
				if meta:get_int("work_delay")>0 then
					meta:set_int("work_delay",meta:get_int("work_delay")-1)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_steam_vertical_miner(meta)
				timer:start(0.2)
			end,
			on_destruct = function(pos)
				owl_tech.remove_all_pipe_vertical(pos)
			end
		})
		--Crafte "owl_tech:steam_vertical_miner",
		minetest.register_craft({
			type = "shaped",
			output ="owl_tech:"..table_all_machines[i][2].."_vertical_miner",
			recipe = {
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:iron_item_pipe_input","owl_tech:bronze_fluid_pipe","owl_tech:iron_item_pipe_input"}
			}
		})
	end
	if table_all_machines[i][15]then
		local function set_formspect_steam_quarry(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local delay = meta:get_int("work_delay")
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Steam vertical miner"))).."]"..
			"list[context;dst;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2.5;"..minetest.formspec_escape(minetest.colorize("#313131", (delay.."--delay"))).."]"..
			"listring[context;dst]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_quarry", {
			description = S(table_all_machines[i][1].." quarry"),
			_doc_items_longdesc = S("Dig the Hole"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_quary.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_quary.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_quary.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_quary.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,dst=1,owl_tech_electro_mashine=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				meta:set_int("work_delay",0)
				set_formspect_steam_quarry(meta)
				owl_tech.prepear_for_verticak_steam_miner(pos)
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("dst", 1)
				timer:start(2)
			end,
			on_timer = function(pos, elapsed)
				local meta = minetest.get_meta(pos)
				local timer = minetest.get_node_timer(pos)
				if meta:get_int("min_x")==0 and meta:get_int("min_z")==0 then
					owl_tech.scan_for_point(pos,meta,table_all_machines[i][16])
				end
				if meta:get_int("get_bedrock")==0 and meta:get_int("work_delay")==0 and 
				owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then
					local inv = meta:get_inventory()
					local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)
					owl_tech:set_charge(meta,steam_new)
					local can_mine = owl_tech.can_quary_mine_this(pos,meta)
					owl_tech.diging_quary(pos,meta,can_mine)
					meta:set_int("work_delay",table_all_machines[i][14])
				end
				if meta:get_int("work_delay")>0 then
					meta:set_int("work_delay",meta:get_int("work_delay")-1)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_steam_quarry(meta)
				timer:start(0.2)
			end
		})
		--Crafte controller 
		minetest.register_craft({
			type = "shaped",
			output = "owl_tech:"..table_all_machines[i][2].."_quarry",
			recipe = {
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:"..table_all_machines[i][6].."_plate"},
				{table_all_machines[i][8][1],"owl_tech:circuit_tire_"..i,table_all_machines[i][8][1],}
			}
		})
	end
	if table_all_machines[i][17] then
		local function set_formspect_wood_planter(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local delay = meta:get_int("work_delay")
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S(table_all_machines[i][1].."wood planter"))).."]"..
			"list[context;it_sapling;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2.5;"..minetest.formspec_escape(minetest.colorize("#313131", (delay.."--delay"))).."]"..
			"listring[context;it_sapling]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_wood_planter", {
			description = S(table_all_machines[i][1].." tree planter"),
			_doc_items_longdesc = S("Dig the Hole"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128^owl_tech_steam_input.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_sapling.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_sapling.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_sapling.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_sapling.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,owl_tech_electro_mashine=1,it_sapling=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				meta:set_int("work_delay",0)
				set_formspect_wood_planter(meta)
				owl_tech.prepear_for_sapling_plant(pos,table_all_machines[i][19])
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("it_sapling", 1)
				timer:start(2)
			end,
			on_timer = function(pos, elapsed)
				local meta = minetest.get_meta(pos)
				local timer = minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				local fluid_its = inv:get_stack('it_sapling', 1)
				if  meta:get_int("work_delay")==0 and owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta)/16
				and minetest.get_item_group((fluid_its:get_name()),"sapling")>0 then
					owl_tech.look_for_place_new_sapling(pos,meta)
					meta:set_int("work_delay",table_all_machines[i][14])
				end
				if meta:get_int("work_delay")>0 then
					meta:set_int("work_delay",meta:get_int("work_delay")-1)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_wood_planter(meta)
				timer:start(0.2)
			end
		})
		--Crafte controller 
		minetest.register_craft({
			type = "shaped",
			output = "owl_tech:"..table_all_machines[i][2].."_wood_planter",
			recipe = {
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:"..table_all_machines[i][6].."_plate"},
				{table_all_machines[i][8][1],"owl_tech:circuit_tire_"..i,table_all_machines[i][8][1],}
			}
		})
	end
	if table_all_machines[i][18] then
		local function set_formspect_wood_chopper(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local delay = meta:get_int("work_delay")
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S(table_all_machines[i][1].." wood chopper"))).."]"..
			"list[context;wood;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..			
			"list[context;leaves;5.5,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,2.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2.5;"..minetest.formspec_escape(minetest.colorize("#313131", (delay.."--delay"))).."]"..
			"listring[context;wood]"..
			"listring[context;leaves]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_wood_chopper", {
			description = S(table_all_machines[i][1].." tree chopper"),
			_doc_items_longdesc = S("Chopp all tress in smal radius"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_tool_steelaxe.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_tool_steelaxe.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_tool_steelaxe.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,owl_tech_electro_mashine=1,wood=1,leaves=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				set_formspect_wood_chopper(meta)
				owl_tech.prepear_for_sapling_plant(pos,table_all_machines[i][19])
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("wood", 1)
				inv:set_size("leaves", 1)  
				timer:start(2)
			end,
			on_timer = function(pos, elapsed)
				local meta = minetest.get_meta(pos)
				local timer = minetest.get_node_timer(pos)
				if  owl_tech:get_charge(meta)>=(owl_tech:get_voltage(meta)/16 ) then
					local steam_new =owl_tech:get_charge(meta)-owl_tech:get_voltage(meta)/16
					owl_tech:set_charge(meta,steam_new)
					owl_tech:look_for_trees(pos,meta)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_wood_chopper(meta)
				timer:start(0.2)
			end
		})
		--Crafte controller 
		minetest.register_craft({
			type = "shaped",
			output = "owl_tech:"..table_all_machines[i][2].."_wood_chopper",
			recipe = {
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:"..table_all_machines[i][6].."_plate"},
				{table_all_machines[i][8][5],"owl_tech:circuit_tire_"..i,table_all_machines[i][8][5],}
			}
		})
	end
	if table_all_machines[i][20] then
		local function set_formspect_seed_planter(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local delay = meta:get_int("work_delay")
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S(table_all_machines[i][1].."seed planter"))).."]"..
			"list[context;it_sapling;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2.5;"..minetest.formspec_escape(minetest.colorize("#313131", (delay.."--delay"))).."]"..
			"listring[context;it_sapling]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_seed_planter", {
			description = S(table_all_machines[i][1].." seed planter"),
			_doc_items_longdesc = S("Plant seed"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128^mcl_farming_wheat_seeds.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^mcl_farming_wheat_seeds.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^mcl_farming_wheat_seeds.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^mcl_farming_wheat_seeds.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^mcl_farming_wheat_seeds.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,owl_tech_electro_mashine=1,it_sapling=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				meta:set_int("work_delay",0)
				set_formspect_seed_planter(meta)
				owl_tech.prepear_for_sapling_plant(pos,table_all_machines[i][19])
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("it_sapling", 1)
				timer:start(2)
			end,
			on_timer = function(pos, elapsed)
				local meta = minetest.get_meta(pos)
				local timer = minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				local fluid_its = inv:get_stack('it_sapling', 1)
				local name_seed =string.gsub(fluid_its:get_name(),"owl_tech:","")
				name_seed = string.gsub(name_seed,"mcl_farming:","")
				if  meta:get_int("work_delay")==0 and owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta)/16 
				and CROPS_VALID[name_seed]~=nil and CROPS_VALID[name_seed]~="" then
					owl_tech:look_for_place_new_sids(pos,meta)
					meta:set_int("work_delay",table_all_machines[i][14])
				end
				if meta:get_int("work_delay")>0 then
					meta:set_int("work_delay",meta:get_int("work_delay")-1)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_seed_planter(meta)
				timer:start(0.2)
			end
		})
		--Crafte controller 
		minetest.register_craft({
			type = "shaped",
			output = "owl_tech:"..table_all_machines[i][2].."_wood_planter",
			recipe = {
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:"..table_all_machines[i][6].."_plate"},
				{table_all_machines[i][8][4],"owl_tech:circuit_tire_"..i,table_all_machines[i][8][4],}
			}
		})
	end
	if table_all_machines[i][21] then
		local function set_formspect_harveste(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local delay = meta:get_int("work_delay")
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S(table_all_machines[i][1].." harvester"))).."]"..
			"list[context;wood;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..			
			"list[context;leaves;5.5,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,2.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2.5;"..minetest.formspec_escape(minetest.colorize("#313131", (delay.."--delay"))).."]"..
			"listring[context;wood]"..
			"listring[context;leaves]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_harvester", {
			description = S(table_all_machines[i][1].." hervester"),
			_doc_items_longdesc = S("Harvest all redy plants"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_tool_shears.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_tool_shears.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^default_tool_shears.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,owl_tech_electro_mashine=1,wood=1,leaves=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				set_formspect_harveste(meta)
				owl_tech.prepear_for_sapling_plant(pos,table_all_machines[i][19])
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("wood", 1)
				inv:set_size("leaves", 1)  
				timer:start(2)
			end,
			on_timer = function(pos, elapsed)
				local meta = minetest.get_meta(pos)
				local timer = minetest.get_node_timer(pos)
				if  owl_tech:get_charge(meta)>=(owl_tech:get_voltage(meta)/16 ) then
					owl_tech:look_for_plants(pos)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_harveste(meta)
				timer:start(0.2)
			end
		})
		--Crafte controller 
		minetest.register_craft({
			type = "shaped",
			output = "owl_tech:"..table_all_machines[i][2].."_harvester",
			recipe = {
				{"owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:circuit_tire_"..i,},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:"..table_all_machines[i][6].."_plate"},
				{table_all_machines[i][8][5],"owl_tech:circuit_tire_"..i,table_all_machines[i][8][5],}
			}
		})
	end
	if table_all_machines[i][22] then
		--Mixer
		local function set_formspect_base_alloy_smelter(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local redines = owl_tech.get_item_redines(meta)
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Steam alloy smelter"))).."]"..
			"list[context;input_in;2.5,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
			"list[context;input_in_add;4,2.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(4,2.5,1,1)..
			"list[context;dst;5.5,1.5;1,1;]"..
			mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
			"listring[context;dst]"..
			"listring[current_player;main]"..
			"listring[context;input_in]"..
			"listring[context;input_in_add]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		-------------------------------------------
		--mixer
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_mixer", {
			description = S( table_all_machines[i][1].." mixer"),
			_doc_items_longdesc = S("Mixes the dust"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_mixer.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,owl_tech_electro_mashine=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1)
				owl_tech.reset_item_redines(meta)
				meta:set_int("eat_in_tick",0)
				meta:set_int("bonus",0)
				set_formspect_base_alloy_smelter(meta)
				local timer =minetest.get_node_timer(pos)
				local inv = meta:get_inventory()
				inv:set_size("input_in", 1) 
				inv:set_size("input_in_add", 1) 
				inv:set_size("dst", 1)
				timer:start(0.2)
			end,
			on_timer = function(pos, elapsed)
				local timer = minetest.get_node_timer(pos)
				local meta = minetest.get_meta(pos)
				local inv = meta:get_inventory()
				local input_its = inv:get_stack('input_in', 1)
				local input_its_add = inv:get_stack('input_in_add', 1)
				local dst_its = inv:get_stack('dst', 1)
				if not input_its:is_empty() then -- chek for item in slots  
					local name1= string.sub(input_its:get_name(), 10)
					local name2 = string.sub(input_its_add:get_name(), 10)
					local name =""
					local recipt_true = false 
					if MIXER_RESIPS[name1..name2]~=nil then
						recipt_true= true
						name=name1..name2
					elseif MIXER_RESIPS[name2..name1]~=nil then
						recipt_true= true
						name=name2..name1
					end
					if recipt_true and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
						if owl_tech.can_mixer_recips_recips_go_in_this_mashien(meta,name,dst_its)  then-- chek for all itemslot and machine tire
							local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
							owl_tech:set_charge(meta,steam_new)
							owl_tech.add_item_redines(meta,meta:get_int("bonus"))
						end
					elseif recipt_true and owl_tech.get_item_redines(meta)>=100 then
						if owl_tech.can_mixer_recips_recips_go_in_this_mashien(meta,name,dst_its)   then-- chek for all itemslot and machine tire
							input_its:set_count(input_its:get_count()-MIXER_RESIPS[name][1]) --Remove item from input slot
							inv:set_stack('input_in', 1, input_its)
							input_its_add:set_count(input_its_add:get_count()-MIXER_RESIPS[name][2]) --Remove item from input slot
							inv:set_stack('input_in_add', 1, input_its_add )

							if not dst_its:is_empty() then-- Add  main output 
								dst_its:set_count(dst_its:get_count()+MIXER_RESIPS[name][4]) 
								inv:set_stack('dst', 1, dst_its)
							else
								local item ={name=MIXER_RESIPS[name][3], count=MIXER_RESIPS[name][4], wear=0, metadata=""}
								dst_its:add_item(item)
								inv:set_stack('dst', 1, dst_its)
							end
							meta:set_int("eat_in_tick",0)
							meta:set_int("bonus",0)
							owl_tech.reset_item_redines(meta)
						end
					elseif recipt_true and  100/MIXER_RESIPS[name][6]*12<=owl_tech:get_charge(meta) and owl_tech.get_item_redines(meta)==0 then --begin for work
						local amount=owl_tech.calculate_recipe_tire_bonus(meta,MIXER_RESIPS[name][5],MIXER_RESIPS[name][6])
						meta:set_int("eat_in_tick",((100/MIXER_RESIPS[name][6]*12)/(100/amount)))
						meta:set_int("bonus",amount)
						local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
						owl_tech:set_charge(meta,steam_new)
						owl_tech.add_item_redines(meta,meta:get_int("bonus"))
					end

				else 
					owl_tech.reset_item_redines(meta)
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_base_alloy_smelter(meta)
				timer:start(0.2)
			end
		})
		--Crafte controller 
		minetest.register_craft({
		type = "shaped",
		output = "owl_tech:"..table_all_machines[i][2].."_mixer",
		recipe = {
			{"owl_tech:"..table_all_machines[i][6].."_plate",table_all_machines[i][8][5],"owl_tech:"..table_all_machines[i][6].."_plate"},
			{"owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:circuit_tire_"..i},
			{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_plate"}
		}
		})
	end
	if  table_all_machines[i][23]then
		local function set_formspect_base_fluid_separator(meta)
			local max_charge = owl_tech:get_charge_max(meta) 
			local curent_charge = owl_tech:get_charge(meta)
			local redines = owl_tech.get_item_redines(meta)
			local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
			local fluid_2_name = owl_tech.get_pull_fluid_name(meta,2)
			local fluid_3_name = owl_tech.get_pull_fluid_name(meta,3)
			local fluid_1_volume = owl_tech.get_pull_volume(meta,1)	
			local fluid_2_volume = owl_tech.get_pull_volume(meta,2)
			local fluid_3_volume = owl_tech.get_pull_volume(meta,3)
			local formspec = "size[9,8.75]"..
			"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
			"list[current_player;main;0,4.5;9,3;9]"..
			mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
			"list[current_player;main;0,7.74;9,1;]"..
			mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
			"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S( table_all_machines[i][1].." fluid separator"))).."]"..
			"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
			"label[0,3;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_2_name.."--"..fluid_2_volume))).."]"..
			"label[4,2;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_3_name.."--"..fluid_3_volume))).."]"..
			"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (max_charge.."--"..curent_charge.."--"..table_all_machines[i][3]))).."]"..
			"label[8,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"..
			"listring[current_player;main]"
			meta:set_string("formspec", formspec)
		end
		
		--base macerator
		minetest.register_node("owl_tech:"..table_all_machines[i][2].."_fluid_separtor", {
			description = S(table_all_machines[i][1].."fluid separator"),
			_doc_items_longdesc = S("Separate fluid and gas (tire"..i..") ,main output - botom ,second output - left and right"),
			tiles = {
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_input.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_steam_output.png",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128^owl_tech_steam_output.png",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128^owl_tech_steam_output.png",
				"owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128^owl_tech_steam_input.png",
				"(owl_tech_base_meshanism_side.png^[colorize:"..table_all_machines[i][5]..":128)^owl_tech_macerator_face.png",
			},
			is_ground_content = false,
			stack_max = 64,
			groups = {pickaxey=2, owl_tech_machine=1,dst=1,dst_add=1,owl_tech_electro_mashine=1},
			sounds = mcl_sounds.node_sound_metal_defaults(),
			paramtype2 = "facedir",
			_mcl_blast_resistance = 6,
			_mcl_hardness = 5,
			on_construct = function(pos)
				local meta = minetest.get_meta(pos)
				owl_tech:add_electro(pos,table_all_machines[i][3],table_all_machines[i][4])
				owl_tech.set_mashine_tire(meta,i+1) --all steam mishine has 1 tire
				owl_tech.reset_item_redines(meta)
				meta:set_int("eat_in_tick",0)
				meta:set_int("bonus",0)
				owl_tech.add_new_pull(meta,table_all_machines[i][4],1,0) --1 input
				owl_tech.add_new_pull(meta,table_all_machines[i][4],0,1) --2 output
				owl_tech.add_new_pull(meta,table_all_machines[i][4],0,1) --3 add output
				set_formspect_base_fluid_separator(meta)
				local timer =minetest.get_node_timer(pos)
				timer:start(0.2)
			end,
			on_timer = function(pos, elapsed)
				local timer = minetest.get_node_timer(pos)
				local meta = minetest.get_meta(pos)

				local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
				local fluid_2_name = owl_tech.get_pull_fluid_name(meta,2)
				local fluid_3_name = owl_tech.get_pull_fluid_name(meta,3)
				local fluid_1_volume = owl_tech.get_pull_volume(meta,1)	
				local fluid_2_volume = owl_tech.get_pull_volume(meta,2)
				local fluid_3_volume = owl_tech.get_pull_volume(meta,3)

				if fluid_1_name~="none" then -- chek for item in slots  
					local name =string.sub(fluid_1_name, 10)
					if FLUID_SEPARATOR[name]~=nil and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
						if owl_tech.can_fluid_separator_recips_go_in_this_mashien(meta,name,fluid_1_volume,fluid_2_name,fluid_2_volume,fluid_3_name,fluid_3_volume,table_all_machines[i][4],table_all_machines[i][4])  then-- chek for all itemslot and machine tire
							local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
							owl_tech:set_charge(meta,steam_new)
							owl_tech.add_item_redines(meta,meta:get_int("bonus"))
						end
					elseif FLUID_SEPARATOR[name]~=nil and owl_tech.get_item_redines(meta)>=100 then
						if owl_tech.can_fluid_separator_recips_go_in_this_mashien(meta,name,fluid_1_volume,fluid_2_name,fluid_2_volume,fluid_3_name,fluid_3_volume,table_all_machines[i][4],table_all_machines[i][4])  then-- chek for all itemslot and machine tire
							owl_tech.set_pull_volume(meta,1,fluid_1_volume-FLUID_SEPARATOR[name][1])
							if fluid_2_name~='none' then--main fluid 
								owl_tech.set_pull_volume(meta,2,fluid_2_volume+FLUID_SEPARATOR[name][4])
							else
								owl_tech.set_pull_volume(meta,2,fluid_2_volume+FLUID_SEPARATOR[name][4])
								owl_tech.set_pull_fluid_name(meta,2,FLUID_SEPARATOR[name][3])
							end
							local random = math.random(1,100) 
							if FLUID_SEPARATOR[name][6]>random then --second output
								if fluid_3_name~='none' then 
									owl_tech.set_pull_volume(meta,3,fluid_3_volume+FLUID_SEPARATOR[name][8])
								else
									owl_tech.set_pull_volume(meta,3,fluid_3_volume+FLUID_SEPARATOR[name][8])
									owl_tech.set_pull_fluid_name(meta,3,FLUID_SEPARATOR[name][7])
								end
							end
							meta:set_int("eat_in_tick",0)
							meta:set_int("bonus",0)
							owl_tech.reset_item_redines(meta)
						end
					elseif FLUID_SEPARATOR[name]~=nil and  100/FLUID_SEPARATOR[name][5]*12<= owl_tech:get_charge(meta)  and owl_tech.get_item_redines(meta)==0 then --begin for work
						local amount=owl_tech.calculate_recipe_tire_bonus(meta,FLUID_SEPARATOR[name][2],FLUID_SEPARATOR[name][5])
						meta:set_int("eat_in_tick",((100/FLUID_SEPARATOR[name][5]*12)/(100/amount)))
						meta:set_int("bonus",amount)
						local steam_new =owl_tech:get_charge(meta)-meta:get_int("eat_in_tick")
						owl_tech:set_charge(meta,steam_new)
						owl_tech.add_item_redines(meta,meta:get_int("bonus"))
					end
				else 
					owl_tech.reset_item_redines(meta)
				end

				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"owl_tech_electro_wire")>0
				and owl_tech:get_charge_max(meta)-owl_tech:get_charge(meta)>=owl_tech:get_voltage(meta) then --get from wire electro
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y+1,z=pos.z})
					if owl_tech:get_charge(meta_up)>0 and owl_tech:get_voltage(meta)==owl_tech:get_voltage(meta_up) then
						owl_tech.send_electro_from_wire_in_pos(meta_up,pos)
					end
				end
				local rotation = minetest.get_node(pos).param2 % 32 --get rotattion ( return int )
				if rotation==0 then --z+1
					if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+1})).name,"owl_tech_fluid_pipe") then --get from pipe steam
						local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+1})
						local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,1)
						local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
						local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
						if can_do and can_do2 and inde_pull==1 then
							owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
							owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
						end
					end
					for i = -1, 1, 2 do
						if minetest.get_item_group((minetest.get_node({x=pos.x+i,y=pos.y,z=pos.z})).name,"owl_tech_fluid_pipe") then --sand in pipe
							local meta_up = minetest.get_meta({x=pos.x+i,y=pos.y,z=pos.z})
							local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,2)
							local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
							local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
							if can_do and can_do2 then
								owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
								local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
								local difer = pull_curent_volume-remove_amount
								owl_tech.set_pull_volume(meta,2,difer)
							end
						end
					end
				end
				if rotation==2 then --z-1
					if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z-1})).name,"owl_tech_fluid_pipe") then --get from pipe steam
						local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z-1})
						local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,1)
						local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
						local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
						if can_do and can_do2  then
							owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
							owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
						end
					end
					for i = -1, 1, 2 do
						if minetest.get_item_group((minetest.get_node({x=pos.x+i,y=pos.y,z=pos.z})).name,"owl_tech_fluid_pipe") then --sand in pipe
							local meta_up = minetest.get_meta({x=pos.x+i,y=pos.y,z=pos.z})
							local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,2)
							local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
							local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
							if can_do and can_do2  then
								owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
								local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
								local difer = pull_curent_volume-remove_amount
								owl_tech.set_pull_volume(meta,2,difer)
							end
						end
					end
				end
				if rotation==3 then --x+1
					if minetest.get_item_group((minetest.get_node({x=pos.x-1,y=pos.y,z=pos.z})).name,"owl_tech_fluid_pipe") then --get from pipe steam
						local meta_up = minetest.get_meta({x=pos.x-1,y=pos.y,z=pos.z})
						local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,2)
						local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
						local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
						if can_do and can_do2 and inde_pull==1 then
							owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
							owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
						end
					end
					for i = -1, 1, 2 do
						if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+i})).name,"owl_tech_fluid_pipe") then --sand in pipe
							local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+i})
							local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,2)
							local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
							local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
							if can_do and can_do2 then
								owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
								local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
								local difer = pull_curent_volume-remove_amount
								owl_tech.set_pull_volume(meta,2,difer)
							end
						end
					end
				end
				if rotation==1 then --x-1
					if minetest.get_item_group((minetest.get_node({x=pos.x+1,y=pos.y,z=pos.z})).name,"owl_tech_fluid_pipe") then --get from pipe steam
						local meta_up = minetest.get_meta({x=pos.x+1,y=pos.y,z=pos.z})
						local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta_up,1)
						local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta))
						local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
						if can_do and can_do2 and inde_pull==1 then
							owl_tech.add_fluid_in_node_pull(meta,fluid_name_to_send,remove_amount,inde_pull)
							owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
						end
					end
					for i = -1, 1, 2 do
						if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y,z=pos.z+i})).name,"owl_tech_fluid_pipe")>0 then --sand in pipe
							local meta_up = minetest.get_meta({x=pos.x,y=pos.y,z=pos.z+i})
							local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,2)
							local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
							local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
							if can_do and can_do2 then
								owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
								local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
								local difer = pull_curent_volume-remove_amount
								owl_tech.set_pull_volume(meta,2,difer)
							end
						end
					end
				end
				if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"owl_tech_fluid_pipe") then --sand in pipe
					local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
					local fluid_name_to_send = owl_tech.get_pull_fluid_name(meta,3)
					local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta_up,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
					local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta,fluid_name_to_send,owl_tech.get_fluid_sand_in_tick(meta_up))
					if can_do and can_do2 then
						owl_tech.add_fluid_in_node_pull(meta_up,fluid_name_to_send,remove_amount,inde_pull)
						local pull_curent_volume = owl_tech.get_pull_volume(meta,1)
						local difer = pull_curent_volume-remove_amount
						owl_tech.set_pull_volume(meta,1,difer)
					end
				end
				owl_tech.delit_name_fluid_if_0(meta)
				meta:set_string("infotext",owl_tech:get_charge(meta))
				set_formspect_base_fluid_separator(meta)
				timer:start(0.2)
			end
		})
		--Crafte 
		minetest.register_craft({
			type = "shaped",
			output = "owl_tech:"..table_all_machines[i][2].."_fluid_separtor",
			recipe = {
				{table_all_machines[i][8][2],table_all_machines[i][8][2],table_all_machines[i][8][2]},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:"..table_all_machines[i][6].."_frames","owl_tech:"..table_all_machines[i][6].."_plate"},
				{"owl_tech:"..table_all_machines[i][6].."_plate","owl_tech:circuit_tire_"..i,"owl_tech:"..table_all_machines[i][6].."_plate"}
			}
		})
	end
end