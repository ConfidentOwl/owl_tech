local S = minetest.get_translator(minetest.get_current_modname())
local name = minetest.get_current_modname()
local path = minetest.get_modpath(name)

--Bronze mashins
local function set_formspect_base_macerator(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
	local redines = owl_tech.get_item_redines(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Stean macerator"))).."]"..
	"list[context;input_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;dst;5.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
	"list[context;dst_add;6.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(6.5,1.5,1,1)..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"listring[context;dst]"..
	"listring[context;dst_add]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end

--bronze_macerator"
minetest.register_node("owl_tech:bronze_macerator", {
	description = S("Bronze macerator"),
	_doc_items_longdesc = S("Macerate dust"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_macerator_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,dst=1,dst_add=1,input_in=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,8000,1,0) --steam  eat 25 steam in 1 tick
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,1) --all steam mishine has 1 tire
		owl_tech.reset_item_redines(meta)
		set_formspect_base_macerator(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("input_in", 1) 
		inv:set_size("dst", 1)
		inv:set_size("dst_add", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local dst_its = inv:get_stack('dst', 1)
		local dst_add_its = inv:get_stack('dst_add', 1)
        if not input_its:is_empty() then -- chek for item in slots  
			local name =""
			if "mcl_mangrove:mangroveleaves"==input_its:get_name() then
				name= "mangroveleaves"
			else
				name= string.sub(input_its:get_name(), 10)
			end
			if MACERATO_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
				if owl_tech.can_macaerat_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					local amount=owl_tech.calculate_recipe_tire_bonus(meta,MACERATO_RECIPS[name][2],MACERATO_RECIPS[name][5])
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.add_item_redines(meta,amount)
				end
			elseif MACERATO_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>=100 then
				if owl_tech.can_macaerat_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					
					input_its:set_count(input_its:get_count()-1) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)

					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+MACERATO_RECIPS[name][4]) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item ={name=MACERATO_RECIPS[name][3], count=MACERATO_RECIPS[name][4], wear=0, metadata=""}
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end

					local random = math.random(1,100) --chans to get second  output
					if MACERATO_RECIPS[name][6]>random then-- Add  second output if has 
						if not dst_add_its:is_empty() then
							dst_add_its:set_count(dst_add_its:get_count()+MACERATO_RECIPS[name][8]) 
							inv:set_stack('dst_add', 1, dst_add_its)
						else
							local item ={name=MACERATO_RECIPS[name][7], count=MACERATO_RECIPS[name][8], wear=0, metadata=""}
							dst_add_its:add_item(item)
							inv:set_stack('dst_add', 1, dst_add_its)
						end
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
				end
			elseif MACERATO_RECIPS[name]~=nil and  100/MACERATO_RECIPS[name][5]*25<= owl_tech.get_pull_volume(meta,1)  and owl_tech.get_item_redines(meta)==0 then --begin for work
				local amount=owl_tech.calculate_recipe_tire_bonus(meta,MACERATO_RECIPS[name][2],MACERATO_RECIPS[name][5])
				local steam_new =owl_tech.get_pull_volume(meta,1)-25
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,amount)
			end
        else 
			owl_tech.reset_item_redines(meta)
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_base_macerator(meta)
		timer:start(0.2)
	end
})
--Crafte "owl_tech:bronze_macerator"
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:bronze_macerator",
    recipe = {
        {"mcl_core:flint", "mcl_core:flint", "mcl_core:flint"},
        {"owl_tech:bronze_plate","owl_tech:bronze_frames","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"}
    }
})
--alloy smelter
local function set_formspect_base_alloy_smelter(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
	local redines = owl_tech.get_item_redines(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Steam alloy smelter"))).."]"..
	"list[context;input_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;input_in_add;4,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(4,2.5,1,1)..
	"list[context;dst;5.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"listring[context;dst]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[context;input_in_add]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end
-------------------------------------------
--bronze_alloy_smelter
minetest.register_node("owl_tech:bronze_alloy_smelter", {
	description = S("Bronze alloy smelter"),
	_doc_items_longdesc = S("Smelt alll"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_smelter_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,input_in=1,input_in_add=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,8000,1,0) --steam  eat 25 steam in 1 tick
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,1) --all steam mishine has 1 tire
		owl_tech.reset_item_redines(meta)
		set_formspect_base_alloy_smelter(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("input_in", 1) 
		inv:set_size("input_in_add", 1) 
		inv:set_size("dst", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local input_its_add = inv:get_stack('input_in_add', 1)
		local dst_its = inv:get_stack('dst', 1)
        if not input_its:is_empty() then -- chek for item in slots  
            local name1= string.sub(input_its:get_name(), 10)
			local name2 = string.sub(input_its_add:get_name(), 10)
			local name =""
			local recipt_true = false 
			if SMELTER_RECIPS[name1..name2]~=nil then
				recipt_true= true
				name=name1..name2
			elseif SMELTER_RECIPS[name2..name1]~=nil then
				recipt_true= true
				name=name2..name1
			end
			if recipt_true and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real

				if owl_tech.can_smelter_recips_recips_go_in_this_mashien(meta,name,dst_its)  then-- chek for all itemslot and machine tire

					local amount=owl_tech.calculate_recipe_tire_bonus(meta,SMELTER_RECIPS[name][5],SMELTER_RECIPS[name][6])
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.add_item_redines(meta,amount)
				end
			elseif recipt_true and owl_tech.get_item_redines(meta)>=100 then
				if owl_tech.can_smelter_recips_recips_go_in_this_mashien(meta,name,dst_its)   then-- chek for all itemslot and machine tire
					input_its:set_count(input_its:get_count()-SMELTER_RECIPS[name][1]) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)
					input_its_add:set_count(input_its_add:get_count()-SMELTER_RECIPS[name][2]) --Remove item from input slot
					inv:set_stack('input_in_add', 1, input_its_add )

					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+SMELTER_RECIPS[name][4]) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item ={name=SMELTER_RECIPS[name][3], count=SMELTER_RECIPS[name][4], wear=0, metadata=""}
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
				end
			elseif recipt_true and  100/SMELTER_RECIPS[name][6]*25<= owl_tech.get_pull_volume(meta,1)  and owl_tech.get_item_redines(meta)==0 then --begin for work
				
				local amount=owl_tech.calculate_recipe_tire_bonus(meta,SMELTER_RECIPS[name][2],SMELTER_RECIPS[name][5])
				local steam_new =owl_tech.get_pull_volume(meta,1)-25
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,amount)
			end
        else 
			owl_tech.reset_item_redines(meta)
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_base_alloy_smelter(meta)
		timer:start(0.2)
	end
})
--Crafte "owl_tech:bronze_alloy_smelter"
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:bronze_alloy_smelter",
    recipe = {
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"},
        {"mcl_furnaces:furnace","owl_tech:bronze_frames","mcl_furnaces:furnace"},
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"}
    }
})
-------------------------------------------
--base sieve
local function set_formspect_base_sieve(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
	local redines = owl_tech.get_item_redines(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Stean sieve"))).."]"..
	"list[context;input_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;dst;5.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
	"list[context;dst_add;6.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(6.5,1.5,1,1)..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"listring[context;dst]"..
	"listring[context;dst_add]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end
--bronze_sieve"
minetest.register_node("owl_tech:bronze_sieve", {
	description = S("Bronze sieve"),
	_doc_items_longdesc = S("sieve alls"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_sieve_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,input_in=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,8000,1,0) --steam  eat 25 steam in 1 tick
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,1) --all steam mishine has 1 tire
		owl_tech.reset_item_redines(meta)
		set_formspect_base_sieve(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("input_in", 1) 
		inv:set_size("dst", 1)
		inv:set_size("dst_add", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local dst_its = inv:get_stack('dst', 1)
		local dst_add_its = inv:get_stack('dst_add', 1)
        if not input_its:is_empty() then -- chek for item in slots  
            local name= string.sub(input_its:get_name(), 10)
			if SIEV_RESIPS[name]~=nil and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
				if owl_tech.can_siev_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					local amount=owl_tech.calculate_recipe_tire_bonus(meta,SIEV_RESIPS[name][2],SIEV_RESIPS[name][5])
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.add_item_redines(meta,amount)
				end
			elseif SIEV_RESIPS[name]~=nil and owl_tech.get_item_redines(meta)>=100 then
				if owl_tech.can_siev_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					
					input_its:set_count(input_its:get_count()-1) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)

					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+SIEV_RESIPS[name][4]) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item ={name=SIEV_RESIPS[name][3], count=SIEV_RESIPS[name][4], wear=0, metadata=""}
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end

					local random = math.random(1,100) --chans to get second  output
					if SIEV_RESIPS[name][6]>random then-- Add  second output if has 
						if not dst_add_its:is_empty() then
							dst_add_its:set_count(dst_add_its:get_count()+SIEV_RESIPS[name][8]) 
							inv:set_stack('dst_add', 1, dst_add_its)
						else
							local item ={name=SIEV_RESIPS[name][7], count=SIEV_RESIPS[name][8], wear=0, metadata=""}
							dst_add_its:add_item(item)
							inv:set_stack('dst_add', 1, dst_add_its)
						end
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
				end
			elseif SIEV_RESIPS[name]~=nil and  100/SIEV_RESIPS[name][5]*25<= owl_tech.get_pull_volume(meta,1)  and owl_tech.get_item_redines(meta)==0 then --begin for work
				local amount=owl_tech.calculate_recipe_tire_bonus(meta,SIEV_RESIPS[name][2],SIEV_RESIPS[name][5])
				local steam_new =owl_tech.get_pull_volume(meta,1)-25
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,amount)
			end
        else 
			owl_tech.reset_item_redines(meta)
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_base_sieve(meta)
		timer:start(0.2)
	end
})
--Crafte "owl_tech:bronze_sieve"
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:bronze_sieve",
    recipe = {
        {"mcl_wool:white","mcl_wool:white","mcl_wool:white"},
        {"owl_tech:bronze_plate","owl_tech:bronze_frames","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"}
    }
})
--Forge hammer bronze
local function set_formspect_base_forge_hammer(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
	local redines = owl_tech.get_item_redines(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Steam Forge Hammer"))).."]"..
	"list[context;input_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;dst;5.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
	"list[context;dst_add;6.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(6.5,1.5,1,1)..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"listring[context;dst]"..
	"listring[context;dst_add]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end
--froge hammer"
minetest.register_node("owl_tech:bronze_forge_hammer", {
	description = S("Bronze forge hammer"),
	_doc_items_longdesc = S("Automative forge"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_forge_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,input_in=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,8000,1,0) --steam  eat 25 steam in 1 tick
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,1) --all steam mishine has 1 tire
		owl_tech.reset_item_redines(meta)
		set_formspect_base_forge_hammer(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("input_in", 1) 
		inv:set_size("dst", 1)
		inv:set_size("dst_add", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local dst_its = inv:get_stack('dst', 1)
		local dst_add_its = inv:get_stack('dst_add', 1)
        if not input_its:is_empty() then -- chek for item in slots  
            local name= string.sub(input_its:get_name(), 10)
			if FORGE_HAMMER_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
				if owl_tech.can_forge_hammer_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					local amount=owl_tech.calculate_recipe_tire_bonus(meta,FORGE_HAMMER_RECIPS[name][2],FORGE_HAMMER_RECIPS[name][5])
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.add_item_redines(meta,amount)
				end
			elseif FORGE_HAMMER_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>=100 then
				if owl_tech.can_forge_hammer_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					
					input_its:set_count(input_its:get_count()-1) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)

					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+FORGE_HAMMER_RECIPS[name][4]) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item ={name=FORGE_HAMMER_RECIPS[name][3], count=FORGE_HAMMER_RECIPS[name][4], wear=0, metadata=""}
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end

					local random = math.random(1,100) --chans to get second  output
					if FORGE_HAMMER_RECIPS[name][6]>random then-- Add  second output if has 
						if not dst_add_its:is_empty() then
							dst_add_its:set_count(dst_add_its:get_count()+FORGE_HAMMER_RECIPS[name][8]) 
							inv:set_stack('dst_add', 1, dst_add_its)
						else
							local item ={name=FORGE_HAMMER_RECIPS[name][7], count=FORGE_HAMMER_RECIPS[name][8], wear=0, metadata=""}
							dst_add_its:add_item(item)
							inv:set_stack('dst_add', 1, dst_add_its)
						end
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
				end
			elseif FORGE_HAMMER_RECIPS[name]~=nil and  100/FORGE_HAMMER_RECIPS[name][5]*25<= owl_tech.get_pull_volume(meta,1)  and owl_tech.get_item_redines(meta)==0 then --begin for work
				local amount=owl_tech.calculate_recipe_tire_bonus(meta,FORGE_HAMMER_RECIPS[name][2],FORGE_HAMMER_RECIPS[name][5])
				local steam_new =owl_tech.get_pull_volume(meta,1)-25
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,amount)
			end
        else 
			owl_tech.reset_item_redines(meta)
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_base_forge_hammer(meta)
		timer:start(0.2)
	end
})
--Crafte "owl_tech:bronze_sieve"
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:bronze_forge_hammer",
    recipe = {
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","owl_tech:bronze_frames","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","mcl_anvils:anvil","owl_tech:bronze_plate"}
    }
})
--------------------------------------------
--Steam furnance
--[[
	local aftercooked
	cooked, aftercooked = minetest.get_craft_result({method = "cooking", width = 1, items = srclist})
]]
local function set_formspect_base_furnance(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
	local redines = owl_tech.get_item_redines(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Bronze furnance"))).."]"..
	"list[context;input_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;dst;5.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"listring[context;dst]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end
--Bronze furnance"
minetest.register_node("owl_tech:bronze_furnance", {
	description = S("Bronze furnance"),
	_doc_items_longdesc = S("Just a furnance - but use steam energy"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"owl_tech_base_meshanism_side.png^[colorize:#a35900:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#a35900:128)^owl_tech_forge_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,input_in=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,8000,1,0) --steam  eat 25 steam in 1 tick
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,1) --all steam mishine has 1 tire
		owl_tech.reset_item_redines(meta)
		set_formspect_base_furnance(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("input_in", 1) 
		inv:set_size("dst", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local dst_its = inv:get_stack('dst', 1)

		local aftercooked = minetest.get_craft_result({method = "cooking", width = 1, items = {input_its:get_name()}})
		if aftercooked.time>0  then
			if owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
				local steam_new =owl_tech.get_pull_volume(meta,1)-10
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,10)
			elseif  owl_tech.get_item_redines(meta)>=100 then
					
					input_its:set_count(input_its:get_count()-1) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)

					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+aftercooked.item:get_count()) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item =aftercooked.item
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-10
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
			elseif 100<= owl_tech.get_pull_volume(meta,1)  and owl_tech.get_item_redines(meta)==0 then --begin for work
				local steam_new =owl_tech.get_pull_volume(meta,1)-10
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,10)
			end
        else 
			owl_tech.reset_item_redines(meta)
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_base_furnance(meta)
		timer:start(0.2)
	end
})
--Crafte 
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:bronze_furnance",
    recipe = {
        {"owl_tech:bronze_plate","owl_tech:bronze_plate","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","owl_tech:bronze_frames","owl_tech:bronze_plate"},
        {"owl_tech:bronze_plate","mcl_furnaces:furnace","owl_tech:bronze_plate"}
    }
})

--------------------------------------------
--Steel mashins
local function set_formspect_steeel_macerator(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
	local redines = owl_tech.get_item_redines(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Steel macerator"))).."]"..
	"list[context;input_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;dst;5.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
	"list[context;dst_add;6.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(6.5,1.5,1,1)..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"listring[context;dst]"..
	"listring[context;dst_add]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end

--steel_macerator"
minetest.register_node("owl_tech:steel_macerator", {
	description = S("Steel macerator"),
	_doc_items_longdesc = S("Macerate dust"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_macerator_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,dst=1,dst_add=1,input_in=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,12000,1,0) --steam  eat 25 steam in 1 tick
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,2) 
		owl_tech.reset_item_redines(meta)
		set_formspect_steeel_macerator(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("input_in", 1) 
		inv:set_size("dst", 1)
		inv:set_size("dst_add", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local dst_its = inv:get_stack('dst', 1)
		local dst_add_its = inv:get_stack('dst_add', 1)
        if not input_its:is_empty() then -- chek for item in slots  
            local name= string.sub(input_its:get_name(), 10)
			if MACERATO_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
				if owl_tech.can_macaerat_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					local amount=owl_tech.calculate_recipe_tire_bonus(meta,MACERATO_RECIPS[name][2],MACERATO_RECIPS[name][5])
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.add_item_redines(meta,amount)
				end
			elseif MACERATO_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>=100 then
				if owl_tech.can_macaerat_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					
					input_its:set_count(input_its:get_count()-1) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)

					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+MACERATO_RECIPS[name][4]) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item ={name=MACERATO_RECIPS[name][3], count=MACERATO_RECIPS[name][4], wear=0, metadata=""}
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end

					local random = math.random(1,100) --chans to get second  output
					if MACERATO_RECIPS[name][6]>random then-- Add  second output if has 
						if not dst_add_its:is_empty() then
							dst_add_its:set_count(dst_add_its:get_count()+MACERATO_RECIPS[name][8]) 
							inv:set_stack('dst_add', 1, dst_add_its)
						else
							local item ={name=MACERATO_RECIPS[name][7], count=MACERATO_RECIPS[name][8], wear=0, metadata=""}
							dst_add_its:add_item(item)
							inv:set_stack('dst_add', 1, dst_add_its)
						end
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
				end
			elseif MACERATO_RECIPS[name]~=nil and  100/MACERATO_RECIPS[name][5]*25<= owl_tech.get_pull_volume(meta,1)  and owl_tech.get_item_redines(meta)==0 then --begin for work
				local amount=owl_tech.calculate_recipe_tire_bonus(meta,MACERATO_RECIPS[name][2],MACERATO_RECIPS[name][5])
				local steam_new =owl_tech.get_pull_volume(meta,1)-25
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,amount)
			end
        else 
			owl_tech.reset_item_redines(meta)
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y-1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_steeel_macerator(meta)
		timer:start(0.2)
	end
})
--Crafte "owl_tech:steel_macerator"
minetest.register_craft({
    type = "shaped",
    output = "owl_tech:steel_macerator",
    recipe = {
        {"mcl_core:flint", "mcl_core:flint", "mcl_core:flint"},
        {"owl_tech:steel_plate","owl_tech:steel_frames","owl_tech:steel_plate"},
        {"owl_tech:steel_plate","owl_tech:steel_plate","owl_tech:steel_plate"}
    }
})
--alloy smelter
local function set_formspect_steel_alloy_smelter(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
	local redines = owl_tech.get_item_redines(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.75,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Steel alloy smelter"))).."]"..
	"list[context;input_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;input_in_add;4,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(4,2.5,1,1)..
	"list[context;dst;5.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"listring[context;dst]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[context;input_in_add]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end
-------------------------------------------
--steel_alloy_smelter
minetest.register_node("owl_tech:steel_alloy_smelter", {
	description = S("Steel alloy smelter"),
	_doc_items_longdesc = S("Smelt alll"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_smelter_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,input_in=1,input_in_add=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,12000,1,0) --steam  eat 25 steam in 1 tick
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,2) --all steam mishine has 1 tire
		owl_tech.reset_item_redines(meta)
		set_formspect_steel_alloy_smelter(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("input_in", 1) 
		inv:set_size("input_in_add", 1) 
		inv:set_size("dst", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local input_its_add = inv:get_stack('input_in_add', 1)
		local dst_its = inv:get_stack('dst', 1)
        if not input_its:is_empty() then -- chek for item in slots  
            local name1= string.sub(input_its:get_name(), 10)
			local name2 = string.sub(input_its_add:get_name(), 10)
			local name =""
			local recipt_true = false 
			if SMELTER_RECIPS[name1..name2]~=nil then
				recipt_true= true
				name=name1..name2
			elseif SMELTER_RECIPS[name2..name1]~=nil then
				recipt_true= true
				name=name2..name1
			end
			if recipt_true and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real

				if owl_tech.can_smelter_recips_recips_go_in_this_mashien(meta,name,dst_its)  then-- chek for all itemslot and machine tire

					local amount=owl_tech.calculate_recipe_tire_bonus(meta,SMELTER_RECIPS[name][5],SMELTER_RECIPS[name][6])
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.add_item_redines(meta,amount)
				end
			elseif recipt_true and owl_tech.get_item_redines(meta)>=100 then
				if owl_tech.can_smelter_recips_recips_go_in_this_mashien(meta,name,dst_its)   then-- chek for all itemslot and machine tire
					input_its:set_count(input_its:get_count()-SMELTER_RECIPS[name][1]) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)
					input_its_add:set_count(input_its_add:get_count()-SMELTER_RECIPS[name][2]) --Remove item from input slot
					inv:set_stack('input_in_add', 1, input_its_add )

					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+SMELTER_RECIPS[name][4]) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item ={name=SMELTER_RECIPS[name][3], count=SMELTER_RECIPS[name][4], wear=0, metadata=""}
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
				end
			elseif recipt_true and  100/SMELTER_RECIPS[name][6]*25<= owl_tech.get_pull_volume(meta,1)  and owl_tech.get_item_redines(meta)==0 then --begin for work
				local amount=owl_tech.calculate_recipe_tire_bonus(meta,SMELTER_RECIPS[name][2],SMELTER_RECIPS[name][5])
				local steam_new =owl_tech.get_pull_volume(meta,1)-25
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,amount)
			end
        else 
			owl_tech.reset_item_redines(meta)
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_steel_alloy_smelter(meta)
		timer:start(0.2)
	end
})
--Crafte 
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:steel_alloy_smelter",
    recipe = {
        {"owl_tech:steel_plate","owl_tech:steel_plate","owl_tech:steel_plate"},
        {"mcl_furnaces:furnace","owl_tech:steel_frames","mcl_furnaces:furnace"},
        {"owl_tech:steel_plate","owl_tech:steel_plate","owl_tech:steel_plate"}
    }
})
local function set_formspect_steel_forge_hammer(meta)
	local fluid_1_name = owl_tech.get_pull_fluid_name(meta,1)
	local fluid_1_volume = owl_tech.get_pull_volume(meta,1)
	local redines = owl_tech.get_item_redines(meta)
	local formspec = "size[9,8.75]"..
	"label[0,4;"..minetest.formspec_escape(minetest.colorize("#313131", S("Inventory"))).."]"..
	"list[current_player;main;0,4.5;9,3;9]"..
	mcl_formspec.get_itemslot_bg(0,4.5,9,3)..
	"list[current_player;main;0,7.74;9,1;]"..
	mcl_formspec.get_itemslot_bg(0,7.74,9,1)..
	"label[2.25,0;"..minetest.formspec_escape(minetest.colorize("#313131", S("Steel Forge Hammer"))).."]"..
	"list[context;input_in;2.5,2.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(2.5,2.5,1,1)..
	"list[context;dst;5.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(5.5,1.5,1,1)..
	"list[context;dst_add;6.5,1.5;1,1;]"..
	mcl_formspec.get_itemslot_bg(6.5,1.5,1,1)..
	"label[0,1.5;"..minetest.formspec_escape(minetest.colorize("#313131", (fluid_1_name.."--"..fluid_1_volume))).."]"..
	"label[0,2;"..minetest.formspec_escape(minetest.colorize("#313131", (redines.." / 100"))).."]"..
	"listring[context;dst]"..
	"listring[context;dst_add]"..
	"listring[current_player;main]"..
	"listring[context;input_in]"..
	"listring[current_player;main]"..
	"listring[current_player;main]"
	meta:set_string("formspec", formspec)
end
--froge hammer"
minetest.register_node("owl_tech:steel_forge_hammer", {
	description = S("Steel forge hammer"),
	_doc_items_longdesc = S("Automative forge"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_forge_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,input_in=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,12000,1,0) --steam  eat 25 steam in 1 tick
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,2) 
		owl_tech.reset_item_redines(meta)
		set_formspect_steel_forge_hammer(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("input_in", 1) 
		inv:set_size("dst", 1)
		inv:set_size("dst_add", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local dst_its = inv:get_stack('dst', 1)
		local dst_add_its = inv:get_stack('dst_add', 1)
        if not input_its:is_empty() then -- chek for item in slots  
            local name= string.sub(input_its:get_name(), 10)
			if FORGE_HAMMER_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
				if owl_tech.can_forge_hammer_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					local amount=owl_tech.calculate_recipe_tire_bonus(meta,FORGE_HAMMER_RECIPS[name][2],FORGE_HAMMER_RECIPS[name][5])
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.add_item_redines(meta,amount)
				end
			elseif FORGE_HAMMER_RECIPS[name]~=nil and owl_tech.get_item_redines(meta)>=100 then
				if owl_tech.can_forge_hammer_recips_go_in_this_mashien(meta,name,dst_its,dst_add_its)  then-- chek for all itemslot and machine tire
					
					input_its:set_count(input_its:get_count()-1) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)

					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+FORGE_HAMMER_RECIPS[name][4]) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item ={name=FORGE_HAMMER_RECIPS[name][3], count=FORGE_HAMMER_RECIPS[name][4], wear=0, metadata=""}
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end

					local random = math.random(1,100) --chans to get second  output
					if FORGE_HAMMER_RECIPS[name][6]>random then-- Add  second output if has 
						if not dst_add_its:is_empty() then
							dst_add_its:set_count(dst_add_its:get_count()+FORGE_HAMMER_RECIPS[name][8]) 
							inv:set_stack('dst_add', 1, dst_add_its)
						else
							local item ={name=FORGE_HAMMER_RECIPS[name][7], count=FORGE_HAMMER_RECIPS[name][8], wear=0, metadata=""}
							dst_add_its:add_item(item)
							inv:set_stack('dst_add', 1, dst_add_its)
						end
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-25
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
				end
			elseif FORGE_HAMMER_RECIPS[name]~=nil and  100/FORGE_HAMMER_RECIPS[name][5]*25<= owl_tech.get_pull_volume(meta,1)  and owl_tech.get_item_redines(meta)==0 then --begin for work
				local amount=owl_tech.calculate_recipe_tire_bonus(meta,FORGE_HAMMER_RECIPS[name][2],FORGE_HAMMER_RECIPS[name][5])
				local steam_new =owl_tech.get_pull_volume(meta,1)-25
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,amount)
			end
        else 
			owl_tech.reset_item_redines(meta)
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_steel_forge_hammer(meta)
		timer:start(0.2)
	end
})
--Crafte
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:steel_forge_hammer",
    recipe = {
        {"owl_tech:steel_plate","owl_tech:steel_plate","owl_tech:steel_plate"},
        {"owl_tech:steel_plate","owl_tech:steel_frames","owl_tech:steel_plate"},
        {"owl_tech:steel_plate","mcl_anvils:anvil","owl_tech:steel_plate"}
    }
})
--Bronze furnance"
minetest.register_node("owl_tech:steel_furnance", {
	description = S("Steel furnance"),
	_doc_items_longdesc = S("Just a furnance - but use steam energy"),
	tiles = {
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_steam_input.png",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"owl_tech_base_meshanism_side.png^[colorize:#575757:128",
		"(owl_tech_base_meshanism_side.png^[colorize:#575757:128)^owl_tech_forge_face.png",
	},
	is_ground_content = false,
	stack_max = 64,
	groups = {pickaxey=2, owl_tech_machine=1,input_in=1},
	sounds = mcl_sounds.node_sound_metal_defaults(),
    paramtype2 = "facedir",
	_mcl_blast_resistance = 6,
	_mcl_hardness = 5,
    on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		owl_tech.add_new_pull(meta,8000,1,0) --steam  eat 25 steam in 1 tick
		owl_tech.set_pull_fluid_whitlist(meta,1,"owl_tech_steam")--whitlist for make and output ONLY Steam
        owl_tech.set_mashine_tire(meta,1) --all steam mishine has 1 tire
		owl_tech.reset_item_redines(meta)
		set_formspect_base_furnance(meta)
		owl_tech.update_fluid_pipe_around(pos)
		local timer =minetest.get_node_timer(pos)
		local inv = meta:get_inventory()
		inv:set_size("input_in", 1) 
		inv:set_size("dst", 1)
		timer:start(0.2)
	end,
	on_timer = function(pos, elapsed)
		local timer = minetest.get_node_timer(pos)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local input_its = inv:get_stack('input_in', 1)
		local dst_its = inv:get_stack('dst', 1)

		local aftercooked = minetest.get_craft_result({method = "cooking", width = 1, items = {input_its:get_name()}})
		if aftercooked.time>0  then
			if owl_tech.get_item_redines(meta)>0 and owl_tech.get_item_redines(meta)<100  then --recipt real
				local steam_new =owl_tech.get_pull_volume(meta,1)-10
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,10)
			elseif  owl_tech.get_item_redines(meta)>=100 then
					
					input_its:set_count(input_its:get_count()-1) --Remove item from input slot
					inv:set_stack('input_in', 1, input_its)

					if not dst_its:is_empty() then-- Add  main output 
						dst_its:set_count(dst_its:get_count()+aftercooked.item:get_count()) 
						inv:set_stack('dst', 1, dst_its)
					else
						local item =aftercooked.item
						dst_its:add_item(item)
						inv:set_stack('dst', 1, dst_its)
					end
					local steam_new =owl_tech.get_pull_volume(meta,1)-10
					owl_tech.set_pull_volume(meta,1,steam_new)
					owl_tech.reset_item_redines(meta)
			elseif 100<= owl_tech.get_pull_volume(meta,1)  and owl_tech.get_item_redines(meta)==0 then --begin for work
				local steam_new =owl_tech.get_pull_volume(meta,1)-10
				owl_tech.set_pull_volume(meta,1,steam_new)
				owl_tech.add_item_redines(meta,10)
			end
        else 
			owl_tech.reset_item_redines(meta)
		end
		if minetest.get_item_group((minetest.get_node({x=pos.x,y=pos.y+1,z=pos.z})).name,"fluid_pipe") then --get from pipe steam
			local meta_up = minetest.get_meta({x=pos.x,y=pos.y-1,z=pos.z})
			local can_do ,inde_pull =owl_tech.test_add_fluid_in_any_pulls(meta,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta))
			local can_do2 ,inde_pull_2 , remove_amount =owl_tech.test_remove_fluid_in_any_pulls(meta_up,"owl_tech_steam",owl_tech.get_fluid_sand_in_tick(meta_up))
			if can_do and can_do2 then
				owl_tech.add_fluid_in_node_pull(meta,"owl_tech_steam",remove_amount,inde_pull)
				owl_tech.remove_fluid_in_node_pull(meta_up,remove_amount,inde_pull)
			end
		end
		owl_tech.delit_name_fluid_if_0(meta)
		set_formspect_base_furnance(meta)
		timer:start(0.2)
	end
})
--Crafte 
minetest.register_craft({
    type = "shaped",
    output ="owl_tech:steel_furnance",
    recipe = {
        {"owl_tech:steel_plate","owl_tech:steel_plate","owl_tech:steel_plate"},
        {"owl_tech:steel_plate","owl_tech:steel_frames","owl_tech:steel_plate"},
        {"owl_tech:steel_plate","mcl_furnaces:furnace","owl_tech:steel_plate"}
    }
})
